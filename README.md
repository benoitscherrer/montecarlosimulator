# Monte Carlo simulator of the diffusion MRI signal in the white-matter microstructure#
Author: Gaetan Rensonnet
(gaetan.rensonnet@uclouvain.be, gaetan.rensonnet@epfl.ch)

If you make use of this software or parts of it, please cite:

Rensonnet, Gaëtan, Benoît Scherrer, Gabriel Girard, Aleksandar Jankovski, Simon K. Warfield, Benoit Macq, Jean-Philippe Thiran, and Maxime Taquet. 
"Towards microstructure fingerprinting: Estimation of tissue properties from a dictionary of Monte Carlo diffusion MRI simulations." 
NeuroImage 184 (2019): 964-980. Publisher: https://www.sciencedirect.com/science/article/pii/S1053811918319487

### 1. What does the simulator do? ###

* Monte Carlo simulations of the random walk of water molecules in 3D geometries mimicking white matter tissues in diffusion-weighted MRI (DW-MRI) experiments;
* parallel, multi-core computing supported with OpenMP;
* cells (e.g., axons and glia) are represented by perfectly impermeable cylinders and spheres packed in an infinite --but not necessarily periodic-- pattern;
* the random-walk dynamics rely on fixed-length steps with uniformly-distributed orientations;
* for computational efficiency, simulations are only performed in the extracellular space since fast and accurate [methods][MCFAL] [1] exist to compute the DW-MRI signal arising from water in closed cylindrical and spherical pores. The total signal can then be obtained as a weighted sum of the intra- and extracellular contributions [2];
* the pulse-gradient spin-echo (PGSE) experiment is implemented but more sophisticated diffusion sequences can easily be included.

### 2. Requirements ###

* CMake 3.1.0 or higher (https://cmake.org/), freely-available on all platforms;
* C++ compiler such as
	+ Microsoft Visual Studio Compiler (MSVC). The free Community version is enough (https://visualstudio.microsoft.com/vs/community/). Make sure to install the **Desktop development with C++** module. Tested with Visual Studio 14 Win64 (2015);
	+ GNU Compiler Collection (GCC) available on most Unix-like systems as well as on Windows. Tested on Windows 10 with gcc 4.9.2 and on Linux CentOS 7 with versions 4.8.5 and 4.9.2 (Red Hat).

### 3. Compilation ###
The following directory structure is assumed:

```
└── montecarlosimulator
    └── src
    └── build
    └── tests
```
where `src` contains the source code cloned from this repository, including the main `CMakeLists.txt` file.

Make sure the code is compiled in `Release` mode and not in `Debug` as this dramatically affects the performance of the simulator.

#### Unix-like systems

We assume that CMake is known to the system as the `cmake` command. The `make` utility must be available.
```
 > cd montecarlosimulator/build
 > cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release   ../src
 > make
```
#### Windows - Using the GCC compiler
We assume that CMake is known to the system as the `cmake` command. A Make utility such as the GNU Make `gmake` command for Windows must be available.
```
 > cd montecarlosimulator\build
 > cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release   ../src
 > gmake
```
#### Windows -  Using Visual Studio
We assume that CMake is known to the system as the `cmake` command.
```
> cd montecarlosimulator\build
> cmake -G "Visual Studio 14 2015"  ../src
```
where the generator (`-G`) corresponding to your version of Visual Studio should be provided (`cmake --help` for details).

The command should have created a Solution file `MC_SIMULATOR.sln` in the current `build` directory. Open it in Visual Studio. 
From the **Build** menu select **Configuration Manager**, then select **Release**. From the **Build** menu, select **Build Solution** (Ctrl+Shift+B).
This should create five exectuable `.exe` files in the `build\Release` folder, which you can move to the `build` folder (this is just to match the example commands given in the remainder of this Read-me file).

### 4. Usage ###
After compilation, five executable files should have been created in the `build` folder:

Unix  						 | Windows 							 |	  Purpose
---------------------------- | --------------------------------- | ---------------------------------------------------------------------------------------------------------------------
`MC_sim_hexagonal_packing`   | `MC_sim_hexagonal_packing.exe`    | Hexagonal packing of straight, identical, parallel cylinders
`MC_sim_cylinder_packing`    | `MC_sim_cylinder_packing.exe`     | Random packing of straight, parallel cylinders with varying radii
`MC_sim_sphere_packing`      | `MC_sim_sphere_packing.exe`       | Random packing of spheres with varying radii
`MC_sim_crossing`            | `MC_sim_crossing.exe`             | Two populations of cylinders crossing in interleaved planes with population-specific radius and packing density [3]
`MC_dev`                     | `MC_dev.exe`   					 | Entry-point for developers to create new environments from scratch or use existing wrappers

The first four executables are **command-line** utilities. To get a help menu with details on the inputs and options, assuming you are in the `build` folder, do
```
./MC_sim_hexagonal_packing --help
```
on a Unix-like system, and

```
MC_sim_hexagonal_packing.exe --help
```
on Windows.


### 5. References ###

[MCFAL]: https://pmc.polytechnique.fr/pagesperso/dg/MCF/MCF_e.htm

[1] Grebenkov, Denis S. "Laplacian eigenfunctions in NMR. I. A numerical tool." Concepts in Magnetic Resonance Part A: An Educational Journal 32, no. 4 (2008): 277-301.

[2] Rensonnet, Gaëtan, Damien Jacobs, Benoît Macq, and Maxime Taquet. "A hybrid method for efficient and accurate simulations of diffusion compartment imaging signals." In 11th International Symposium on Medical Information Processing and Analysis, vol. 9681, p. 968107. International Society for Optics and Photonics, 2015.

[3] Rensonnet, Gaëtan, Benoît Scherrer, Simon K. Warfield, Benoît Macq, and Maxime Taquet. "Assessing the validity of the approximation of diffusion‐weighted‐MRI signals from crossing fascicles by sums of signals from single fascicles." Magnetic resonance in medicine 79, no. 4 (2018): 2332-2345.