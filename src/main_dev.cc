#define _CRT_SECURE_NO_WARNINGS // stop Visual Studio from issuing warnings about fopen being unsafe
#define _CRTDBG_MAP_ALLOC   // for line number in memory reports by _CrtDumpMemoryLeaks(); must be placed in every .c[xx] file
#define _USE_MATH_DEFINES

#include <fstream>  // std::ofstream
#include <math.h>   // sqrt, pow, M_PI, ... (requires -lm in compilation command)
#include <stdio.h>  // printf, FILE, fread(), fwrite(), ..
#include <stdlib.h> // malloc, calloc, free
#include <string>   // for distribution of pores
#include <vector>   // C++ vectors
#ifdef _DEBUG
//For memory reports on Visual Sutdio (must come after stdlib.h), 
// https://msdn.microsoft.com/en-us/library/x98tx3cf.aspx)
// Enables _CrtDumpMemoryLeaks();
// Replaces new operator 
# include <crtdbg.h>
# define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#else
# define DBG_NEW new
#endif // _DEBUG

#include "MP3D/MP3D.h"     // 3D multipopulation  environment and methods
#include "MP3D/RNG.h"      // random number generation (MT19937), for debugging
#include "MP3D/RNG_r.h"    // thread-safe re-entrant RNG, for debugging
#include "MP3D/Sig_Sim.h"  // Signal simulator module
#include "pore_packing/pack_pores.h"  // sphere and cylinder packing modules

// Entry-point for the developer.
// This script is intended for testing and experimenting with new environments,
// wrapper functions, simulations parameters, etc.
// Our philosophy is to impose stringent compilation constraints such as 
// treating warnings as errors. In particular, no variable can be declared and
// remain unused (otherwise this warning would turn into a compilation errror).
// This results in a lot of code having to be commented out in this script.
// A bit annoying to experiment and play around but it makes the rest of the
// software more robust.


// Execute program : 
// - `./main '     for normal execution
// - `export OMP_NUM_THREADS=[1|2|...|8] ; ./main ; '
//    for multi-threading after compilation with -fopenmp flag
// - `time { ./main ; } ;'  for function timing 
// - `valgrind --leak-check=full --show-leak-kinds=all 
//       --suppressions=/home/rensonnetg/lib/valgrind/my_suppressed_errors.supp
//       ./main'
//    for analyzing memory leaks with Valgrind
// - `valgrind --tool=massif --massif-out-file=massif.out --time-unit=B ./main ;
//    ms_print massif.out ;' 
//    to gather heap profiling information
//  (http://valgrind.org/docs/manual/ms-manual.html)




int main( int /*argc*/, char** /*argv[]*/ ) {

 { // braces needed for memory reports by _CrtDumpMemoryLeaks()
   // on Vis Studio 2015 (only in Debug compilation)

  // ------------------------------
  // ---- Test pore packing ----
  // ------------------------------

  //unsigned long pos_seed = 141414;
  //unsigned long rad_seed = 151515;
  //double gap = 1.0e-8;
  //int num_pores = 1000;
  //double icvf = 0.50;
  //double std = 0.8e-6;
  //double mean = 1.5e-6;
  //double maxRad = 7.0e-6;
  //double minRad = 1.0e-8;
  //std::string distribution = "normal";
  //std::string pore_shape = "cylinder";
  //std::string output_basename = "../output/packing_" + distribution;

  //int max_atttempts = 1000000;
  //Substrate* substrate = NULL;

  //if(pore_shape == "sphere") {
  //  substrate = DBG_NEW SphereSubstrate(distribution, minRad, maxRad, mean, std,
  //                                  icvf, gap, num_pores, max_atttempts);
  //}
  //else if(pore_shape == "cylinder") {
  //  substrate = DBG_NEW CylinderSubstrate(distribution, minRad, maxRad, mean, std,
  //                                    icvf, gap, num_pores, max_atttempts);
  //}
  //
  //substrate->position_pores(rad_seed, pos_seed);
  //    
  //std::string file1 = output_basename + "_" + pore_shape + "s.txt";
  //std::ofstream out1(file1.c_str());
  //substrate->printPores(out1);
  //out1.close();
  //    
  //std::string file2 = output_basename + "_unique" + "_" + pore_shape + "s.txt";
  //std::ofstream out2(file2.c_str());
  //substrate->printUniquePores(out2);
  //out2.close();
  //    
  //std::string file3 = output_basename + "_substrate_properties.txt";
  //std::ofstream out3(file3.c_str());
  //substrate->printSubstrateProperties(out3);
  //out3.close();

  //// Prepare substrate creation
  //struct PackedPoreSubstrateParams params;
  //params.lattice_size = substrate->max_limits[0] - substrate->min_limits[0];
  //params.from_file = true;
  //int i;
  //if (params.from_file) {
  //  params.porefilepath = (char*)file1.c_str();
  //}
  //else {
  //  params.num_lines = (int)substrate->pores.size();
  //  params.pore_array = (double**)malloc(params.num_lines * sizeof(double*));
  //  for (i = 0; i < params.num_lines; i++) {
  //    params.pore_array[i] = (double*)malloc(4 * sizeof(double));
  //    params.pore_array[i][0] = substrate->pores[i].P_[0];
  //    params.pore_array[i][1] = substrate->pores[i].P_[1];
  //    params.pore_array[i][2] = substrate->pores[i].P_[2];
  //    params.pore_array[i][3] = substrate->pores[i].radius_;
  //  }
  //}

  //// Create substrate
  //MP3D_env myenv;
  //if (pore_shape == "sphere") {
  //   MP3D_create_environment_sphere_heterpack(&myenv, params);  // new style
  //  //MP3D_create_environment_sphere_heterpack(&myenv,
  //  //                                         params.lattice_size,
  //  //                                         params.porefilepath); // old style
  //}
  //else if (pore_shape == "cylinder") {
  //  MP3D_create_environment_heterpack(&myenv, params); // new style
  //  //MP3D_create_environment_heterpack(&myenv, params.lattice_size, params.porefilepath);  // old style
  //}

  //if (!params.from_file) {
  //  for (i = 0; i < params.num_lines; i++) {
  //    free(params.pore_array[i]);
  //  }
  //  free(params.pore_array);
  //}


  // -----------------------------
  // ---- end pore packing ------
  // -----------------------------


  // -----------------------------
  // ---- Select an environment----
  // --------------------------------

  //double Lsizes[6] = { 0.168829667548242E-4,
  //  0.151005737526562E-4,
  //  0.137848494078320E-4,
  //  0.144142592591770E-4,
  //  0.144820924093385E-4,
  //  0.176446753540625E-4 };
  //unsigned int subID = 5;
  //double L = Lsizes[subID - 1];
  //char cylfilepath[150];
  //sprintf(&(cylfilepath[0]),"../data/cylfiles/cylpos%d.txt", subID);
  
  
  double rad = 0.5e-6;
  double fin_tot = 0.7;
  //double rotangle = 3.0*M_PI / 8.0;
  //unsigned int n = 5;  // cylinder thickness in case of two populations crossing in interwoven sheets of cylinders. E.g., n=3: || || || o o o  || || || o o o || || ||
  //double angle_max = M_PI / 7;  // half angular sector in environments with helicoidally-placed cylinders
  //unsigned int numpop = 7;


  //double temp_term = (2.0 + (n - 1.0)*sqrt(3.0));  // uncomment for environment_crossing
  //double determinant = rad*rad*(temp_term*temp_term - (1.0 + (n - 1.0)*sqrt(3.0) / 2.0)*(4.0 + 2.0 * (n - 1.0)*sqrt(3.0) - n*M_PI/fin_tot));
  //double e = (-rad*(2.0 + (n - 1)*sqrt(3.0)) + sqrt(determinant)) / (1.0 + (n - 1.0)*sqrt(3.0) / 2.0); // when each pop is in hexagonal packing

  // Create environement using one of the wrapper functions
  MP3D_env myenv;

  //MP3D_create_environment(&myenv, 5);  // scenario numbers: 1 (3cyl pop + 1 sph pop), 2 (crossing cyls), 5 (120 randomly-packed cylinders)

  MP3D_create_environment_hexpack(&myenv, rad, fin_tot);

  // NOTES r0.5_s0.2_f0.70_n$n_g0.01_substrate_properties: 
  // n=100: 0.00001077614256140, n=500:0.00002497253964041, n=1000:0.00003549381087273, 
  // n=5000: 0.00007997034998211, n=10000:0.00011322935375896, n=15000:0.00013853445866540,
  // n=100,000: 0.00035948396194708
 // struct PackedPoreSubstrateParams params;
 // params.from_file = true;
 // params.lattice_size = 0.00003549381087273;
 // params.porefilepath = (char*)"../data/cylfiles/r0.5_s0.2_f0.70_n1000_g0.01_gamma_cylinders.txt";
  //MP3D_create_environment_heterpack(&myenv, params);

  //MP3D_create_environment_crossing(&myenv, rad, rad, e, e, e, rotangle, n, n);

  //MP3D_create_environment_dispersion(&myenv, angle_max, numpop, rad, fin_tot);

  //MP3D_create_environment_free_diffusion(&myenv);

 // struct PackedPoreSubstrateParams params;
 // params.from_file = true;
 // params.lattice_size = 0.00005188304201186;
 // params.porefilepath = (char*) "../data/spherefiles/dynamic_array_gamma_spheres.txt";
  //MP3D_create_environment_sphere_heterpack(&myenv, params);

  // Crash test: dilation in piling direction (avoid integer values)
  //myenv.pile_period = myenv.pile_period * 1.37;

  // -------------------------------------
  // --- Check and display properties ---
  // -------------------------------------

  // Check consistency (should that be included in environment creation ?)
  // Should MP3D_check_consistency throw the error itself?
  unsigned char env_ok = MP3D_check_consistency(&myenv);
  if (!env_ok) {
    printf("PROBLEM WITH ENVIRONMENT. ABORTING.\n");
    return -1;
  }

  // Display environment properties
  MP3D_print_info(&myenv);



  //-------------------------------------------------
  // --- Run and/or debug Monte Carlo simulations ---
  // ------------------------------------------------
  // Monte Carlo parameters
  unsigned int seed = 141414;
  unsigned int Nspins = 1000;
  unsigned int Nsteps = 2000;
  double  DIFF = 2.0e-9;
  const bool MultiThread_on = false;
  const bool return_phases = false;
  char schemefile[150] = "../data/schemefiles/dummy.scheme1";

  char outputfile[150]; // extends with null characters (value-initialized characters)
  //sprintf(&(outputfile[0]), "../src/signal_output/hex_acaxnoddi_r=%g_f=%g_Nspins=%d_Njumps=%d_seed%d.bdouble",rad*1e6,fin_tot,Nspins,Nsteps,seed);
  sprintf(&(outputfile[0]), "../output/test_packing"); // ../src/signal_output/
  printf("Outputfile is %s\n", &(outputfile[0]));

  // START DEBUG jump
  // double dt = 0.0537 / Nsteps;
  // double Ljump = sqrt(2 * DIFF * 3 * dt); // 8.027452896e-07;
  // MP3D_initialize_spatial_sectors(&myenv, Ljump);
  //printf("Nsteps=%d, DIFF=%g, seed=%d, Ljump=%g \n",
  //    Nsteps, DIFF, seed, Ljump);
  //double x[3] = { -5.130704e-05, 3.109669e-07, 1.025879e-06};
  //double xup[3] = {-5.094188e-05, 7.259776e-07, 8.969476e-07 };
  //MP3D_fixed_jump(x, xup, &myenv);
  //printf("After jump: x=[%g, %g, %g]\n", x[0], x[1], x[2]);
  // END DEBUG jump

  // START DEBUG check is_intra
  //double x[3];
  //unsigned int indpopin, i, numin = 0, Ndrops = 100;
  //init_genrand(seed);
  //unsigned char isin;

  //double dt = 0.0537 / Nsteps;
  //double Ljump = sqrt(2 * DIFF * 3 * dt); // 8.027452896e-07;
  //MP3D_initialize_spatial_sectors(&myenv, Ljump);

  //for (i = 0; i < Ndrops; i++) {
  //  MP3D_drop_particle_initialized_sgthrd(x, &myenv);  // modifies x
  //  isin = MP3D_is_intra(&x, &myenv, &indpopin);
  //  if (isin) {
  //    numin++;
  //    printf("Drop %d: spin inside population %d\n", i + 1, indpopin);
  //  }  
  //}
  //printf("Total in %d out of %d (fin +- %g)\n", numin, Ndrops, (double)numin / (double)Ndrops);
  // END DEBUG check is_intra

  //Perform Monte Carlo simulation
  SigSim_3D_sPGSE(seed, &(schemefile[0]), &(outputfile[0]), &myenv, Nspins, Nsteps, DIFF, MultiThread_on, return_phases);

  printf("Simulation successful.\n");

  // Clean up
  MP3D_clean_env(&myenv);
 
} // braces needed for memory reports with _CrtDumpMemoryLeaks()
// on Vis Studio 2015 (only in Debug compilation)

  //_CrtDumpMemoryLeaks(); // for memory reports on VisualStudio 2015

  return 0;
}

