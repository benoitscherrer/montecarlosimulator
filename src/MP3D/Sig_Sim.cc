// Signal Simulator
#define _CRTDBG_MAP_ALLOC   // for line number in memory reports by _CrtDumpMemoryLeaks(); must be placed in every .c[xx] file
#define _CRT_SECURE_NO_WARNINGS // to stop Visual Studio from issuing warnings about fopen being unsafe

#include "Sig_Sim.h" 

#define _USE_MATH_DEFINES
#include <math.h>  // sqrt, pow, M_PI, ... (add -lm in compilation command)
#include <omp.h>  // openMP shared-memory parallelism 
#include <stdio.h>  // printf, FILE, fread(), fwrite(), ..
#include <stdlib.h>  // malloc, calloc, free, exit
#include <string>  // C++ strings
#include <vector>  // C++ vectors

#include "MP3D.h"  // 3D multipopulation  environment and methods
#include "mymaths.h"  // gen_point_sphere, add_arrays, 
#include "RNG.h"  // random number generation (MT19937, Matlab's default), single-thread execution only
#include "RNG_r.h"  // re-entrant RNG (MT19937), thread-safe on different data
#include "tools.h"  // import_scheme_alloc, fileparts



const double gam = 2*M_PI*42.577480e6;

const double scale_G = 1e-3;  // rescaling factor for gradient intensities, converts them them to milliTesla/micrometer
const double scale_T = 1e3;  // rescaling factor for time quantities (Delta, delta, ...) converts them to millisecond
const double scale_gam = 1e-6;  // rescaling factor for gyromagnetic ratio, converts it to 1/(ms mT)
const double scale_L = 1e6;  // rescaling factor for spatial dimensions, converts them to micrometers

unsigned int max_spindone_displays = 100 ;  // number of times "spin ... done" shows on the standard output
unsigned int min_spindone_interval = 500;  // do not display "spin... done" if fewer than min_spindone_interval trajectories have been completed

/*
Development: Common simulation framework: import scheme matrix, test opening of output file(s), get dynamics duration, jump length, spin display interval, initiliaze Nseqx1 E[cos(phi)] array.
*/

void SigSim_3D_sPGSE(const unsigned int seed, char * const schemefilename, const std::string & output_basename,
  MP3D_env * const environment, const unsigned int Nspins, const unsigned int Njumps, const double D,
  const bool multithread_on, const bool return_phases) {

  // NOTE2SELF: a const function parameter imposes an error on the programmer of the function in case of any attempt to modify it
  unsigned int i;

  // Importat data from schemefile, compute TE_max
  double** scheme_matrix;
  unsigned int Nseqs;
  scheme_matrix = import_scheme_alloc(schemefilename, &Nseqs);
  printf("import_scheme_alloc successfully imported %d sequences.\n", Nseqs);


  // Test opening output binary file, abort if unable to open before long MC simulation
  FileParts output_fparts = fileparts(output_basename);
  std::string outputfile;
  outputfile.append(output_fparts.path).append(output_fparts.name).append(".bdouble");

  FILE *ptr_outfile = fopen(outputfile.c_str(), "wb");
  if (!ptr_outfile) {
    printf("Unable to open file %s. ABORTING.\n", outputfile.c_str());
    perror("Error detail");
    exit(-1);
  }
  fclose(ptr_outfile);
  printf("Outputfile is %s\n", outputfile.c_str());

  // Test opening of final phase files, abort if unable to open before long MC simulation
  std::string file_phase_x = output_fparts.path;
  std::string file_phase_y = output_fparts.path;
  std::string file_phase_z = output_fparts.path;
  FILE * ptr_phase_file_x, *ptr_phase_file_y, *ptr_phase_file_z;
  if (return_phases) {
    file_phase_x.append(output_fparts.name).append("_phase_x").append(".bdouble");
    file_phase_y.append(output_fparts.name).append("_phase_y").append(".bdouble");
    file_phase_z.append(output_fparts.name).append("_phase_z").append(".bdouble");

    ptr_phase_file_x = fopen(&(file_phase_x[0]), "wb");
    if (!ptr_phase_file_x) {
      printf("Unable to open file %s. ABORTING.\n", &(file_phase_x[0]));
      perror("Error detail");
      exit(-1);
    }
    fclose(ptr_phase_file_x);

    ptr_phase_file_y = fopen(&(file_phase_y[0]), "wb");
    if (!ptr_phase_file_y) {
      printf("Unable to open file %s. ABORTING.\n", &(file_phase_y[0]));
      perror("Error detail");
      exit(-1);
    }
    fclose(ptr_phase_file_y);

    ptr_phase_file_z = fopen(&(file_phase_z[0]), "wb");
    if (!ptr_phase_file_z) {
      printf("Unable to open file %s. ABORTING.\n", &(file_phase_z[0]));
      perror("Error detail");
      exit(-1);
    }
    fclose(ptr_phase_file_z);
  }

  // Get duration of the dynamics based on maximum time with magnetic gradients turned on
  double TE_max = -1.0;
  for (i = 0; i< Nseqs; i++) {
    double TE_i = scheme_matrix[i][4] + scheme_matrix[i][5];
    TE_max = ((TE_i > TE_max) ? TE_i : TE_max);
  }
  // Set time step, jump length
  const unsigned char dim = 3;
  double dt = TE_max / ((double)Njumps);
  double Ljump = sqrt(2 * dim*D*dt);

  // Initialize spatial sectors for efficient intersection checking
  MP3D_initialize_spatial_sectors(environment, Ljump);  // this will modify fields of environment

  // Set spin interval for displaying progress of simulation
  unsigned int spin_display_interval = ((Nspins / max_spindone_displays > min_spindone_interval) ? Nspins / max_spindone_displays : min_spindone_interval);
  
  // Initialize final signal array [Nseqs x 1]
  double* E_cos_phase;  // [Nseqs x 1] will hold the final diffusion signal
  E_cos_phase = (double*)calloc(Nseqs, sizeof(double));  // initialized to 0

  // Declare phase arrays to pass them as arguments below 
  std::vector < std::vector<double> > phase_final_x;
  std::vector < std::vector<double> > phase_final_y;
  std::vector < std::vector<double> > phase_final_z;

  // Initialize phase_arrays requested [Nseqs x Nspins] (may be more efficient transposed...)
  if (return_phases) {
    phase_final_x.resize(Nseqs);
    phase_final_y.resize(Nseqs);
    phase_final_z.resize(Nseqs);
    for (i = 0; i < Nseqs; i++) {
      phase_final_x[i] = std::vector<double>(Nspins, 0.0);  // initialized to 0.0
      phase_final_y[i] = std::vector<double>(Nspins, 0.0);
      phase_final_z[i] = std::vector<double>(Nspins, 0.0);
    }
  }

  // Simulate  spin trajectory, store trajectory for later integration
  printf("Beginning fixed-step simulation with %d spin(s), %d jump(s) per trajectory (dt=%4.3e, dx=%4.3e, D=%g), %d sequence(s), duration %g sec.\n", Nspins, Njumps, dt, Ljump, D, Nseqs, TE_max);
  
  // Call to Monte Carlo simulation routine, modify E_cos_phase
  if (multithread_on) {
    SigSim_3D_sPGSE_multithread(seed, scheme_matrix, environment, Nspins, Njumps, Nseqs, dt, Ljump, spin_display_interval, E_cos_phase, return_phases, phase_final_x, phase_final_y, phase_final_z);
  }
  else {
    SigSim_3D_sPGSE_singlethread(seed, scheme_matrix, environment, Nspins, Njumps, Nseqs, dt, Ljump, spin_display_interval, E_cos_phase, return_phases, phase_final_x, phase_final_y, phase_final_z);
  }

  // Open output binary file, abort if unable to open
  ptr_outfile = fopen(&(outputfile[0]), "wb");
  if (!ptr_outfile) {
    printf("Unable to open file %s. ABORTING.\n", &(outputfile[0]));
    perror("Error detail");
    exit(-1);
  }
  // Print final signals after MC simulation and write to binary file in big-endian format
  printf("Printing out all %d signal(s) : \n", Nseqs);
  double swapped_double;
  for (i = 0; i<Nseqs; i++) {
    // Print out final signal to standard output
    printf("%5f\n", E_cos_phase[i]); 
    // Write to output binary file
    if (is_bigendian()) {
      fwrite(&(E_cos_phase[i]), sizeof(E_cos_phase[i]), 1, ptr_outfile); // diff_signals+i should work as well 
    }
    else {
      swapped_double = swap_double_64(E_cos_phase[i]);
      fwrite(&swapped_double, sizeof(swapped_double), 1, ptr_outfile);
    }
  }
  fclose(ptr_outfile);

  // Write phase files if requested
  if (return_phases) {
    write_array_to_file(phase_final_x, &(file_phase_x[0]));
    printf("Wrote phases related to x-component of gradient(s) to %s (%g elements, %g Mbytes).\n", 
      &(file_phase_x[0]), (double)Nspins * (double)Nseqs, (double)Nspins*Nseqs*8.0/1e6);

    write_array_to_file(phase_final_y, &(file_phase_y[0]));
    printf("Wrote phases related to y-component of gradient(s) to %s (%g elements, %g Mbytes).\n", 
      &(file_phase_y[0]), (double)Nspins * (double)Nseqs, (double)Nspins*Nseqs*8.0 / 1e6);

    write_array_to_file(phase_final_z, &(file_phase_z[0]));
    printf("Wrote phases related to z-component of gradient(s) to %s (%g elements, %g Mbytes).\n", 
      &(file_phase_z[0]), (double)Nspins * (double)Nseqs, (double)Nspins*Nseqs*8.0 / 1e6);
  }

  // Clean up
  for (i = 0; i< Nseqs; i++) {
    free(scheme_matrix[i]);
  }
  free(scheme_matrix);

  free(E_cos_phase);
}

/*
 Multi-thread Monte Carlo simulation routine. 
 Modifies the Nseq x 1 array E_cos_phase containing the final signal values. 
 This routine uses more memory as a Nseq x Nspins array needs to be created to store the final phases of all random walkers for all sequences in scheme_matrix (shared by all threads, not replicated).
 In addition, in program thread stores a Nsteps x 3 array containing the trajectory of the current random walker.

*/
void SigSim_3D_sPGSE_multithread(const unsigned int seed, double * const * const scheme_matrix, MP3D_env *  const environment,
  const unsigned int Nspins, const unsigned int Njumps, const unsigned int Nseqs, const double dt, const double Ljump,
  const unsigned int spin_display_interval, double * const E_cos_phase, const bool return_phases, 
  std::vector<std::vector<double> > & phase_final_x, std::vector<std::vector<double> > & phase_final_y, std::vector<std::vector<double> > & phase_final_z) {

  unsigned int i, j;  // makes more sense to define j within each thread locally
  const unsigned char dim = 3;

  std::vector<std::vector <double> > phase_final;  // [Nseqs x Nspins] such a large array needs to be dynamically allocated, not on stack, might be more efficient to "transpose" it ?
  if (!return_phases) {
    phase_final.resize(Nseqs);
    for (i = 0; i < Nseqs; i++) {
      phase_final[i] = std::vector<double>(Nspins, 0.0);
    }
  }
  int ispin;  // OpenMP in Visual Studio does not accept unsigned loop variables for some reason
#pragma omp parallel default(none) private(ispin,i,j) shared(phase_final, phase_final_x, phase_final_y, phase_final_z) 
  //shared(environment,Ljump,Nseqs,scheme_matrix,phase_final,dt,Njumps,Nspins, seed, spin_display_interval) n
  //not accepted by openMP bc it is redundant to tell it to share const variables.
  {
    // Initialize random number generator in each thread. Each thread MUST have its independent RNG state vector.
    unsigned long* rng_state_thrd = (unsigned long*)malloc(get_mt19337_state_size() * sizeof(unsigned long));
    if (!rng_state_thrd) {
      printf("ERROR: SigSim_3D_sPGSE: Failure to allocate memory for RNG state in thread %d of %d. ABORTING.\n", omp_get_thread_num(), omp_get_num_threads());
      perror("Error detail");
      exit(-1);
    }
    init_genrand_r(seed + 1000 * omp_get_thread_num(), &rng_state_thrd);

    // Initialize vector of candidate object indices in each thread independently
    // To make MP3D_get_sector_objects and MP3D_is_intra re-entrant and 
    // thread-safe.
    std::vector<unsigned int> cand_obj(environment->totobjects, 0); // size [totobjects] buffer to hold candidate objects for intersection at every jump, preallocated once and for all


#pragma omp for  
    for (ispin = 0; ispin < (int)Nspins; ispin++) {
      // (casting to avoid comparison between signed and unsigned integer expressions)

      // Initialize individual spin's trajectory
      std::vector< std::vector<double> > trajectory_hist(dim, std::vector<double>(Njumps + 1, 0.0)); // dim-by-(Njumps+1) array storing RELATIVE position of spin taken from initial point

      // Initial particle drop in extracellular space
      double x[3], dx[3], xup[3];  // buffers for current particle position, displacement vector, candidate for new position xup=x+dx
      double x_backup[3], xup_backup[3];  // buffers for x and xup for debugging in case the jump leads to an undesired crossing
      unsigned int indpopin;  // stores population index containing spin if intracellular, -1 if extracellular
      MP3D_drop_particle_initialized(x, &rng_state_thrd, environment);  // will modify x with random, uniformly-drawn position within environment
      while (MP3D_is_intra(&x, environment, &indpopin, cand_obj)) {
        MP3D_drop_particle_initialized(x, &rng_state_thrd, environment);
      }

      // Store initial position
      double x0[3];  // initial spin position
      for (i = 0; i < dim; i++) {
        trajectory_hist[i][0] = 0.0; // x[i];
        x0[i] = x[i];
      }

      // Generate and store trajectory with multiple reflections
      unsigned int numjumps;
      for (numjumps = 0; numjumps < Njumps; numjumps++) {
          //if(numjumps<200 && ispin==0){  // DEBUGGING
          //    printf("%d [%6.5e %6.5e %6.5e] [%6.5e %6.5e %6.5e]\n",numjumps,x[0],x[1],x[2],trajectory_hist[0][numjumps], trajectory_hist[1][numjumps], trajectory_hist[2][numjumps]) ;
          //}

        // Draw random jump
        gen_point_sphere(dx, Ljump, &rng_state_thrd);
        add_arrays(xup, x, dx, dim);

        // Backup for potential debugging
        x_backup[0] = x[0]; x_backup[1] = x[1]; x_backup[2] = x[2];
        xup_backup[0] = xup[0]; xup_backup[1] = xup[1]; xup_backup[2] = xup[2];

        // Performed jump and reflections if any
        MP3D_fixed_jump(x, xup, cand_obj, environment);  // modifies x which becomes new reflected point, and xup which becomes garbage

        // [optional] Verify that spin remains extracellular
        if (MP3D_is_intra(&x, environment, &indpopin, cand_obj)) {
          printf("WARNING: SigSim_3D_sPGSE: Spin %d/%d detected in the intracellular space of population %d after jump %d/%d in thread %d of %d.\n",
            ispin + 1, Nspins, indpopin + 1, numjumps + 1, Njumps, omp_get_thread_num() + 1, omp_get_num_threads());
          printf("From x=[%10e %10e %10e] to xup=[%10e %10e %10e], ends up at xref=[%10e, %10e, %10e].\n",
            x_backup[0], x_backup[1], x_backup[2], xup_backup[0], xup_backup[1], xup_backup[2], x[0], x[1], x[2]);
          //printf("ABORTING.\n"); exit(-1);
        }

        // Store position relative to initial position to avoid numerical errors in computation of the final phase due to gradient offsets
        for (i = 0; i < dim; i++) {
          trajectory_hist[i][numjumps + 1] = x[i] - x0[i];  // subtract initial spin position
        }
      }

      // Spin dephasing for each sequence
      unsigned int iseq;
      for (iseq = 0; iseq < Nseqs; iseq++) {
        double G_i = scheme_matrix[iseq][3], Del_i = scheme_matrix[iseq][4], del_i = scheme_matrix[iseq][5];
        double g_i[3] = { scheme_matrix[iseq][0], scheme_matrix[iseq][1], scheme_matrix[iseq][2] };
        double t_j = 0.0;

        //double B1_ij = (G_i * scale_G)*(g_i[0] * (trajectory_hist[0][0] * scale_L) + g_i[1] * (trajectory_hist[1][0] * scale_L) + g_i[2] * (trajectory_hist[2][0] * scale_L));
        //phase_final[iseq][ispin] = 0.5*B1_ij; // trapezoid quadrature: beginning

        double phase_ij_x = 0.0, phase_ij_y = 0.0, phase_ij_z = 0.0;
        phase_ij_x = 0.5 * (trajectory_hist[0][0] * scale_L);
        phase_ij_y = 0.5 * (trajectory_hist[1][0] * scale_L);
        phase_ij_z = 0.5 * (trajectory_hist[2][0] * scale_L);

        char grad_on_j;

        for (j = 1; j < Njumps; j++) {
          t_j = j*dt;
          grad_on_j = (0.0 <= t_j && t_j < del_i) - (Del_i <= t_j && t_j <= (Del_i + del_i));

          //B1_ij = grad_on_j*(G_i*scale_G)* (g_i[0] * (trajectory_hist[0][j] * scale_L) + g_i[1] * (trajectory_hist[1][j] * scale_L) + g_i[2] * (trajectory_hist[2][j] * scale_L));
          //phase_final[iseq][ispin] += B1_ij; // trapezoid quadrature rule

          phase_ij_x += grad_on_j * (trajectory_hist[0][j] * scale_L);
          phase_ij_y += grad_on_j * (trajectory_hist[1][j] * scale_L);
          phase_ij_z += grad_on_j * (trajectory_hist[2][j] * scale_L);
        }
        t_j = Njumps*dt;
        grad_on_j = (0.0 <= t_j && t_j < del_i) - (Del_i <= t_j && t_j <= (Del_i + del_i));  // grad_on might be zero for shorter sequences simulated alongside longer sequences

        //B1_ij = grad_on_j*(G_i*scale_G)* (g_i[0] * (trajectory_hist[0][j] * scale_L) + g_i[1] * (trajectory_hist[1][j] * scale_L) + g_i[2] * (trajectory_hist[2][j] * scale_L));
        //phase_final[iseq][ispin] = (gam*scale_gam)*(dt*scale_T)*(phase_final[iseq][ispin] + 0.5*B1_ij); // trapezoid quadrature : end

        phase_ij_x = (gam*scale_gam) * (dt*scale_T) * (G_i * scale_G) * g_i[0] * (phase_ij_x + 0.5 * grad_on_j * (trajectory_hist[0][j] * scale_L));
        phase_ij_y = (gam*scale_gam) * (dt*scale_T) * (G_i * scale_G) * g_i[1] * (phase_ij_y + 0.5 * grad_on_j * (trajectory_hist[1][j] * scale_L));
        phase_ij_z = (gam*scale_gam) * (dt*scale_T) * (G_i * scale_G) * g_i[2] * (phase_ij_z + 0.5 * grad_on_j * (trajectory_hist[2][j] * scale_L));

        // If directional phases need to be returned
        if (return_phases) {
          phase_final_x[iseq][ispin] = phase_ij_x;
          phase_final_y[iseq][ispin] = phase_ij_y;
          phase_final_z[iseq][ispin] = phase_ij_z;
        }
        else { // phase_final not initialized if directional phases are returned to save about 25percent of memory
          phase_final[iseq][ispin] = phase_ij_x + phase_ij_y + phase_ij_z;
        }
      }

      // Display progression on standard output
      if (ispin % spin_display_interval == 0) {
        printf("Spin %d done in thread %d out of %d.\n", ispin, omp_get_thread_num() + 1, omp_get_num_threads());
      }
    } // end of Monte Carlo for-loop
    free(rng_state_thrd);
  }// end of parallel region

  // Compute final signal after MC simulation
  for (i = 0; i<Nseqs; i++) {
    E_cos_phase[i] = 0.0;
    for (j = 0; j<Nspins; j++) {
      if (return_phases) {
        E_cos_phase[i] += cos(phase_final_x[i][j] + phase_final_y[i][j] + phase_final_z[i][j]);
      }
      else {
        E_cos_phase[i] += cos(phase_final[i][j]);
      }
    }
    E_cos_phase[i] /= Nspins;
  }
}

/*
 Single-thread, low memory Monte Carlo simulation routine. 
 Modifies the Nseq x 1 array E_cos_phase containing the final signal values. Final phase is updated online, spin after spin.
 Only stores the final E_cos_phase array and a Nsteps x 3 array containning the trajectory of the current random walker.
 Uses spatial sub-gridding to check for intersection (MP3D_is_intra and MP3D_get_sector_objects, which are thread-safe).
*/
void SigSim_3D_sPGSE_singlethread(
    const unsigned int seed, double * const * const scheme_matrix, MP3D_env * const environment, const unsigned int Nspins,
    const unsigned int Njumps, const unsigned int Nseqs, const double dt, const double Ljump, const unsigned int spin_display_interval, double * const E_cos_phase,
    const bool return_phases, std::vector<std::vector<double> > & phase_final_x, std::vector<std::vector<double> > & phase_final_y, std::vector<std::vector<double> > & phase_final_z) {

  // Initialize random number generator
  init_genrand(seed);  // TODO make it re-entrant as in multi-threading

  // Initialize buffer for objects to check 
  std::vector<unsigned int> cand_obj(environment->totobjects, 0); // size [totobjects] buffer to hold candidate objects for intersection at every jump, preallocated once and for all


  // Declare necessary variables
  const unsigned char dim = 3;
  unsigned int i, j, ispin, iseq;
  double phase_final;  // buffer variable below

  //Prepare array containing trajectory history
  std::vector < std::vector <double> > trajectory_hist(dim, std::vector <double>(Njumps + 1, 0.0)); // dim-by-(Njumps+1) array storing RELATIVE position of spin taken from initial point

  double x[3], dx[3], xup[3];
  double x_backup[3], xup_backup[3];  // buffers for x and xup for debugging in case the jump leads to an undesired crossing
  unsigned int indpopin;  // will store population index containing spin if intracellular, -1 if extracellular
  double x0[3];  // initial spin position
  // Monte Carlo loop over spins
  for (ispin = 0; ispin< Nspins; ispin++) {
    // Initial drop
    MP3D_drop_particle_initialized_sgthrd(x, environment);  // modifies x, TODO : use MP3D_drop_particle_initialized as in multi-thread re-entrant version
    while (MP3D_is_intra(&x, environment, &indpopin, cand_obj)) {
      MP3D_drop_particle_initialized_sgthrd(x, environment);
    }

    for (i = 0; i<dim; i++) {
      trajectory_hist[i][0] = 0.0; // formerly x[i], store position of spin relative to initial position to avoid round-off errors due to gradient offsets
      x0[i] = x[i];
    }
    // Generate discrete trajectory
    unsigned int numjumps;
    for (numjumps = 0; numjumps<Njumps; numjumps++) {
        //if (numjumps<200 && ispin == 0) { // DEBUGGING
        //  printf("%d [%6.5e %6.5e %6.5e] [%6.5e %6.5e %6.5e]\n", numjumps, x[0], x[1], x[2], trajectory_hist[0][numjumps], trajectory_hist[1][numjumps], trajectory_hist[2][numjumps]);
        //}
      gen_point_sphere_sgthrd(dx, Ljump);  // modifies dx, TODO: make it renentrant too
      add_arrays(xup, x, dx, dim);  // modifies xup

      // Backup for potential debugging
      x_backup[0] = x[0]; x_backup[1] = x[1]; x_backup[2] = x[2];
      xup_backup[0] = xup[0]; xup_backup[1] = xup[1]; xup_backup[2] = xup[2];

      MP3D_fixed_jump(x, xup, cand_obj, environment);  // modifies x and xup (xup ends up being garbage)

      if (MP3D_is_intra(&x, environment, &indpopin, cand_obj)) {  // [optional] Verify that spin remains extracellular
        printf("WARNING: SigSim_3D_sPGSE: Spin %d/%d detected in the intracellular space of population %d after jump %d/%d.\n", ispin + 1, Nspins, indpopin + 1, numjumps + 1, Njumps);
        printf("From x=[%10e, %10e, %10e] to xup=[%10e, %10e, %10e], ends up at xref=[%10e %10e %10e].\n",
          x_backup[0], x_backup[1], x_backup[2], xup_backup[0], xup_backup[1], xup_backup[2], x[0], x[1], x[2]);
        //printf("ABORTING.\n"); exit(-1);
      }

      for (i = 0; i<dim; i++) {
        trajectory_hist[i][numjumps + 1] = x[i] - x0[i]; // store RELATIVE position from origin of trajectory
      }
    }

    // Compute spin dephasing for each sequence
    for (iseq = 0; iseq<Nseqs; iseq++) {
      double G_i = scheme_matrix[iseq][3], Del_i = scheme_matrix[iseq][4], del_i = scheme_matrix[iseq][5];
      double g_i[3] = { scheme_matrix[iseq][0], scheme_matrix[iseq][1], scheme_matrix[iseq][2] };
      double t_j = 0.0;

      double phase_ij_x = 0.0, phase_ij_y = 0.0, phase_ij_z = 0.0;
      phase_ij_x = 0.5 * (trajectory_hist[0][0] * scale_L); // trapezoid quadrature: beginning
      phase_ij_y = 0.5 * (trajectory_hist[1][0] * scale_L);
      phase_ij_z = 0.5 * (trajectory_hist[2][0] * scale_L);

      char grad_on_j = 1;

      //double B1_ij = grad_on_j*G_i*(g_i[0] * trajectory_hist[0][0] + g_i[1] * trajectory_hist[1][0] + g_i[2] * trajectory_hist[2][0]);
      //phase_final = 0.5*B1_ij; // trapezoid quadrature: beginning

      for (j = 1; j<Njumps; j++) {
        t_j = j*dt;
        grad_on_j = (0.0 <= t_j && t_j<del_i) - (Del_i <= t_j && t_j <= (Del_i + del_i));

        //B1_ij = grad_on_j*G_i* (g_i[0] * trajectory_hist[0][j] + g_i[1] * trajectory_hist[1][j] + g_i[2] * trajectory_hist[2][j]);
        //phase_final += B1_ij; // trapezoid quadrature rule

        phase_ij_x += grad_on_j * (trajectory_hist[0][j] * scale_L);
        phase_ij_y += grad_on_j * (trajectory_hist[1][j] * scale_L);
        phase_ij_z += grad_on_j * (trajectory_hist[2][j] * scale_L);
      }
      t_j = Njumps*dt;
      grad_on_j = (0.0 <= t_j && t_j<del_i) - (Del_i <= t_j && t_j <= (Del_i + del_i));

      //B1_ij = grad_on_j*G_i* (g_i[0] * trajectory_hist[0][j] + g_i[1] * trajectory_hist[1][j] + g_i[2] * trajectory_hist[2][j]);
      //phase_final = gam*dt*(phase_final + 0.5*B1_ij); // trapezoid quadrature : end

      phase_ij_x = (gam*scale_gam) * (dt*scale_T) * (G_i * scale_G) * g_i[0] * (phase_ij_x + 0.5 * grad_on_j * (trajectory_hist[0][j] * scale_L));
      phase_ij_y = (gam*scale_gam) * (dt*scale_T) * (G_i * scale_G) * g_i[1] * (phase_ij_y + 0.5 * grad_on_j * (trajectory_hist[1][j] * scale_L));
      phase_ij_z = (gam*scale_gam) * (dt*scale_T) * (G_i * scale_G) * g_i[2] * (phase_ij_z + 0.5 * grad_on_j * (trajectory_hist[2][j] * scale_L));
      phase_final = phase_ij_x + phase_ij_y + phase_ij_z;

      E_cos_phase[iseq] = ((double)ispin*E_cos_phase[iseq] + cos(phase_final)) / ((double)ispin + 1.0); // expectation computed online

      // If directional phases need to be returned
      if (return_phases) {
        phase_final_x[iseq][ispin] = phase_ij_x;
        phase_final_y[iseq][ispin] = phase_ij_y;
        phase_final_z[iseq][ispin] = phase_ij_z;
      }
    }
    if (ispin % spin_display_interval == 0) {
      printf("Spin %d done.\n", ispin);
    }
  } // end for ispin, end of phase computations
}
