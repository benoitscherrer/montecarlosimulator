#ifndef MP3D_MYMATHS_H
#define MP3D_MYMATHS_H

//#ifndef M_PI
//#define M_PI 3.14159265359
//#endif

void cumsum(unsigned int* res, unsigned int* array, int size) ;
void cumsum_m1(unsigned int* res, unsigned int* array, int size) ;
void cross_3D(double* outarray, double* a, double* b) ;
double largest(double* a, int length) ;
double smallest(double* a, int length) ;
void add_arrays(double* out, double* a, double* b, int length) ;
double sum_array(double* a, int length) ;
double norm(double* v, int length);
double real_lcm(double a,double b, double tol) ;
void gen_point_sphere(double* x, double l, unsigned long** rng_state) ;
void gen_point_sphere_sgthrd(double* x, double l);
char signum(double x) ;

#endif