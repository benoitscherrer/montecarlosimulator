#ifndef MP3D_MP3D_H
#define MP3D_MP3D_H

#include <vector> 

enum EnumPopulationTypes {
  kCylinder = 0,
  kSphere = 1
};
typedef enum EnumPopulationTypes PopulationTypes;

// String representations of population types
const char kTypeNames[2][15] = { "cylinder", "sphere" };

// Basic representation of a 3D multi-population environment
// TODO: convert this to a class with data members and member functions
//     (the code was originally pure C but that is no longer so)
struct MP3D_envs {
  unsigned int numpop;
  unsigned int* popsizes;        // size [numpop]
  PopulationTypes* poptypes;    // size [numpop], formerly unsigned char
  unsigned int totobjects;
  unsigned int* obj_start_ind;  // size [numpop]
  unsigned int* obj_end_ind;    // size [numpop]
  double* obj_pile_pos;          // size [totobjects]
  double* obj_perp_pos;          // size [totobjects]
  double* obj_cdir_pos;          // size [totobjects]
  double* radii;                // size [totobjects]
  double* cell_pile_sizes;      // size [numpop]
  double* cell_perp_sizes;      // size [numpop]
  double* cell_cdir_sizes;      // size [numpop]
  double* off_pile;              // size [numpop]
  double* off_perp;              // size [numpop]
  double* off_cdir;              // size [numpop]
  double piling_dir[3];
  double* perpdir;              // size [3*numpop]
  double* cdir;                  // size [3*numpop]
  double pile_period;

  // After initialization:
  double* noncolinear_cdirs;    // size [<= (3*numpop)]
  double* noncolinear_pdirs;    // size [<= (3*numpop)]
  double* perpdists;            // size [<= numpop]
  double* cdirdists;            // size [<= numpop] to account for sphere populations
  unsigned int num_noncolinear_cdirs;

  // After initialization and once jump length is known, spatial sub-gridding
  std::vector<double> lperp;        // size [numpop], length of sector along perpendicular direction
  std::vector<double> lpile;        // size [numpop], length of sector along piling direction
  std::vector<double> lcdir;        // size [numpop], length of sector along cdir/main direction
  std::vector<unsigned int> Nperp;  // size [numpop], number of sectors along perpendicular direction
  std::vector<unsigned int> Npile;  // size [numpop], number of sectors along piling direction
  std::vector<unsigned int> Ncdir;  // size [numpop], number of sectors along cdir/main direction
  
  std::vector<                 // all populations
    std::vector<               // sectors along perpendicular direction
    std::vector<               // sectors along piling direction
    std::vector<               // sectors along cdir/main direction 
    std::vector<unsigned int>  // indices of all objects in given sector 
    >
    >
    >
  > obj_spatial_distribution;    // size [numpop]. For all i, 
                                // obj_spatial_distribution[i] 
                                // has size [Nperp(i), Npile(i), Ncdir(i)] and
                                // contains a variable-length list of indices 
};
typedef struct MP3D_envs MP3D_env;


struct PackedPoreSubstrateParams {
  // Lattice size, always set
  double lattice_size;
  // Are pore positions and radii specified in a text file?
  bool from_file;
  // If so, what is that text file?
  char* porefilepath;
  // If not, provide array with pore positions and radii...
  double** pore_array;
  // ... and number of lines in array (4 columns are assumed)
  int num_lines;
};

// Environment constructors:
// TODO turn these into derived classes of base class 3D env
void MP3D_create_environment(MP3D_env* env, unsigned char scenario);
void MP3D_create_environment_free_diffusion(MP3D_env* env);
void MP3D_create_environment_crossing(MP3D_env* env, double r1, double r2,
                                      double e1, double e2, double e_l,
                                      double rotangle, unsigned int n1,
                                      unsigned int n2);
void MP3D_create_environment_dispersion(MP3D_env* env, double angle_max,
                                        unsigned int numpop, double rad,
                                        double fin_tot);
void MP3D_create_environment_hexpack(MP3D_env* env, double r, double f);
void MP3D_create_environment_heterpack(MP3D_env* env,
                                       struct PackedPoreSubstrateParams p);
void MP3D_create_environment_sphere_heterpack(
    MP3D_env* env, struct PackedPoreSubstrateParams p);

// Initialize elementary polyhedron representing environment.
// MUST be called before MP3D_drop_particle_initialized[_sgthrd] is called.
void MP3D_compute_generator(MP3D_env* env);

// Initialize spatial sub-gridding of each population.
// Must be called before any call to MP3D_fixed_jump once jump length is known.
void MP3D_initialize_spatial_sectors(MP3D_env* env, double Ljump);

// Destructor (free allocated memory)
void MP3D_clean_env(MP3D_env* env);

// Performs sanity checks on environment after creation
unsigned char MP3D_check_consistency(MP3D_env* subinfo);


void MP3D_get_fic(double* fic_tot, double* fic_pops, MP3D_env* env);


double MP3D_get_pop_fic(unsigned int popID, MP3D_env* env);

// Check if spin is intracellular.
// Returns: 
// - 0 if x extracellular, 1 if x intracellular
// Requires: 
// - call to MP3D_initialize_spatial_sectors on env
// - allocation of cand_obj to hold at least env->totobjects elements 
//   in CURRENT thread for thread safety
// Modifies:
// - indpopin: -1 if x is extracellular, index of population x is in otherwise
unsigned char MP3D_is_intra(double (*x)[3], MP3D_env* env,
    unsigned int* indpopin, std::vector<unsigned int>& cand_obj);

// Print environment description.
void MP3D_print_info(MP3D_env* myenv);

// Get objects potentially intersected in population popID during jump 
// from x to xup.
// Requires:
// - call to MP3D_initialize_spatial_sectors on subinfo
// - allocation of cand_obj to hold at least env->totobjects elements 
//   in CURRENT thread for thread safety
// Modifies:
// - Ncand: number of detected candidate objects
void MP3D_get_sector_objects(MP3D_env* subinfo, unsigned int & Ncand,
    unsigned int popID, double x_perp, double x_pile, double x_cdir,
    double xup_perp, double xup_pile, double xup_cdir,
    std::vector<unsigned int> & cand_obj);

// Uniform particle dropping in environment for multi-threading.
// Requires:
// - initializing re-entrant MT RNG state with init_genrand(seed, rng_state)
//   after allocating rng_state in CURRENT thread
// - call to MP3D_compute_generator on env
void MP3D_drop_particle_initialized(double* x, unsigned long** rng_state,
    MP3D_env* env);

// Uniform particle dropping in environment (not thread-safe).
// Requires:
//  - initializing the MT RNG state with init_genrand(seed)
//    or init_by_array(init_key, key_length)
//  - call to  MP3D_compute_generator on env
void MP3D_drop_particle_initialized_sgthrd(double* x, MP3D_env* env);

// Compute reflection off object objID in population popID closest to x
// and figure out reflected point x_refl. 
void MP3D_reflection(double* x, double* xup,
    double x_perp, double x_pile, double x_cdir,
    double xup_perp, double xup_pile, double xup_cdir,
    unsigned char* crossing, double* x_refl, double* lam_inters,
    unsigned int popID, unsigned int objID, MP3D_env* subinfo);

// Performs fixed jump from x to xup in environment subinfo.
// Returns: 
// - 1 for crossing, 0 if no crossing detected
// Requires:
// - call to MP3D_initialize_spatial_sectors on subinfo
// - allocation of cand_obj to hold at least subinfo->totobjects elements
//   in CURRENT thread for thread safety
// Modifies:
// - x, which becomes reflected point
// - xup, which eventually becomes garbage
unsigned char MP3D_fixed_jump(double* x, double* xup,
    std::vector<unsigned int>& cand_obj, MP3D_env* subinfo) ;

#endif

// TODO: get rid of MP3D_drop_particle_initialized_sgthrd 
//       (which requires editing SigSim_3D_sPGSE_singlethread)