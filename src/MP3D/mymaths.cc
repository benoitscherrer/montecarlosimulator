// for line number in memory reports by _CrtDumpMemoryLeaks();
// must be placed in every .c[xx] file
#define _CRTDBG_MAP_ALLOC
#define _USE_MATH_DEFINES

#include "mymaths.h"  // prototypes of functions defined below

#include <math.h>  // M_PI, sqrt, sin, cos ! -lm in compilation command needed
#include <stdio.h>  // printf
#include <stdlib.h>  // exit

#include "RNG.h"  // MT19937 rng , single-thread only
#include "RNG_r.h"  // re-entrant MT19937 RNG, thread-safe

/*
res and array must have the same length
*/
void cumsum(unsigned int* res, unsigned int* array, int size) {
  int i;
  unsigned int acc = 0;
  for (i = 0; i < size; i++) {
    acc += array[i];
    res[i] = acc;
  }
}
/*
Cumsum with a minus 1 performed on each entry of the output array
*/
void cumsum_m1(unsigned int* res, unsigned int* array, int size) {
  int i;
  unsigned int acc = 0;
  for (i = 0; i < size; i++) {
    acc += array[i];
    res[i] = acc -1;
  }
}
/*
Cross product in 3D
*/
void cross_3D(double* outarray, double* a, double* b) {
  outarray[0] = a[1]*b[2] - a[2]*b[1];
  outarray[1] = a[2]*b[0] - a[0]*b[2];
  outarray[2] = a[0]*b[1] - a[1]*b[0];
}
/*
Largest element in an array
*/
double largest(double* a, int length) {
  double maxval = a[0];
  int i;
  for (i = 1; i < length; i++) {
    if (a[i] > maxval) {
      maxval = a[i];
    }
  }
  return maxval;
}
/*
Smallest element in an array (i.e. element closest to negative infinity)
*/
double smallest(double* a, int length) {
  double minval = a[0];
  int i;
  for (i = 1; i < length; i++) {
    if (a[i] < minval) minval = a[i];
  }
  return minval;
}
/*
Sum of arrays
*/
void add_arrays(double* out, double* a, double* b, int length) {
  int i; 
  for (i = 0; i < length; i++) {
    out[i] = a[i] + b[i];
  }
}
/*
Sum of elements of an array
*/
double sum_array(double* a, int length) {
  double mysum = a[0];
  int i;
  for(i = 1; i < length ; i++) {
    mysum += a[i];
  }
  return mysum;
}
/*
Norm of a vector 
*/
double norm(double* v, int length) {
  double res = v[0]*v[0];
  int i; 
  for (i = 1; i < length; i++) {
    res += v[i]*v[i];
  }
  return sqrt(res);
}

/*
Least Common Multiple for two non-negative real numbers a and b.

returns the smallest non-negative number lcm such that
lcm = ka*a = (kb+beta)*b,
where ka and kb are positive integers and beta is a real number in [0,1[, 
with ka and kb selected such that 
beta/kb < tol.

Boundary cases:
real_lcm(0, a) = real_lcm(a, 0) = max(0, a) for any a > 0
real_lcm(0, 0) = 0

Inputs : 
- a,b : two non-negative real numbers
- tol : positive real number. A typical value is 1e-2.
*/
double real_lcm(double a, double b, double tol) {

  if (a < 0 || b < 0) {
    printf("real_lcm: input arguments a and b should be non-negative."
           " Detected %g and %g. ABORTING.\n", a, b);
    exit(-1);
  }

  // set a as max(a,b) and b as min(a,b)
  if (a < b) {
    double tmp = a;
    a = b;
    b = tmp;
  }
  // Could be a do-while loop
  double ka = 1.0;
  double kb = floor(ka*a/b);
  double ratio = (ka*a/b - kb)/kb;  // sort of a relative ratio 
  while (fabs(ratio-round(ratio)) >= tol) {
    ka = ka+1;
    kb = floor(ka*a/b);
    ratio = (ka*a/b - kb)/kb;
  }
  return a*ka;
}

/* 
Generate a point from a uniform distribution on the 3D spherical shell
B[0,l]\B(0,l)
*/
void gen_point_sphere_old(double* x, double l, unsigned long** rng_state) {
  double x1 = 2.0*genrand_res53_r(rng_state) - 1.0;
  double x2 = 2.0*genrand_res53_r(rng_state) - 1.0;
  double ssq = x1*x1 + x2*x2;
  while (ssq > 1) {
    x1 = 2.0*genrand_res53_r(rng_state) - 1.0;
    x2 = 2.0*genrand_res53_r(rng_state) - 1.0;
    ssq = x1*x1 + x2*x2;
  }
  double rad = 2.0*l*sqrt(1.0-ssq);
  x[0] = x1*rad;
  x[1] = x2*rad;
  x[2] = l*(1.0-2.0*ssq);
}

/*
Generate a point from a uniform distribution on the 3D spherical shell
B[0,l]\B(0,l)
Version not requiring a while-loop but using external sin and cos functions.
Uses thread-safe, re-entrant random number generator.
*/
void gen_point_sphere(double* x, double l, unsigned long** rng_state) {
  double th = 2.0 * genrand_res53_r(rng_state) * M_PI;
  double u = 2.0 * genrand_res53_r(rng_state) - 1.0;
  double tmp = l*sqrt(1.0 - u*u);
  x[0] = tmp*cos(th);
  x[1] = tmp*sin(th); 
  x[2] = l*u;
}

/*
Generate a point from a uniform distribution on the 3D spherical shell
B[0,l]\B(0,l)
Version not requiring a while-loop but using external sin and cos functions.
Suitable for single-thread execution only.
*/
void gen_point_sphere_sgthrd(double* x, double l) {
  double th = 2.0 * genrand_res53() * M_PI;
  double u = 2.0 * genrand_res53() - 1.0;
  double tmp = l*sqrt(1.0 - u*u);
  x[0] = tmp*cos(th);
  x[1] = tmp*sin(th);
  x[2] = l*u;
}

/*
Signum function. Returns +1, 0 or -1 if number x is positive, zero or negative
*/
char signum(double x) {
    return (x > 0) - (x < 0);
}
