#ifndef RNG_R_H
#define RNG_R_H

int get_mt19337_state_size();
void init_genrand_r(unsigned long s, unsigned long** mt);
unsigned long genrand_int32_r(unsigned long** mt);
long genrand_int31_r(unsigned long** mt);
double genrand_real1_r(unsigned long** mt);
double genrand_real2_r(unsigned long** mt);
double genrand_real3_r(unsigned long** mt);
double genrand_res53_r(unsigned long** mt);

#endif