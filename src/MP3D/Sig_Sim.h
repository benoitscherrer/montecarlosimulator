#ifndef MP3D_SIG_SIM_H
#define MP3D_SIG_SIM_H

#include <vector>
#include <string>

#include "MP3D.h"

void SigSim_3D_sPGSE(
    const unsigned int seed, char* schemefilename,
    const std::string & outputfile_str,
    MP3D_env* environment,
    unsigned int Nspins, unsigned int Njumps, const double D,
    const bool multithread_on, const bool return_phases);

void SigSim_3D_sPGSE_multithread(
    const unsigned int seed,
    double * const *  const scheme_matrix,
    MP3D_env * const environment,
    const unsigned int Nspins,
    const unsigned int Njumps,
    const unsigned int Nseqs, const double dt,
    const double Ljump,
    const unsigned int spin_display_interval,
    double * const E_cos_phase,
    const bool return_phases,
    std::vector<std::vector<double> > & phase_final_x,
    std::vector<std::vector<double> > & phase_final_y,
    std::vector<std::vector<double> > & phase_final_z);

void SigSim_3D_sPGSE_singlethread(
    const unsigned int seed,
    double * const * const scheme_matrix,
    MP3D_env * const environment,
    const unsigned int Nspins, 
    const unsigned int Njumps,
    const unsigned int Nseqs, const double dt,
    const double Ljump,
    const unsigned int spin_display_interval,
    double * const E_cos_phase,
    const bool return_phases,
    std::vector<std::vector<double> > & phase_final_x,
    std::vector<std::vector<double> > & phase_final_y,
    std::vector<std::vector<double> > & phase_final_z);
#endif
