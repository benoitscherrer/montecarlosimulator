// get line number in memory reports by _CrtDumpMemoryLeaks(); in Visual Studio
#define _CRTDBG_MAP_ALLOC
// stop Visual Studio from issuing warnings about fopen being unsafe
#define _CRT_SECURE_NO_WARNINGS

#include "tools.h" 

#include <stdio.h>  // printf, FILE, fread(), fwrite(),
#include <stdlib.h> // malloc, calloc, size_t via stddef.h
#include <vector>    // C++ vector 

/*
Count number of lines in a file
*/
unsigned int countlines(char *filename) {
  FILE *fp = fopen(filename,"r");
  int ch=0;
  unsigned int lines=0;

  if (!fp) {
    printf("Could not open file %s in function countlines.\nABORTING.\n",
           filename);
    perror("Error detail");
    exit(-1);
  }

  while(!feof(fp)) {
    ch = fgetc(fp);
    if(ch == '\n' || feof(fp)) {
      lines++;
    }
  }
  fclose(fp);
  return lines;
}
/*
Read a schemefile and import data into a two-dimensional,
dynamically-allocated array of dimension Nseq-by-7
where each line has the format [gx gy gz G Del del TE ] in SI units.
The first line of the file must be the header "VERSION: 1"

Note to self: This HAS to use dynamic allocation through malloc otherwise
ownership won't extend outside the scope of the function.

*/
double** import_scheme_alloc(char* filepath, unsigned int* numseq) {
  // Estimate number of lines in file first (header included):
  unsigned int num_lines_file = countlines(filepath);

  FILE *schemefile;
  schemefile = fopen(filepath, "r");
  if (!schemefile){
    printf("Could not open schemefile %s after counting lines.\nABORTING.\n",
           filepath);
    perror("Error detail");
    exit(-1) ;
  }
  // Skip header line and verify that it is "VERSION: 1" 
  // (beware of C's null string termination)
  // buffer size at least one character greater than the length of the
  // string in "visible" characters for the terminating null character
  // to go
  char ref_header[11] = {'V','E','R','S','I','O','N',':',' ','1'};
  int hdr_cnt = 0;
  // use int instead of unsigned char c because fgetc might return the larger
  // EOF integer
  int c = fgetc(schemefile);

  if (c == '\n' || c == '\r') {
    printf("Schemefile %s does not seem to have a header as first "
           "line.\nABORTING.\n", filepath);
    exit(-1);
  }

  while (c != '\n' && c != '\r') {
    if (c != ref_header[hdr_cnt]) {
      printf("After parsing its first %d characters, schemefile %s "
             "does not seem to be of the type %s.\nABORTING.\n",
             hdr_cnt + 1, filepath, ref_header);
      exit(-1);
    }
    hdr_cnt++;
    c = fgetc(schemefile);
  }

  // Dynamically allocate output array, assuming fixed number
  // of entries per line
  int entry_per_line = 7;
  double** scheme_array;
  // excludes header but might include ghost lines near end of file
  scheme_array = (double**)malloc((num_lines_file-1)*sizeof(double*));
  unsigned int i; 
  for (i=0; i < num_lines_file-1; i++) {
    scheme_array[i] = (double*)malloc(entry_per_line*sizeof(double));
  }

  // Browse through file until end of file reached
  int entry_count = 0;
  while(! feof(schemefile)) {
    //%lf expects pointer to double
    fscanf(schemefile, "%lf",
            &scheme_array[entry_count/entry_per_line]
            [entry_count%entry_per_line]);
    entry_count ++;
  }
  fclose(schemefile);
  // actual number of sequences without ghost line effect !
  *numseq = (entry_count/entry_per_line );  // integer division

  // Free the memory allocated for ghost lines
  if (*numseq < (num_lines_file - 1)) {
    for (i = *numseq; i < (num_lines_file - 1); i++) {
      free(scheme_array[i]);
    }
    double** tmp_realloc = 
        (double**)realloc(scheme_array, (*numseq) * sizeof(double*));
    if (tmp_realloc) {
      scheme_array = tmp_realloc;
    }
    else {
      printf("ERROR: import_scheme_alloc: memory reallocation failed after"
             " removing ghost lines. ABORTING.\n");
      perror("Error detail");
      exit(-1);
    }
  }
  return scheme_array;
}

/*
Read text file containing N-by-4 entries, typically sphere or cylinder file
*/
double** import_porefile_alloc(char* filepath, unsigned int* num_pores) {
  // Estimate number of lines in file first:
  unsigned int num_lines_file = countlines(filepath);

  FILE *porefile;
  porefile = fopen(filepath, "r");
  if (!porefile) {
    printf("Could not open pore file %s.\nABORTING.\n", filepath);
    perror("Error detail");
    exit(-1);
  }

  // Dynamically allocate output array:
  unsigned int entry_per_line = 4;
  double** pore_array;  // might include ghost lines near end of file
  pore_array = (double**)malloc(num_lines_file * sizeof(double*));
  unsigned int i;
  for (i = 0; i < num_lines_file; i++) {
    pore_array[i] = (double*)malloc(entry_per_line * sizeof(double));
  }
  // TODO: detect if lattice size was provided on first line of text file,
  // assign it accordingly

  // Browse through file until end of file reached
  unsigned int entry_count = 0;
  while (!feof(porefile)) {
    // %lf expects pointer to double
    fscanf(porefile, "%lf",
           &pore_array[entry_count / entry_per_line]
           [entry_count%entry_per_line]);
    entry_count++;
  }
  fclose(porefile);
  // actual number of sequences without ghost line effect
  *num_pores = (entry_count / entry_per_line);  // integer division
  // Free the memory allocated for ghost lines
  if (*num_pores < num_lines_file) {
    for (i = *num_pores; i < num_lines_file; i++) {
      free(pore_array[i]);
    }
    double** tmp_realloc = (double**)realloc(pore_array,
                                             (*num_pores) * sizeof(double*));
    if (tmp_realloc) {
      pore_array = tmp_realloc;
    }
    else {
      printf("ERROR: import_porefile_alloc: memory reallocation failed "
             "after removing ghost lines. ABORTING.\n");
      perror("Error detail");
      exit(-1);
    }
  }
  return pore_array;
}

/*
Swap byte order of an 8-byte double (for conversion from little
to big endian for instance)
*/
double swap_double_64(double d) {
  double revd;
  char* c = (char*)&d;
  char* prev = (char*)&revd;
  int i;
  for (i=0; i<8; i++) {
    prev[i] = c[7-i];
  }
  return revd;
}

/*
Return 1 if platform is detected as big-endian, 0 otherwise
*/
unsigned int is_bigendian() {
  // Note to self: I could also add  #include <limits.h> and
  //  #if UINT_MAX < 16 #error "unsupported unsigned int size"  #endif
  unsigned int i = 1; 
  return (*(char*)&i) == 0;
}

/*
Write 2D data row per row in file filename
*/
void write_array_to_file(const std::vector<std::vector<double> > & data,
                         const char * const filename) {
  unsigned int i, j;
  double swapped_double;

  FILE* ptr_file = fopen(&(filename[0]), "wb");
  if (!ptr_file) {
    printf("Unable to open file %s. ABORTING.\n", &(filename[0]));
    perror("Error detail");
    exit(-1);
  }
  for (i = 0; i < data.size(); i++) {
    for (j = 0; j < data[0].size(); j++) {
      // Write to output binary file in big-endian format
      if (is_bigendian()) {
        fwrite(&(data[i][j]), sizeof(data[i][j]), 1, ptr_file);
      }
      else {
        swapped_double = swap_double_64(data[i][j]);
        fwrite(&swapped_double, sizeof(swapped_double), 1, ptr_file);
      }
    }
  }
  fclose(ptr_file);
}

void print_cmd_to_log_file(int argc, char* const argv[],
                           const std::string & outfile) {
  std::string logfile = outfile + "_cmd.txt";

  FILE* ptr_file = fopen(&(logfile[0]), "w");
  if (!ptr_file) {
    printf("Unable to open file %s. ABORTING.\n", &(logfile[0]));
    perror("Error detail");
    exit(-1);
  }
  int i;
  for (i = 0; i < argc; i++) {
    fprintf(ptr_file, "%s ", argv[i]);
  }
  fclose(ptr_file);
}

// From
// https://stackoverflow.com/questions/4901999/c-equivalent-of-matlabs-fileparts-function/4902056#4902056 
// by jtbr on Jun 29, 2018, at 14:59
//"! Using only text manipulation, splits a full path (either Unix or Windows
// style) into component file parts"
//
// Returns the path name, file name, and extension for the specified file
// in a Fileparts structure containing fields
// - path (including the final file separator)
// - name
// - ext(including the dot)
FileParts fileparts(const std::string &fullpath) {
  using namespace std;

  size_t idxSlash = fullpath.rfind("/");  // rensonnetg: Linux separator
  if (idxSlash == string::npos) {
    idxSlash = fullpath.rfind("\\");  // rensonnetg: Windows separator
  }
  // rensonnetg: if not found, string::npos is returned, i.e. largest size_t
  size_t idxDot = fullpath.rfind(".");

  FileParts fp;
  // rensonnetg: note about string::substr : 
  // If pos argument is greater than the string length, it throws out_of_range.
  if (idxSlash != string::npos && idxDot != string::npos) {
    // rensonnetg: fsep and extension dot were found
    fp.path = fullpath.substr(0, idxSlash + 1);
    fp.name = fullpath.substr(idxSlash + 1, idxDot - idxSlash - 1);
    fp.ext = fullpath.substr(idxDot);
  }
  else if (idxSlash == string::npos && idxDot == string::npos) {
    // rensonnetg: no fsep and no extension dot
    fp.name = fullpath;
  }
  else if (/* only */ idxSlash == string::npos) {
    // rensonnetg: no fsep but an extension dot
    fp.name = fullpath.substr(0, idxDot);
    fp.ext = fullpath.substr(idxDot);
  }
  else { // only idxDot == string::npos
    // rensonnet: fsep found but no extension dot
    fp.path = fullpath.substr(0, idxSlash + 1);
    fp.name = fullpath.substr(idxSlash + 1);
  }
  return fp;
}