#ifndef TOOLS_H
#define TOOLS_H
#include <string> // C++ strings
#include <vector> // C++ vector 
unsigned int countlines(char *filename);
double** import_scheme_alloc(char* filepath, unsigned int* numseq);
double** import_porefile_alloc(char* filepath, unsigned int* numcyls);
double swap_double_64(double d);
unsigned int is_bigendian();
// ! on gcc mandatory to put a space between > >  within a nested template
// argument list
void write_array_to_file(
    const std::vector< std::vector<double> > & data,
    const char * const filename);
void print_cmd_to_log_file(
    int argc, char* const argv[], const std::string& outfile);

// From https://stackoverflow.com/questions/4901999/c-equivalent-of-matlabs-fileparts-function/4902056#4902056 
// by jtbr on Jun 29, 2018, at 14:59
struct FileParts {
  std::string path; // containing folder if provided, including trailing slash
  std::string name; // base file name, without extension
  std::string ext;  // extension, including '.'
};

FileParts fileparts(const std::string & fullpath);
#endif