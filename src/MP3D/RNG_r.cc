/*
A C-program for MT19937 with re-entrant implementation by Gaetan
Rensonnet on Feb 3, 2017, from the original Jan 26, 2002 code by
Takuji Nishimura and Makoto Matsumoto.

This implementation is given the state of the random generator as
an argument, which makes it thread-safe "with respect to the system", 
i.e. it is safe to call the function concurrently from multiple 
threads, provided that the different calls operate on different data, 
just like POSIX's rand_r or memcpy.

Before using, initialize the state by using init_genrand(seed). Note 
that array initialization has been removed in this implementation.
Also note that failing to call init_genrand_r_alloc before calling any 
other function will result in undetermined outcome, most likely a 
segmentation fault.

Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. The names of its contributors may not be used to endorse or promote
products derived from this software without specific prior written
permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
// for line number in memory reports by _CrtDumpMemoryLeaks();
// must be placed in every .c[xx] file
#define _CRTDBG_MAP_ALLOC

#include "RNG_r.h"

/* Period parameters */
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

int get_mt19337_state_size() {
  return N + 1;
}

/* initializes state mt with a seed */
void init_genrand_r(unsigned long s, unsigned long** mt)
{
  (*mt)[0] = s & 0xffffffffUL;
  unsigned long mti;
  for (mti = 1; mti<N; mti++) {
    (*mt)[mti] =
      (1812433253UL * ((*mt)[mti - 1] ^ ((*mt)[mti - 1] >> 30)) + mti);
    /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
    /* In the previous versions, MSBs of the seed affect   */
    /* only MSBs of the array mt[].                        */
    /* 2002/01/09 modified by Makoto Matsumoto             */
    (*mt)[mti] &= 0xffffffffUL;
    /* for >32 bit machines */
  }
  (*mt)[N] = mti;
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long genrand_int32_r(unsigned long** mt)
{
  unsigned long y;
  static unsigned long mag01[2] = { 0x0UL, MATRIX_A };
  /* mag01[x] = x * MATRIX_A  for x=0,1 */
  unsigned long mti = (*mt)[N];

  if (mti >= N) { /* generate N words at one time */
    int kk;

    for (kk = 0; kk<N - M; kk++) {
      y = ((*mt)[kk] & UPPER_MASK) | ((*mt)[kk + 1] & LOWER_MASK);
      (*mt)[kk] = (*mt)[kk + M] ^ (y >> 1) ^ mag01[y & 0x1UL];
    }
    for (; kk<N - 1; kk++) {
      y = ((*mt)[kk] & UPPER_MASK) | ((*mt)[kk + 1] & LOWER_MASK);
      (*mt)[kk] = (*mt)[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
    }
    y = ((*mt)[N - 1] & UPPER_MASK) | ((*mt)[0] & LOWER_MASK);
    (*mt)[N - 1] = (*mt)[M - 1] ^ (y >> 1) ^ mag01[y & 0x1UL];
    mti = 0;
  }
  y = (*mt)[mti++];
  /* Tempering */
  y ^= (y >> 11);
  y ^= (y << 7) & 0x9d2c5680UL;
  y ^= (y << 15) & 0xefc60000UL;
  y ^= (y >> 18);

  (*mt)[N] = mti;

  return y;
}

/* generates a random number on [0,0x7fffffff]-interval */
long genrand_int31_r(unsigned long** mt)
{
  return (long)(genrand_int32_r(mt) >> 1);
}

/* generates a random number on [0,1]-real-interval */
double genrand_real1_r(unsigned long** mt)
{
  return genrand_int32_r(mt)*(1.0 / 4294967295.0);
  /* divided by 2^32-1 */
}

/* generates a random number on [0,1)-real-interval */
double genrand_real2_r(unsigned long** mt)
{
  return genrand_int32_r(mt)*(1.0 / 4294967296.0);
  /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double genrand_real3_r(unsigned long** mt)
{
  //return (((double)genrand_int32()) + 0.5)*(1.0/4294967296.0); 
  return (((double)genrand_int32_r(mt)) + 0.5)*((double)1.0 / 4294967296.0);
  /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
double genrand_res53_r(unsigned long** mt)
{
  unsigned long a = genrand_int32_r(mt) >> 5, b = genrand_int32_r(mt) >> 6;
  return(a*67108864.0 + b)*(1.0 / 9007199254740992.0);

}
/* These real versions are due to Isaku Wada, 2002/01/09 added */

