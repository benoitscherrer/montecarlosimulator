/*
3-D multipopulation environment object MP3D
*/
// For line number in memory reports by _CrtDumpMemoryLeaks()
// must be placed in every .c[xx] file:
#define _CRTDBG_MAP_ALLOC  

#include "MP3D.h"

#define _USE_MATH_DEFINES
#include <math.h>    // sqrt, pow, fmod
#include <stdio.h>   // printf
#include <stdlib.h>  // malloc, calloc, free, exit
#include <string.h> // memcpy
#include <vector>    // C++ vector 

#include "mymaths.h" // largest, smallest, cross_3D, add_arrays, real_lcm, etc.
#include "RNG.h"     // random number generation (MT19937) single-thread only
#include "RNG_r.h"   // RNG (MT19937) re-entrant, thread-safe 
#include "tools.h"   // import_porefile_alloc


/*
Wrapper for quickly creating diffusion environments.

Inputs:
 - env: reference of structure to modify
 - scenario: 1, 2, 5

Scenario 1: 4 populations containing respectively 3 cylinders, 1 cylinder,
            2 cylinders and 2 spheres.
Scenario 2: crossing fascicles Camino style, identical radius and spacing
Scenario 5: 120 randomly-packed cylinders (139 with periodic clones) in a
            square lattice with side 14.48 microns (! hard-coded path to
            cylinder file !)
*/
void MP3D_create_environment(MP3D_env* env, unsigned char scenario) {
  double cdir1[3] = { 0.0, 0.0, 1.0 };
  double tmp_piling_dir[3] = { 0.0, 1.0, 0.0 };
  memcpy(env->piling_dir, tmp_piling_dir, sizeof(tmp_piling_dir));
  // cdir2 corresponds to rotation of -2pi/8 around piling dir
  double cdir2[3] = { -sqrt(2.0)/2.0, 0.0, sqrt(2.0)/2.0 };
  double perpdir1[3], perpdir2[3];
  // perpendicular directions obtained by cross-product
  // (right-hand-rule coordinate system)
  cross_3D(&(perpdir1[0]), env->piling_dir, &(cdir1[0]));
  cross_3D(&(perpdir2[0]), env->piling_dir, &(cdir2[0]));

  // SCENARIO 1 : 4 populations, namely 3 cylinders, 1 cylinder, 2 cylinders,
  //              2 spheres with 2 different main orientations
  if (scenario == 1) {
    double r1, r2, e1, e2, e_l;
    r1 = 0.75e-6; // 0.75e-6 for most test cases
    e1 = 0.50e-6; // 0.50e-6 for most test cases
    r2 = 1.5e-6;
    e2 = 0.5e-6;
    e_l = 0.01e-6;
    env->numpop = 4;
    unsigned int tmp_sizes[4] = {3, 1, 2, 2}; // popsizes [numpop]
    PopulationTypes tmp_types[4] = {kCylinder, 
                                    kCylinder,
                                    kCylinder,
                                    kSphere }; // poptypes [numpop]
    double tmp_obj_pile_pos[8] = {1.5e-6, 3.6e-6, 2e-6,
                                  (e_l / 2 + r1),
                                  (e_l / 2 + r2), (e_l / 2 + e2 + 3 * r2),
                                  3.1e-6, 3.1e-6 }; // obj_pile_pos [totobjects]
    double tmp_obj_perp_pos[8] = {1.2e-6, 1.0e-6, 3.9e-6,
                                  (e1 / 2 + r1),
                                  (e2 / 2 + r2), (e2 / 2 + r2),
                                  3.5e-6, 13.9e-6}; // obj_perp_pos [totobjects]
    double tmp_obj_cdir_pos[8] = {0.0, 0.0, 0.0,
                                  0.0,
                                  0.0, 0.0,
                                  3.01e-6, 11.49e-6}; // obj_cdir_pos [totobjects]
    double tmp_radii[8] = {1.0e-6, 0.3e-6, 1.5e-6,
                           r1,
                           r2, r2,
                           3e-6, 1e-6 };  // radii [totobjects]
    double tmp_cell_pile_sizes[4] = {4e-6, 2*r1+e_l, 4*r2+e2+e_l, 6.2e-6}; 
    double tmp_cell_perp_sizes[4] = {6e-6, 2*r1+e1 , 2*r2+e2, 15e-6};
    // Along main direction, use 0 for cylinder populations
    // for initial uniform particle dropping:
    double tmp_cell_cdir_sizes[4] = {0.0, 0.0, 0.0, 18.5e-6};
    // In Matlab: off_pile = cumsum(cell_pile_sizes)-cell_pile_sizes -[0,0,0,0] ;
    double tmp_off_pile[4] =
      {0,
       tmp_cell_pile_sizes[0],
       tmp_cell_pile_sizes[0]+tmp_cell_pile_sizes[1],
       tmp_cell_pile_sizes[0]+tmp_cell_pile_sizes[1]+tmp_cell_pile_sizes[2]};
    double tmp_off_perp[4] = {0, 0, (2 * r1 + e1 - r2 - e2 / 2), 0};
    double tmp_off_cdir[4] = {0.0, 0.0, 0.0, 0.0};
    double tmp_perpdir[12] =
      {perpdir2[0], perpdir2[1], perpdir2[2],
       perpdir1[0],perpdir1[1], perpdir1[2],
       perpdir1[0],perpdir1[1], perpdir1[2],
       perpdir2[0], perpdir2[1], perpdir2[2]}; // perpdir [3*numpop]
    double tmp_cdir[12] =
      {cdir2[0], cdir2[1], cdir2[2],
       cdir1[0], cdir1[1], cdir1[2],
       cdir1[0], cdir1[1], cdir1[2],
       cdir2[0], cdir2[1], cdir2[2]};  // cdir [3*numpop]
    double sum_pile[4];    // [numpop]
    add_arrays(&(sum_pile[0]), tmp_off_pile, tmp_cell_pile_sizes, env->numpop);
    env->pile_period = largest(sum_pile, env->numpop) - smallest(tmp_off_pile, env->numpop);

    env->popsizes = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
    env->poptypes = (PopulationTypes*)malloc(env->numpop * sizeof(PopulationTypes));
    env->obj_pile_pos = (double*)malloc(sizeof(tmp_obj_pile_pos));
    env->obj_perp_pos = (double*)malloc(sizeof(tmp_obj_perp_pos));
    env->obj_cdir_pos = (double*)malloc(sizeof(tmp_obj_cdir_pos));
    env->radii = (double*)malloc(sizeof(tmp_radii));
    env->cell_pile_sizes = (double*)malloc(sizeof(tmp_cell_pile_sizes));
    env->cell_perp_sizes = (double*)malloc(sizeof(tmp_cell_perp_sizes));
    env->cell_cdir_sizes = (double*)malloc(sizeof(tmp_cell_cdir_sizes));
    env->off_pile = (double*)malloc(sizeof(tmp_off_pile));
    env->off_perp = (double*)malloc(sizeof(tmp_off_perp));
    env->off_cdir = (double*)malloc(sizeof(tmp_off_cdir));
    env->perpdir = (double*)malloc(sizeof(tmp_perpdir));
    env->cdir = (double*)malloc(sizeof(tmp_cdir));

    memcpy(env->popsizes, tmp_sizes, sizeof(tmp_sizes));
    memcpy(env->poptypes, tmp_types, sizeof(tmp_types));
    memcpy(env->obj_pile_pos, tmp_obj_pile_pos, sizeof(tmp_obj_pile_pos));
    memcpy(env->obj_perp_pos, tmp_obj_perp_pos, sizeof(tmp_obj_perp_pos));
    memcpy(env->obj_cdir_pos, tmp_obj_cdir_pos, sizeof(tmp_obj_cdir_pos));
    memcpy(env->radii, tmp_radii, sizeof(tmp_radii));
    memcpy(env->cell_pile_sizes, tmp_cell_pile_sizes, sizeof(tmp_cell_pile_sizes));
    memcpy(env->cell_perp_sizes, tmp_cell_perp_sizes, sizeof(tmp_cell_perp_sizes));
    memcpy(env->cell_cdir_sizes, tmp_cell_cdir_sizes, sizeof(tmp_cell_cdir_sizes));
    memcpy(env->off_pile, tmp_off_pile, sizeof(tmp_off_pile));
    memcpy(env->off_perp, tmp_off_perp, sizeof(tmp_off_perp));
    memcpy(env->off_cdir, tmp_off_cdir, sizeof(tmp_off_cdir));
    memcpy(env->perpdir, tmp_perpdir, sizeof(tmp_perpdir));
    memcpy(env->cdir, tmp_cdir, sizeof(tmp_cdir));
  }

  // SCENARIO 2 : crossing fascicles Camino style, identical radius and spacing 
  else if (scenario == 2) {
    double r1, r2, e1, e2, e_l, fin_targ;
    fin_targ = 0.50;
    r1 = 1.0e-6; // identical arrays crossing: r=1.0e-6, sep=2.198458e-6=> fin~=0.65 for testing
    e1 = (sqrt(M_PI/fin_targ)-2)*r1;  //0.198458429686861e-06;
    r2 = r1; 
    e2 = e1; 
    e_l = (e1+e2)/2.0;
    env->numpop = 2;
    unsigned int tmp_sizes[2] = {1, 1};  // popsizes [numpop]
    PopulationTypes tmp_types[2] = {kCylinder, kCylinder};  // poptypes [numpop]
    double tmp_obj_pile_pos[2] =
      {(e_l / 2.0 + r1),
       (e_l / 2.0 + r2)};  // obj_pile_pos [totobjects]
    double tmp_obj_perp_pos[2] =
      {(r1 + e1 / 2.0),
       (r2 + e2 / 2.0)};  // obj_perp_pos [totobjects]
    double tmp_obj_cdir_pos[2] = {0.0, 0.0};  // obj_cdir_pos [totobjects]
    double tmp_radii[2] = {r1, r2};  // radii [totobjects]
    double tmp_cell_pile_sizes[2] =
      {(2 * r1 + e_l),
       (2 * r2 + e_l)};  // cell_pile_sizes [numpop]
    double tmp_cell_perp_sizes[2] =
      {(2 * r1 + e1),
       (2 * r2 + e2)};  // cell_perp_sizes [numpop]
    // Use 0 in main direction for cylinder populations:
    double tmp_cell_cdir_sizes[2] = {0.0, 0.0};  // cell_cdir_sizes [numpop]
    double tmp_off_pile[2] =
      {0 - r1 - e_l / 2,
       (2 * r1 + e_l) - r1 - e_l / 2};  // off_pile [numpop]
    double tmp_off_perp[2] = {(-e1 / 2 - r1), 0.0};  // off_perp [numpop]
    double tmp_off_cdir[2] = {0.0, 0.0};  // off_cdir [numpop] 
    double tmp_perpdir[6] =
      {perpdir1[0],perpdir1[1], perpdir1[2],
       perpdir2[0], perpdir2[1], perpdir2[2]}; // perpdir [3*numpop]
    double tmp_cdir[6] =
      {cdir1[0], cdir1[1], cdir1[2],
       cdir2[0], cdir2[1], cdir2[2]};  // cdir [3*numpop]

    double sum_pile[2];  // [numpop]

    add_arrays(&(sum_pile[0]), tmp_off_pile, tmp_cell_pile_sizes, env->numpop);
    env->pile_period = largest(sum_pile, env->numpop) - smallest(tmp_off_pile, env->numpop);

    env->popsizes = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
    env->poptypes = (PopulationTypes*)malloc(env->numpop * sizeof(PopulationTypes));
    env->obj_pile_pos = (double*)malloc(sizeof(tmp_obj_pile_pos));
    env->obj_perp_pos = (double*)malloc(sizeof(tmp_obj_perp_pos));
    env->obj_cdir_pos = (double*)malloc(sizeof(tmp_obj_cdir_pos));
    env->radii = (double*)malloc(sizeof(tmp_radii));
    env->cell_pile_sizes = (double*)malloc(sizeof(tmp_cell_pile_sizes));
    env->cell_perp_sizes = (double*)malloc(sizeof(tmp_cell_perp_sizes));
    env->cell_cdir_sizes = (double*)malloc(sizeof(tmp_cell_cdir_sizes));
    env->off_pile = (double*)malloc(sizeof(tmp_off_pile));
    env->off_perp = (double*)malloc(sizeof(tmp_off_perp));
    env->off_cdir = (double*)malloc(sizeof(tmp_off_cdir));
    env->perpdir = (double*)malloc(sizeof(tmp_perpdir));
    env->cdir = (double*)malloc(sizeof(tmp_cdir));

    memcpy(env->popsizes, tmp_sizes, sizeof(tmp_sizes));
    memcpy(env->poptypes, tmp_types, sizeof(tmp_types));
    memcpy(env->obj_pile_pos, tmp_obj_pile_pos, sizeof(tmp_obj_pile_pos));
    memcpy(env->obj_perp_pos, tmp_obj_perp_pos, sizeof(tmp_obj_perp_pos));
    memcpy(env->obj_cdir_pos, tmp_obj_cdir_pos, sizeof(tmp_obj_cdir_pos));
    memcpy(env->radii, tmp_radii, sizeof(tmp_radii));
    memcpy(env->cell_pile_sizes, tmp_cell_pile_sizes, sizeof(tmp_cell_pile_sizes));
    memcpy(env->cell_perp_sizes, tmp_cell_perp_sizes, sizeof(tmp_cell_perp_sizes));
    memcpy(env->cell_cdir_sizes, tmp_cell_cdir_sizes, sizeof(tmp_cell_cdir_sizes));
    memcpy(env->off_pile, tmp_off_pile, sizeof(tmp_off_pile));
    memcpy(env->off_perp, tmp_off_perp, sizeof(tmp_off_perp));
    memcpy(env->off_cdir, tmp_off_cdir, sizeof(tmp_off_cdir));
    memcpy(env->perpdir, tmp_perpdir, sizeof(tmp_perpdir));
    memcpy(env->cdir, tmp_cdir, sizeof(tmp_cdir));
  }

  // SCENARIO 5: Random packing of 120 cylinders within one population confined in a square bounding box.
  else if (scenario == 5) {
    double L = 14.4820924093385E-6;
    // File path hard coded here!
    char cylfilepath[150] = "../data/cylfiles/cylpos5.txt";
    struct PackedPoreSubstrateParams params;
    params.from_file = true;
    params.lattice_size = L;
    params.porefilepath = cylfilepath;
    MP3D_create_environment_heterpack(env, params);  // L, cylfilepath
  }

  // ERROR
  else {
    printf("ERROR MP3D_create_environment: unknown scenario %d\n", scenario);
    exit(-1);
  }

  // Rest of function, common to some scenarios
  if (scenario == 1 || scenario == 2) {
    env->totobjects = 0;
    unsigned int i;
    for (i = 0; i < env->numpop; i++) {
      env->totobjects += env->popsizes[i];                      // totobjects
    }
    env->obj_end_ind = (unsigned int*)malloc(sizeof(unsigned int)*env->numpop);
    cumsum_m1(env->obj_end_ind, env->popsizes, env->numpop);       // obj_end_ind
    env->obj_start_ind = (unsigned int*)malloc(sizeof(unsigned int)*env->numpop);
    env->obj_start_ind[0] = 0;
    for (i = 1; i < env->numpop; i++) {
      env->obj_start_ind[i] = env->obj_end_ind[i - 1] + 1;         // obj_start_ind
    }
  }
  // We might want to include MP3D_check_consistency here...
  // Intialize particle dropping
  MP3D_compute_generator(env);
}


/*
Empty environment for free diffusion. 
TODO: make sure this is stable
*/
void MP3D_create_environment_free_diffusion(MP3D_env* env) {
  env->numpop = 0;
  env->pile_period = 0;
  env->cell_pile_sizes = (double*)malloc(sizeof(double));
  env->cell_pile_sizes[0] = 0.0;
  env->piling_dir[0] = 0.0;
  env->piling_dir[1] = 1.0;
  env->piling_dir[2] = 0.0;
  env->num_noncolinear_cdirs = 0; // called in MP3D_drop_particle*
  
  env->popsizes = NULL;
  env->poptypes = NULL;
  env->totobjects = 0;
  env->obj_start_ind = NULL;
  env->obj_end_ind = NULL;
  env->obj_pile_pos = NULL;
  env->obj_perp_pos = NULL;
  env->obj_cdir_pos = NULL;
  env->radii = NULL;
  env->cell_perp_sizes = NULL;
  env->cell_cdir_sizes = NULL;
  env->off_pile = NULL;
  env->off_perp = NULL;
  env->off_cdir = NULL;
  env->perpdir = NULL;
  env->cdir = NULL;
  // After initialization:
  env->noncolinear_cdirs = NULL;
  env->noncolinear_pdirs = NULL;
  env->perpdists = NULL;
  env->cdirdists = NULL;  // to account for sphere populations
}


/*
 Wrapper for multiple-layer crossing fascicles with independent properties.
 Two populations of cylinders crossing in interleaved planes,
 n1 sublayers of Population 1 followed by n2 sublayers of Population 2 and so
 on along a direction named "piling" direction.

 Within a Population, the sublayers of cylinders are arranged in a
 hexagonal packing.

 The cylinders of Population 1 are parallel to the z-axis. Those of
 Population 2 have their main direction (cdir) in the x-z plane.


Inputs:
 - r1, r2: cylinder radius of Population 1 and 2, respectively
 - e1, e2: distance along perp direction between the two closest points of 
           two consecutive cylinders. Also the distance within a cylinder
           and the closest cylinder in the preceding and following sublayer
           (along piling direction) within a population.
 - e_l: distance, in the piling direction, between the last sublayer of of
        one population and the first sublayer of the second population,
        "inter-population" distance.
 - rotangle: around y-axis towards the negative x direction, as in Camino.
             Specified in radians.
 - n1: number of sub-layers of axons in population 1
 - n2: number of sub-layers of axons in population 2

Example with n1=3 and n2=2:
   o o o o o o o
... o o o o o o ...
   o o o o o o o      ^
 =================    |
 ================= piling dir (common to Pop 1 and 2)
   o o o o o o o      | 
... o o o o o o ...   ---- perpdir1 -->
   o o o o o o o
 =================
 =================
   o o o o o o o
... o o o o o o ...
   o o o o o o o
 =================
 =================

 Within Population i:
                                                 ^
 |       x       |         |       x       |     |
 |<-r_i->x<-r_i->|<--e_i-->|<-r_i->x<-r_i->|    cdir i
 |       x       |         |       x       |     | --perp_dir i-->



 Date: August-September 2016
*/
void MP3D_create_environment_crossing(MP3D_env* env, double r1, double r2,
    double e1, double e2, double e_l, double rotangle,
    unsigned int n1, unsigned int n2) {
  double cdir1[3] = {0.0,0.0,1.0} ;
  double tmp_piling_dir[3] = {0.0, 1.0, 0.0} ;                             // piling_dir
  memcpy(env->piling_dir,tmp_piling_dir,sizeof(tmp_piling_dir)) ;
  double cdir2[3] = {sin(-rotangle), 0.0, cos(-rotangle)} ; // corresponds to rotation of -rotangle around piling dir, as in Camino
  double perpdir1[3], perpdir2[3] ;
  cross_3D(&(perpdir1[0]), env->piling_dir, &(cdir1[0])) ;
  cross_3D(&(perpdir2[0]), env->piling_dir, &(cdir2[0])) ;

  env->numpop = 2; 
  unsigned int tmp_sizes[2] = {n1,n2} ;                             // popsizes [numpop]
  PopulationTypes tmp_types[2] = {kCylinder, kCylinder};             // poptypes [numpop]

  double h1 = 0.5*sqrt(3)*(2.0*r1+e1) ;        // height of equilateral triangle of side s1=2*r1+e1
  double h2 = 0.5*sqrt(3)*(2.0*r2+e2) ;        // height of equilateral triangle of side s2=2*r2+e2

  double tmp_cell_pile_sizes[2] = {(2.0*r1+e_l+(n1-1)*h1), (2.0*r2+e_l+(n2-1)*h2 ) };     // cell_pile_sizes [numpop]
  double tmp_cell_perp_sizes[2] = {(2.0*r1+e1) , (2.0*r2+e2)} ;       // cell_perp_sizes [numpop]
  double tmp_cell_cdir_sizes[2] = {0.0,0.0} ;                     // cell_cdir_sizes [numpop] , 1 is not a good idea for uniform particle dropping
  double tmp_off_pile[2] = {0-r1-e_l/2.0 , (2.0*r1+e_l+(n1-1)*h1)-r1-e_l/2.0};    // off_pile [numpop]
  double tmp_off_perp[2] = {-e1/2.0-r1, ((n1+1)%2)*(-e2/2.0-r2)} ;                      // off_perp [numpop]: inst. of -e2/2-r2 so that in limit alpha-> 0 we converge to hex packing when n1 and n2 have the same parity, otherwise nothing to do anyways
  double tmp_off_cdir[2] = {0.0, 0.0} ;                           // off_cdir [numpop] 
  double tmp_perpdir[6] = {perpdir1[0],perpdir1[1], perpdir1[2], perpdir2[0], perpdir2[1], perpdir2[2]} ; // perpdir [3*numpop]
  double tmp_cdir[6] = {cdir1[0], cdir1[1], cdir1[2], cdir2[0], cdir2[1], cdir2[2]} ;                     // cdir [3*numpop]
  
  double sum_pile[2] ;    // [numpop]
  
  add_arrays(&(sum_pile[0]), tmp_off_pile, tmp_cell_pile_sizes, env->numpop) ;
  env->pile_period = largest(sum_pile, env->numpop) - smallest(tmp_off_pile, env->numpop) ;

  env->popsizes = (unsigned int*)malloc(env->numpop*sizeof(unsigned int)) ;
  env->poptypes = (PopulationTypes*)malloc(env->numpop*sizeof(PopulationTypes)) ;
  env->obj_pile_pos = (double*)malloc((n1+n2)*sizeof(double)) ;
  env->obj_perp_pos = (double*)malloc((n1+n2)*sizeof(double)) ;
  env->obj_cdir_pos = (double*)calloc(n1+n2, sizeof(double)) ;   // initialize to 0;  1 is not a good idea for uniform particle dropping
  env->radii = (double*)malloc((n1+n2)*sizeof(double)) ;
  env->cell_pile_sizes = (double*)malloc(sizeof(tmp_cell_pile_sizes)) ;
  env->cell_perp_sizes = (double*)malloc(sizeof(tmp_cell_perp_sizes)) ;
  env->cell_cdir_sizes = (double*)malloc(sizeof(tmp_cell_cdir_sizes)) ;
  env->off_pile = (double*)malloc(sizeof(tmp_off_pile)) ;
  env->off_perp = (double*)malloc(sizeof(tmp_off_perp)) ;
  env->off_cdir = (double*)malloc(sizeof(tmp_off_cdir)) ;
  env->perpdir = (double*)malloc(sizeof(tmp_perpdir)) ;
  env->cdir = (double*)malloc(sizeof(tmp_cdir)) ;
  
  memcpy(env->popsizes, tmp_sizes, sizeof(tmp_sizes)) ;               
  memcpy(env->poptypes,tmp_types,sizeof(tmp_types)) ;                

  // Assign pile and perp positions of objects in each fiber population, performing hexagonal packing in each population:

  unsigned int i ;
  for(i=0; i<n1; i++){
    env->obj_pile_pos[i] = e_l/2.0 + r1 + i*h1  ;
    env->obj_perp_pos[i] = e1/2.0 + r1 + (i%2)*(r1+e1/2.0) ;
    env->radii[i] = r1 ;
  }
  for(i=0; i<n2; i++){
    env->obj_pile_pos[n1+i] = e_l/2.0 + r2 + i*h2  ;
    env->obj_perp_pos[n1+i] = e2/2.0 + r2 + (i%2)*(r2+e2/2.0) ;
    env->radii[n1+i] = r2 ;
  }
  memcpy(env->cell_pile_sizes,tmp_cell_pile_sizes,sizeof(tmp_cell_pile_sizes)) ;
  memcpy(env->cell_perp_sizes,tmp_cell_perp_sizes,sizeof(tmp_cell_perp_sizes)) ;
  memcpy(env->cell_cdir_sizes,tmp_cell_cdir_sizes,sizeof(tmp_cell_cdir_sizes)) ;
  memcpy(env->off_pile,tmp_off_pile,sizeof(tmp_off_pile)) ;
  memcpy(env->off_perp,tmp_off_perp,sizeof(tmp_off_perp)) ;
  memcpy(env->off_cdir,tmp_off_cdir,sizeof(tmp_off_cdir)) ;
  memcpy(env->perpdir, tmp_perpdir, sizeof(tmp_perpdir)) ;
  memcpy(env->cdir, tmp_cdir, sizeof(tmp_cdir)) ;

  env-> totobjects = 0 ;      // initialization

  for(i=0; i<env->numpop; i++){
      env-> totobjects += env->popsizes[i] ;                      // totobjects
  }
  env->obj_end_ind = (unsigned int*)malloc(sizeof(unsigned int)*env->numpop) ;
  cumsum_m1(env->obj_end_ind, env->popsizes, env->numpop) ;       // obj_end_ind
  env->obj_start_ind = (unsigned int*)malloc(sizeof(env->obj_end_ind)) ;
  env->obj_start_ind[0] = 0 ;
  for (i=1; i< env->numpop; i++){
      env->obj_start_ind[i] = env->obj_end_ind[i-1] + 1 ;         // obj_start_ind
  }
  // We might want to include MP3D_check_consistency here...
  // Intialize particle dropping
  MP3D_compute_generator(env);
}


/*
Wrapper for environment with crude 2D axonal dispersion.

The environment is made of successive sheets of parallel
straight identical cylinders that are regularly-spaced, with orientations
(cdir) in the x-z plane uniformly and symmetricallly distributed around
the z-axis, leading to a helicoidal-like structure.

Along the "piling direction" (y-axis here), we thus have
pop_1 - pop_2 - ... - pop_numpop - pop_1 - pop_2 -...

Inputs:
 - env : pointer to 3D environment structure
 - angle_max : maximum angle with respect to z-axis.
              The total angular sector covered is thus 2*angle_max
 - numpop : how many elementary sheets or populations of parallel cylinders
           with different angles. Angular separation is thus
           2*angle_max/(numpop-1)
 - rad :  unique characteristic radius of all cylinders
 - fin_tot : total intracellular volume fraction in ]0, pi/4~0.7854[
           (no check performed!).

Date: October 21, 2016
*/
void MP3D_create_environment_dispersion(MP3D_env* env, double angle_max,
    unsigned int numpop, double rad, double fin_tot) {
  unsigned int dim = 3;
  // intercylinder(center to center) separation that
  // we would have in square packing:
  double sep = rad*sqrt(M_PI/fin_tot);
  double e_l = sep - 2.0 * rad;  // interlayer spacing

  double refdir[3] = { 0.0,0.0,1.0 };  // z-axis by default
  env->piling_dir[0] = 0.0;
  env->piling_dir[1] = 1.0;
  env->piling_dir[2] = 0.0;
  // TODO: ideally piling_dir should be used for Rodriguez rotation formula

  // Populations' properties:
  env->numpop = numpop;
  env->popsizes = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->poptypes = (PopulationTypes*)malloc(env->numpop * sizeof(PopulationTypes));
  env->totobjects = 0;
  env->cell_pile_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->cell_perp_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->cell_cdir_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->off_pile = (double*)malloc(env->numpop * sizeof(double));
  env->off_perp = (double*)malloc(env->numpop * sizeof(double));
  env->off_cdir = (double*)malloc(env->numpop * sizeof(double));
  env->cdir = (double*)malloc(env->numpop*dim * sizeof(double));
  env->perpdir = (double*)malloc(env->numpop*dim * sizeof(double));
  unsigned int i;
  double rotangle;
  for (i = 0; i < env->numpop; i++) {
    env->popsizes[i] = 1;    // each population contains only one cylinder
    env->poptypes[i] = kCylinder;
    env->totobjects += env->popsizes[i];
    env->cell_pile_sizes[i] = 2 * rad + e_l;
    env->cell_perp_sizes[i] = sep;
    env->cell_cdir_sizes[i] = 0.0;
    env->off_pile[i] = i* (2 * rad + e_l);
    env->off_perp[i] = 0.0;
    env->off_cdir[i] = 0.0;
    rotangle = -angle_max + i*(2.0*angle_max) / ((double)env->numpop - 1.0);
    // This should implement Rodrigues' formula with refdir and
    // rotangle, not that hard
    env->cdir[i*dim] = refdir[0] * cos(rotangle) - refdir[2] * sin(rotangle);
    env->cdir[i*dim + 1] = refdir[1];
    env->cdir[i*dim + 2] = refdir[0] * sin(rotangle) + refdir[2] * cos(rotangle);
    // Perpendicular direction obtained as pilingxcdir (vector cross product)
    cross_3D(&(env->perpdir[dim * i]), env->piling_dir, &(env->cdir[dim*i]));
  }
  env->obj_end_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->obj_start_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->obj_start_ind[0] = 0;
  env->obj_end_ind[0] = env->popsizes[0] - 1;
  for (i = 1; i< env->numpop; i++) {
    env->obj_end_ind[i] = env->obj_end_ind[i - 1] + env->popsizes[i];
    env->obj_start_ind[i] = env->obj_end_ind[i - 1] + 1;         // obj_start_ind
  }
  // Individual object properties
  env->obj_pile_pos = (double*)malloc(env->totobjects * sizeof(double));
  env->obj_perp_pos = (double*)malloc(env->totobjects * sizeof(double));
  env->obj_cdir_pos = (double*)malloc(env->totobjects * sizeof(double));
  env->radii = (double*)malloc(env->totobjects * sizeof(double));
  for (i = 0; i < env->totobjects; i++) {
    env->obj_pile_pos[i] = rad + e_l/2.0;
    env->obj_perp_pos[i] = sep/2.0;
    env->obj_cdir_pos[i] = 0.0;
    env->radii[i] = rad;
  }

  std::vector<double> sum_pile(env->numpop);    // [numpop], intermediate result array
  add_arrays(&(sum_pile[0]), env->off_pile, env->cell_pile_sizes, env->numpop);
  env->pile_period = largest(&(sum_pile[0]), env->numpop) - smallest(env->off_pile, env->numpop);

  // We might want to include MP3D_check_consistency here...
  // Intialize particle dropping
  MP3D_compute_generator(env);

}


/*
Wrapper for an environment with cylinders in hexagonal packing oriented
along the z-axis representing an idealized single fascicle of identical axons.

Inputs:
 - env : pointer to 3D environment structure
 - r : radius of all cylinders in the hexagonal packing
 - f : cylinder packing density in the interval ]0, sqrt(3)*M_PI/6[ 

Determines the inter-cylinder center-to-center spacing s via the formula
f = (2*pi/3)*(r/s)^2, 
where s>=2r to avoid cylinder overlap.

Date: November 27, 2016
*/
void MP3D_create_environment_hexpack(MP3D_env* env, double r, double f) {
  unsigned int dim = 3;
  double sep = r*sqrt(2.0 * M_PI / (sqrt(3.0)*f));
  double e = sep - 2.0 * r;

  double refdir[3] = { 0.0,0.0,1.0 };    // z-axis by default
  env->piling_dir[0] = 0.0;
  env->piling_dir[1] = 1.0;
  env->piling_dir[2] = 0.0;        // ideally this should be used for Rodriguez rotation formula

  // Populations' properties:
  env->numpop = 2;
  env->popsizes = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->poptypes = (PopulationTypes*)malloc(env->numpop * sizeof(PopulationTypes));
  env->totobjects = 0;
  env->cell_pile_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->cell_perp_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->cell_cdir_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->off_pile = (double*)malloc(env->numpop * sizeof(double));
  env->off_perp = (double*)malloc(env->numpop * sizeof(double));
  env->off_cdir = (double*)malloc(env->numpop * sizeof(double));
  env->cdir = (double*)malloc(env->numpop*dim * sizeof(double));
  env->perpdir = (double*)malloc(env->numpop*dim * sizeof(double));
  unsigned int i;
  for (i = 0; i < env->numpop; i++) {
    env->popsizes[i] = 1;    // each populationo made of only one cylinder
    env->poptypes[i] = kCylinder;
    env->totobjects += env->popsizes[i];
    env->cell_pile_sizes[i] = 2.0 * r ;
    env->cell_perp_sizes[i] = 2.0*r + e;
    env->cell_cdir_sizes[i] = 0.0;

    env->cdir[i*dim] = refdir[0];
    env->cdir[i*dim + 1] = refdir[1];
    env->cdir[i*dim + 2] = refdir[2];
    // Perpendicular direction obtained as pilingxcdir (vector cross product)
    cross_3D(&(env->perpdir[dim * i]), env->piling_dir, &(env->cdir[dim*i]));
  }
  env->off_pile[0] = -r; env->off_pile[1] = -r + 0.5*sqrt(3.0)*(2.0 * r + e);
  env->off_perp[0] = -e / 2 - r;  env->off_perp[1] = 0.0;
  env->off_cdir[0] = 0.0; env->off_cdir[1] = 0.0;

  env->obj_end_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->obj_start_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->obj_start_ind[0] = 0;
  env->obj_end_ind[0] = env->popsizes[0] - 1;
  for (i = 1; i< env->numpop; i++) {
    env->obj_end_ind[i] = env->obj_end_ind[i - 1] + env->popsizes[i];
    env->obj_start_ind[i] = env->obj_end_ind[i - 1] + 1;         // obj_start_ind
  }
  // Individual object properties
  env->obj_pile_pos = (double*)malloc(env->totobjects * sizeof(double));
  env->obj_perp_pos = (double*)malloc(env->totobjects * sizeof(double));
  env->obj_cdir_pos = (double*)malloc(env->totobjects * sizeof(double));
  env->radii = (double*)malloc(env->totobjects * sizeof(double));
  for (i = 0; i < env->totobjects; i++) {
    env->obj_pile_pos[i] = r;
    env->obj_perp_pos[i] = r + e / 2.0;
    env->obj_cdir_pos[i] = 0.0;
    env->radii[i] = r;
  }

  env->pile_period = sqrt(3.0)*(2.0 * r + e);

  // We might want to include MP3D_check_consistency here...
  // Intialize particle dropping
  MP3D_compute_generator(env);
}

/*
Wrapper for an environment containing straight, parallel cylinders parallel
to the z-axis with radius heterogeneity.

Inputs:
- env : pionter to 3D environment structure
- params.lattice_size : size of square lattice containing the cylinders
- params.from_file: boolean indicating whether cylinder info is read from
  file or directly from a 2D array;
- params.porefilepath : path to 4 column text file where separator is a
  space or tab (no commas), containg the x-, y- and z- coordinates of the
  center and the radius, only set if params.from_file is true;
- params.pore_array: double pointer to double pointing to a 2D array
  containing the cylinder info, set if params.from_file is false;
- params.num_lines: positive integer giving size of pore array, if
  applicable.

Date: February 8, 2017
Updated: June 2019
*/
void MP3D_create_environment_heterpack(
    MP3D_env* env, struct PackedPoreSubstrateParams params) {
  // Extract parameter that are always set
  bool from_file = params.from_file;
  double Lsize = params.lattice_size;

  unsigned int dim = 3;

  double refdir[3] = { 0.0,0.0,1.0 };    // z-axis by default
  env->piling_dir[0] = 0.0;
  env->piling_dir[1] = 1.0;
  env->piling_dir[2] = 0.0;        // ideally this should be used for Rodriguez rotation formula

                                  // Populations' properties:
  env->numpop = 1;
  env->popsizes = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->poptypes = (PopulationTypes*)malloc(env->numpop * sizeof(PopulationTypes));
  env->cell_pile_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->cell_perp_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->cell_cdir_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->off_pile = (double*)malloc(env->numpop * sizeof(double));
  env->off_perp = (double*)malloc(env->numpop * sizeof(double));
  env->off_cdir = (double*)malloc(env->numpop * sizeof(double));
  env->cdir = (double*)malloc(env->numpop*dim * sizeof(double));
  env->perpdir = (double*)malloc(env->numpop*dim * sizeof(double));

  // Get cylinder positions and radii
  unsigned int numcyls;
  double ** cylinder_array;
  if (from_file) {
    cylinder_array = import_porefile_alloc(params.porefilepath, &numcyls);
    printf("MP3D_create_environment_heterpack: Detected %d "
           "cylinders in cylinderfile %s.\n",
           numcyls, params.porefilepath);
  }
  else {
    cylinder_array = params.pore_array;
    numcyls = params.num_lines;
    printf("MP3D_create_environment_heterpack: got list of %d cylinders.\n",
           numcyls);
  }

  // Individual object properties
  env->obj_pile_pos = (double*)malloc(numcyls * sizeof(double));
  env->obj_perp_pos = (double*)malloc(numcyls * sizeof(double));
  env->obj_cdir_pos = (double*)calloc(numcyls, sizeof(double));    // 0 by default
  env->radii = (double*)malloc(numcyls * sizeof(double));

  // Filter out clone cylinders, considering origin of elementary cell at (0,0)
  unsigned int i;
  double x_i, y_i, z_i, r_i;
  unsigned int ok_cnt = 0;
  
  for (i = 0; i < numcyls; i++) {
    x_i = cylinder_array[i][0];
    y_i = cylinder_array[i][1];
    z_i = cylinder_array[i][2];
    r_i = cylinder_array[i][3];
    if (x_i >= 0 && x_i < Lsize && y_i >= 0 && y_i < Lsize) { // September 2018: keep centers in [0,L[x[0,L[
      env->obj_perp_pos[ok_cnt] = x_i;
      env->obj_pile_pos[ok_cnt] = y_i;
      env->obj_cdir_pos[ok_cnt] = z_i; // actually useless for infinite cylinders
      env->radii[ok_cnt] = r_i;
      ok_cnt++;
    }
  }
  printf("MP3D_create_environment_heterpack: Kept %d cylinders after clone removal.\n", ok_cnt);
  // Free unused space if ok_cnt < numcyls
  double* tmp_pile_pos = (double*)realloc(env->obj_pile_pos, ok_cnt * sizeof(double)); // realloc releases the old block of memory pointed to by first arg
  double* tmp_perp_pos = (double*)realloc(env->obj_perp_pos, ok_cnt * sizeof(double)); // truncates data if new size is shorter than previous size
  double* tmp_cdir_pos = (double*)realloc(env->obj_cdir_pos, ok_cnt * sizeof(double));
  double* tmp_radii = (double*)realloc(env->radii, ok_cnt * sizeof(double));
  // Check if reallocation succeeded 
  if (tmp_pile_pos && tmp_perp_pos && tmp_cdir_pos && tmp_radii) {
    env->obj_pile_pos = tmp_pile_pos;
    env->obj_perp_pos = tmp_perp_pos;
    env->obj_cdir_pos = tmp_cdir_pos;
    env->radii = tmp_radii;
  }
  else {
    printf("MP3D_create_environment_heterpack ERROR: memory reallocation "
           "failed during positioning of objects within their bounding boxes "
           "or when storing radii. ABORTING.\n");
    exit(-1);
  }

  env->popsizes[0] = ok_cnt;
  env->poptypes[0] = kCylinder;
  env->totobjects = ok_cnt;
  env->cell_pile_sizes[0] = Lsize;
  env->cell_perp_sizes[0] = Lsize;
  env->cell_cdir_sizes[0] = 0.0;

  env->cdir[0] = refdir[0];
  env->cdir[1] = refdir[1];
  env->cdir[2] = refdir[2];
  // Perpendicular direction obtained as pilingxcdir (vector cross product)
  cross_3D(&(env->perpdir[0]), env->piling_dir, &(env->cdir[0]));
  env->off_pile[0] = 0.0;
  env->off_perp[0] = 0.0;
  env->off_cdir[0] = 0.0;

  env->obj_end_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->obj_start_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->obj_start_ind[0] = 0;
  env->obj_end_ind[0] = env->popsizes[0] - 1;

  env->pile_period = Lsize;

  // We might want to include MP3D_check_consistency here...
  // Intialize particle dropping
  MP3D_compute_generator(env);

  // Clean up if cylinders read from file with dynamic memory allocation
  if (from_file) {
    for (i = 0; i < numcyls; i++) {
      free(cylinder_array[i]);
    }
    free(cylinder_array);
  }
}


/*
Wrapper for an environment containing spheres with radius heterogeneity.

Inputs:
- env : pointer to 3D environment structure
- Lsize : size of square lattice containing the cylinders
- spherefilepath : path to 4 column text file where separator is
                  a space or tab (no commas), containg the x-, y- and z-
                  coordinates of the center and the radius.

Date: November 28, 2018
*/
//void MP3D_create_environment_sphere_heterpack(MP3D_env* env,
//    double Lsize, char* spherefilepath) {
//  unsigned int dim = 3;
//
//  double refdir[3] = { 0.0,0.0,1.0 };    // z-axis by default
//  env->piling_dir[0] = 0.0;
//  env->piling_dir[1] = 1.0;
//  env->piling_dir[2] = 0.0;        // ideally this should be used for Rodriguez rotation formula
//
//  // Populations' properties:
//  env->numpop = 1;
//  env->popsizes = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
//  env->poptypes = (PopulationTypes*)malloc(env->numpop * sizeof(PopulationTypes));
//  env->cell_pile_sizes = (double*)malloc(env->numpop * sizeof(double));
//  env->cell_perp_sizes = (double*)malloc(env->numpop * sizeof(double));
//  env->cell_cdir_sizes = (double*)malloc(env->numpop * sizeof(double));
//  env->off_pile = (double*)malloc(env->numpop * sizeof(double));
//  env->off_perp = (double*)malloc(env->numpop * sizeof(double));
//  env->off_cdir = (double*)malloc(env->numpop * sizeof(double));
//  env->cdir = (double*)malloc(env->numpop*dim * sizeof(double));
//  env->perpdir = (double*)malloc(env->numpop*dim * sizeof(double));
//  unsigned int numspheres;
//
//  double** sphere_array = import_porefile_alloc(spherefilepath, &numspheres);
//
//  // Individual object properties
//  env->obj_pile_pos = (double*)malloc(numspheres * sizeof(double));
//  env->obj_perp_pos = (double*)malloc(numspheres * sizeof(double));
//  env->obj_cdir_pos = (double*)calloc(numspheres, sizeof(double));    // 0 by default
//  env->radii = (double*)malloc(numspheres * sizeof(double));
//
//  // Filter out clone cylinders, considering origin of elementary cell at (0,0)
//  unsigned int i;
//  double x_i, y_i, z_i, r_i;
//  unsigned int ok_cnt = 0;
//  printf("MP3D_create_environment_sphere_heterpack: Detected %d spheres in spherefile %s.\n", numspheres, spherefilepath);
//  for (i = 0; i < numspheres; i++) {
//    x_i = sphere_array[i][0];
//    y_i = sphere_array[i][1];
//    z_i = sphere_array[i][2];
//    r_i = sphere_array[i][3];
//    if (x_i >= 0 && x_i < Lsize && y_i >= 0 && y_i < Lsize && z_i >= 0 && z_i < Lsize) { // keep centers in [0,L[ x [0,L[ x [0, L[
//      env->obj_perp_pos[ok_cnt] = x_i;
//      env->obj_pile_pos[ok_cnt] = y_i;
//      env->obj_cdir_pos[ok_cnt] = z_i; // actually useless for infinite cylinders
//      env->radii[ok_cnt] = r_i;
//      ok_cnt++;
//    }
//  }
//  printf("MP3D_create_environment_sphere_heterpack: Kept %d spheres after clone removal.\n", ok_cnt);
//  // Free unused space if ok_cnt < numspheres
//  double* tmp_pile_pos = (double*)realloc(env->obj_pile_pos, ok_cnt * sizeof(double)); // realloc releases the old block of memory pointed to by first arg
//  double* tmp_perp_pos = (double*)realloc(env->obj_perp_pos, ok_cnt * sizeof(double)); // truncates data if new size is shorter than previous size
//  double* tmp_cdir_pos = (double*)realloc(env->obj_cdir_pos, ok_cnt * sizeof(double));
//  double* tmp_radii = (double*)realloc(env->radii, ok_cnt * sizeof(double));
//  // Check if reallocation succeeded 
//  if (tmp_pile_pos && tmp_perp_pos && tmp_cdir_pos && tmp_radii) {
//    env->obj_pile_pos = tmp_pile_pos;
//    env->obj_perp_pos = tmp_perp_pos;
//    env->obj_cdir_pos = tmp_cdir_pos;
//    env->radii = tmp_radii;
//  }
//  else {
//    printf("MP3D_create_environment_sphere_heterpack ERROR: memory reallocation failed during positioning of objects within their bounding boxes or when storing radii. ABORTING.\n");
//    exit(-1);
//  }
//
//  env->popsizes[0] = ok_cnt;
//  env->poptypes[0] = kSphere;
//  env->totobjects = ok_cnt;
//  env->cell_pile_sizes[0] = Lsize;
//  env->cell_perp_sizes[0] = Lsize;
//  env->cell_cdir_sizes[0] = Lsize;
//
//  env->cdir[0] = refdir[0];
//  env->cdir[1] = refdir[1];
//  env->cdir[2] = refdir[2];
//  // Perpendicular direction obtained as pilingxcdir (vector cross product)
//  cross_3D(&(env->perpdir[0]), env->piling_dir, &(env->cdir[0]));
//  env->off_pile[0] = 0.0;
//  env->off_perp[0] = 0.0;
//  env->off_cdir[0] = 0.0;
//
//  env->obj_end_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
//  env->obj_start_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
//  env->obj_start_ind[0] = 0;
//  env->obj_end_ind[0] = env->popsizes[0] - 1;
//
//  env->pile_period = Lsize;
//
//  // We might want to include MP3D_check_consistency here...
//  // Intialize particle dropping
//  MP3D_compute_generator(env);
//
//  // Clean up
//  for (i = 0; i < numspheres; i++) {
//    free(sphere_array[i]);
//  }
//  free(sphere_array);
//}


/*
Wrapper for an environment containing spheres with radius heterogeneity.

Inputs:
- env : pointer to 3D environment structure
- params: see MP3D_create_environment_heterpack above for cylinders

Date: November 28, 2018
Updated: June 2019
*/
void MP3D_create_environment_sphere_heterpack(
  MP3D_env* env, struct PackedPoreSubstrateParams params) {

  // Extract parameter that are always set
  bool from_file = params.from_file;
  double Lsize = params.lattice_size;

  unsigned int dim = 3;

  double refdir[3] = { 0.0,0.0,1.0 };    // z-axis by default
  env->piling_dir[0] = 0.0;
  env->piling_dir[1] = 1.0;
  env->piling_dir[2] = 0.0;        // ideally this should be used for Rodriguez rotation formula

                                  // Populations' properties:
  env->numpop = 1;
  env->popsizes = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->poptypes = (PopulationTypes*)malloc(env->numpop * sizeof(PopulationTypes));
  env->cell_pile_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->cell_perp_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->cell_cdir_sizes = (double*)malloc(env->numpop * sizeof(double));
  env->off_pile = (double*)malloc(env->numpop * sizeof(double));
  env->off_perp = (double*)malloc(env->numpop * sizeof(double));
  env->off_cdir = (double*)malloc(env->numpop * sizeof(double));
  env->cdir = (double*)malloc(env->numpop*dim * sizeof(double));
  env->perpdir = (double*)malloc(env->numpop*dim * sizeof(double));

  // Get sphere positions and radii
  unsigned int numspheres;
  double** sphere_array;
  if (from_file) {
    sphere_array = import_porefile_alloc(params.porefilepath, &numspheres);
    printf("MP3D_create_environment_sphere_heterpack: Detected %d spheres"
           " in spherefile %s.\n", numspheres, params.porefilepath);
  }
  else {
    sphere_array = params.pore_array;
    numspheres = params.num_lines;
    printf("MP3D_create_environment_sphere_heterpack: got list of %d"
           " spheres.\n", numspheres);
  }

  // Individual object properties
  env->obj_pile_pos = (double*)malloc(numspheres * sizeof(double));
  env->obj_perp_pos = (double*)malloc(numspheres * sizeof(double));
  env->obj_cdir_pos = (double*)calloc(numspheres, sizeof(double));    // 0 by default
  env->radii = (double*)malloc(numspheres * sizeof(double));

  // Filter out clone cylinders, considering origin of elementary cell at (0,0)
  unsigned int i;
  double x_i, y_i, z_i, r_i;
  unsigned int ok_cnt = 0;
  
  for (i = 0; i < numspheres; i++) {
    x_i = sphere_array[i][0];
    y_i = sphere_array[i][1];
    z_i = sphere_array[i][2];
    r_i = sphere_array[i][3];
    if (x_i >= 0 && x_i < Lsize && y_i >= 0 && y_i < Lsize && z_i >= 0 && z_i < Lsize) { // keep centers in [0,L[ x [0,L[ x [0, L[
      env->obj_perp_pos[ok_cnt] = x_i;
      env->obj_pile_pos[ok_cnt] = y_i;
      env->obj_cdir_pos[ok_cnt] = z_i; // actually useless for infinite cylinders
      env->radii[ok_cnt] = r_i;
      ok_cnt++;
    }
  }
  printf("MP3D_create_environment_sphere_heterpack: Kept %d spheres after clone removal.\n", ok_cnt);
  // Free unused space if ok_cnt < numspheres
  double* tmp_pile_pos = (double*)realloc(env->obj_pile_pos, ok_cnt * sizeof(double)); // realloc releases the old block of memory pointed to by first arg
  double* tmp_perp_pos = (double*)realloc(env->obj_perp_pos, ok_cnt * sizeof(double)); // truncates data if new size is shorter than previous size
  double* tmp_cdir_pos = (double*)realloc(env->obj_cdir_pos, ok_cnt * sizeof(double));
  double* tmp_radii = (double*)realloc(env->radii, ok_cnt * sizeof(double));
  // Check if reallocation succeeded 
  if (tmp_pile_pos && tmp_perp_pos && tmp_cdir_pos && tmp_radii) {
    env->obj_pile_pos = tmp_pile_pos;
    env->obj_perp_pos = tmp_perp_pos;
    env->obj_cdir_pos = tmp_cdir_pos;
    env->radii = tmp_radii;
  }
  else {
    printf("MP3D_create_environment_sphere_heterpack ERROR: memory reallocation failed during positioning of objects within their bounding boxes or when storing radii. ABORTING.\n");
    exit(-1);
  }

  env->popsizes[0] = ok_cnt;
  env->poptypes[0] = kSphere;
  env->totobjects = ok_cnt;
  env->cell_pile_sizes[0] = Lsize;
  env->cell_perp_sizes[0] = Lsize;
  env->cell_cdir_sizes[0] = Lsize;

  env->cdir[0] = refdir[0];
  env->cdir[1] = refdir[1];
  env->cdir[2] = refdir[2];
  // Perpendicular direction obtained as pilingxcdir (vector cross product)
  cross_3D(&(env->perpdir[0]), env->piling_dir, &(env->cdir[0]));
  env->off_pile[0] = 0.0;
  env->off_perp[0] = 0.0;
  env->off_cdir[0] = 0.0;

  env->obj_end_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->obj_start_ind = (unsigned int*)malloc(env->numpop * sizeof(unsigned int));
  env->obj_start_ind[0] = 0;
  env->obj_end_ind[0] = env->popsizes[0] - 1;

  env->pile_period = Lsize;

  // We might want to include MP3D_check_consistency here...
  // Intialize particle dropping
  MP3D_compute_generator(env);

  // Clean up if spheres read from file with dynamic memory allocation
  if (from_file) {
    for (i = 0; i < numspheres; i++) {
      free(sphere_array[i]);
    }
    free(sphere_array);
  }
}

/*
Performs a series of quick sanity checks on an environment.
Prints warnings or errors to console. Returns 0 if an error was
detected, 1 if the environment seems safe.

Should always be called after environment creation.

TODO: exit the program when an error is found. (right now
it is left to the user to treat a return value of 0).

New September 2018:
 - ensure that cell_cdir_size[i] for cylinder population i be zero
 - object centers no longer allowed to be outside their bounding box 
*/
unsigned char MP3D_check_consistency(MP3D_env* subinfo) {
  unsigned char env_ok = 1;
  unsigned int numpop = subinfo-> numpop;
    
  unsigned int i, j;

  // Check intracellular volume fraction in each population
  double fic_tot;
  double* fic_pops = (double*)calloc(subinfo->numpop, sizeof(double));
  MP3D_get_fic(&fic_tot, fic_pops, subinfo);
  for (i = 0; i < subinfo->numpop; i++) {
    if (fic_pops[i] < 0) {
      env_ok = 0;
      printf("MP3D_check_consistency ERROR: intracellular volume fraction"
             " of %s population %d/%d negative (found %g).\n",
             kTypeNames[subinfo->poptypes[i]], i+1, subinfo->numpop,
             fic_pops[i]);
    }
    if (fic_pops[i] > 1) {
      env_ok = 0;
      printf("MP3D_check_consistency ERROR: intracellular volume fraction"
             " of %s population %d/%d greater than one (found %g)."
             "This could be due to object overlap or too small an"
             " elementary bounding box, for instance.\n",
             kTypeNames[subinfo->poptypes[i]], i+1, subinfo->numpop,
             fic_pops[i]);
    }
  }
  // Clean up
  free(fic_pops);

  // Check total intracellular volume fraction
  if (fic_tot < 0) {
    env_ok = 0;
    printf("MP3D_check_consistency ERROR: total intracellular volume"
           " fraction of environment negative (found %g).\n", fic_tot);
  }
  if (fic_tot > 1) {
    env_ok = 0;
    printf("MP3D_check_consistency ERROR: total intracellular volume"
           " fraction of environment greater than one (found %g).\n",
           fic_tot);
  }

  // Check right-hand rule for three directions of each populations
  for (i=0; i< subinfo->numpop; i++) {
    double crossprod[3];
    double cdir_i[3] = {subinfo->cdir[3*i],subinfo->cdir[3*i+1],subinfo->cdir[3*i+2]};
    double pdir_i[3] = {subinfo->perpdir[3*i],subinfo->perpdir[3*i+1],subinfo->perpdir[3*i+2]};
    cross_3D(crossprod, cdir_i, pdir_i);
    double diff_vec[3] = {crossprod[0]-subinfo->piling_dir[0],
                          crossprod[1]-subinfo->piling_dir[1],
                          crossprod[2]-subinfo->piling_dir[2]};
    if (norm(diff_vec,3)> 1e-9) {
      env_ok = 0 ;
      printf("MP3D_check_consistency ERROR: cdir-perpdir pair of "
              "population %d (type %s) does not satisfy cross(cdir,perpdir)=piling_dir\n",
              i+1, kTypeNames[subinfo->poptypes[i]]) ;
    }
  }
  // Issue warnings for overlapping elementary bounding boxes
  for (i=0; i< numpop ; i++) {
    for (j=i+1; j<numpop; j++) {
      double pile_start[2] = {subinfo->off_pile[i], subinfo->off_pile[j]};
      double pile_end[2] = {subinfo->off_pile[i]+subinfo->cell_pile_sizes[i],
                            subinfo->off_pile[j]+subinfo->cell_pile_sizes[j]};
      if (smallest(pile_end, 2) > largest(pile_start, 2)) {
        printf("MP3D_check_consistency WARNING: "
               "populations %d (type %s)"
               " and %d (type %s) have overlapping elementary cells.\n",
               i+1, kTypeNames[subinfo->poptypes[i]],
               j+1, kTypeNames[subinfo->poptypes[j]]);
      }
    }
  }

  // Check periodicity along piling directions (sometimes overlap is intentional) 
  if (numpop > 0) { // do not check this for the empty environment
    std::vector<double> pile_reach(numpop);
    add_arrays(&pile_reach[0],
               subinfo->off_pile,
               subinfo->cell_pile_sizes, numpop);
    if (subinfo->pile_period < (largest(&pile_reach[0], numpop)
        - smallest(subinfo->off_pile, numpop))) {
      printf("MP3D_check_consistency WARNING: pile_period is so"
             " short first and last cells overlap.\n");
    }
  }

  // TODO: check dimensions and sizes as in Matlab code.
  // Feasible if everything uses std::vector instead of
  // dynamically-allocated arrays.

  // Check that the size along the principal direction of a cylinder
  // population's elementary cell is 0 to ensure proper uniform particle
  // dropping, proper spatial sub-gridding, etc.
  for (i = 0; i < numpop; i++) {
    if (subinfo->poptypes[i] == kCylinder) {
      if (subinfo->cell_cdir_sizes[i] != 0.0) {
        env_ok = 0;
        printf("MP3D_check_consistency ERROR: %s population %d/%d"
               " should have cell_cdir_size 0, detected %g.\n",
                kTypeNames[subinfo->poptypes[i]], i+1, numpop, subinfo->cell_cdir_sizes[i]);
      }
    }
  }

  // Check position of objects within their populations' bounding boxes.
  // Issue warnings if not fully contained within box (sometimes intentional
  // when creating dense packings).
  // Throw errors when centers of objects are outside their bounding box,
  // with a refined error message if the object is completely outside 
  // its bounding box.

  unsigned int st_ind;
  for (i=0; i<numpop; i++) {
    st_ind = subinfo->obj_start_ind[i];
    for (j=0; j<subinfo->popsizes[i]; j++) {
      // Perpendicular direction
      if ((subinfo->obj_perp_pos[st_ind + j] - subinfo->radii[st_ind + j]
          >= subinfo->cell_perp_sizes[i]) ||
          (subinfo->obj_perp_pos[st_ind + j] + subinfo->radii[st_ind + j]
          <= 0.0)) {
        env_ok = 0;
        printf("MP3D_check_consistency ERROR: object %d in pop. %d (%d in "
               "complete obj. list) is entirely outside its population "
               "bounding box along its perpendicular direction\n",
               j, i+1, st_ind+j);
      } else if ((subinfo->obj_perp_pos[st_ind + j]
                 > subinfo->cell_perp_sizes[i]) ||
                 (subinfo->obj_perp_pos[st_ind + j]
                 < 0.0)) {
        env_ok = 0;
        printf("MP3D_check_consistency ERROR: object %d in pop. %d (%d in "
               "complete obj. list) has its center outside its population "
               "bounding box along its perpendicular direction\n",
               j, i+1, st_ind+j);
      } else if ((subinfo->obj_perp_pos[st_ind+j]-subinfo->radii[st_ind+j] < 0.0)
                 || (subinfo->obj_perp_pos[st_ind+j]+subinfo->radii[st_ind+j]
                 > subinfo->cell_perp_sizes[i])) {
        printf("MP3D_check_consistency WARNING: object %d in %s pop. %d "
               "(%d in complete obj. list) is not confined within its "
               "population bounding box along its perpendicular direction\n",
               j, kTypeNames[subinfo->poptypes[i]], i+1, st_ind+j);
      }
      // Piling direction
      if ((subinfo->obj_pile_pos[st_ind + j] - subinfo->radii[st_ind + j]
          >= subinfo->cell_pile_sizes[i]) || (subinfo->obj_pile_pos[st_ind+j]
          + subinfo->radii[st_ind + j] <= 0.0)) {
        env_ok = 0;
        printf("MP3D_check_consistency ERROR: object %d in pop. %d (%d in "
               "complete obj. list) is entirely outside its population "
               "bounding box in the piling direction\n",
               j, i+1, st_ind+j);
      } else if ((subinfo->obj_pile_pos[st_ind + j]
                 > subinfo->cell_pile_sizes[i])
                 || (subinfo->obj_pile_pos[st_ind + j] < 0.0)) {
        env_ok = 0;
        printf("MP3D_check_consistency ERROR: object %d in %s pop. %d (%d in"
               " complete obj. list) has its center outside its population "
               "bounding box in the piling direction\n",
               j, kTypeNames[subinfo->poptypes[st_ind + j]], i+1, st_ind+j);
      } else if ((subinfo->obj_pile_pos[st_ind+j]-subinfo->radii[st_ind+j]
                 < 0)
                 || (subinfo->obj_pile_pos[st_ind+j]+subinfo->radii[st_ind+j]
                 > subinfo->cell_pile_sizes[i])) {
         printf("MP3D_check_consistency WARNING: object %d in %s pop. %d (%d "
                "in complete obj. list) is not confined within its "
                "population bounding box in the piling direction\n",
                 j, kTypeNames[subinfo->poptypes[i]], i+1, st_ind+j);
      }
      // Main (cdir) direction
      if ( subinfo->poptypes[i]== kSphere ) {
        if ((subinfo->obj_cdir_pos[st_ind + j] - subinfo->radii[st_ind + j]
             >= subinfo->cell_cdir_sizes[i]) ||
             (subinfo->obj_cdir_pos[st_ind + j]
             + subinfo->radii[st_ind + j] <= 0.0)) {
          env_ok = 0;
          printf("MP3D_check_consistency ERROR: object %d in sphere pop. %d "
                 "(%d in complete obj. list) is entirely outside its population"
                 " bounding box along its cdir/main direction\n",
                 j, i+1, st_ind+j);
        } else if ((subinfo->obj_cdir_pos[st_ind + j] >
                    subinfo->cell_cdir_sizes[i])
                    || (subinfo->obj_cdir_pos[st_ind + j] < 0.0)) {
          env_ok = 0;
          printf("MP3D_check_consistency ERROR: object %d in pop. %d (%d "
                 "in complete obj. list) has its center outside its "
                 "population bounding box along its cdir/main direction\n",
                 j, i+1, st_ind+j);
        } else if ((subinfo->obj_cdir_pos[st_ind+j]-subinfo->radii[st_ind+j]
                    < 0.0) ||
                    (subinfo->obj_cdir_pos[st_ind+j]+subinfo->radii[st_ind+j]
                    > subinfo->cell_cdir_sizes[i])) {
          printf("MP3D_check_consistency WARNING: object %d in sphere pop."
                 " %d (%d in complete obj. list) is not confined within its"
                 " population bounding box along its cdir/main direction\n",
                 j, i+1, st_ind+j);
        }
      }
    }  // for j
  }  // for i
    return env_ok ;
}


/*
This routine must be called on any created environment to free memory.
*/
void MP3D_clean_env(MP3D_env* env){
  free(env->popsizes) ;
  free(env->poptypes) ;
  free(env->obj_pile_pos) ;
  free(env->obj_perp_pos) ;
  free(env->obj_cdir_pos) ;
  free(env->radii) ;
  free(env->cell_pile_sizes) ;
  free(env->cell_perp_sizes) ;
  free(env->cell_cdir_sizes) ;
  free(env->off_pile) ;
  free(env->off_perp) ;
  free(env->off_cdir) ;
  free(env->perpdir) ;
  free(env->cdir) ;
  free(env->obj_start_ind) ;
  free(env->obj_end_ind) ;

  free(env->noncolinear_cdirs);
  free(env->noncolinear_pdirs);
  free(env->cdirdists);
  free(env->perpdists);
}

/*
Computes intracellular volume fractions.

Writes total intracellular volume fraction of environment env to fic_tot.
Writes intracellular volume fraction of each population in array fic_pops,
relative to the space alloted to that population which is based on the 
elementary cell containing the objects of that population.
Note that this separation is arbitrary and may not always have a sound
physical meaning.
*/
void MP3D_get_fic(double* fic_tot, double* fic_pops, MP3D_env* env) {
  unsigned int indstart, indend, i, iobj ;
  double Vol_pop = 1.0;
  double Vinpop = 2.0;    // dummy values to avoid potentially uninitialized local variable warning
  for (i = 0; i< env->numpop; i++){
    indstart = env->obj_start_ind[i] ;
    indend = env-> obj_end_ind[i] ;
    if (env->poptypes[i] == kCylinder)
    {
        Vol_pop = (env->cell_pile_sizes[i])*(env->cell_perp_sizes[i]) ;
        //an interesting quantity is with env->pile_period to get absolute ic vol fraction
        Vinpop = pow(env->radii[indstart],2) ;
        for (iobj = indstart+1; iobj <= indend; iobj++)
        {
          Vinpop += pow(env->radii[iobj],2) ;
        }
        Vinpop *= M_PI ;
    }
    else if (env->poptypes[i] == kSphere)
    {
        Vol_pop = (env->cell_pile_sizes[i])*(env->cell_perp_sizes[i])*(env->cell_cdir_sizes[i] );
        //an interesting quantity is with env->pile_period to get absolute ic vol fraction
        Vinpop = pow(env->radii[indstart],3) ;
        for (iobj=indstart+1; iobj<=indend;iobj++)
        {
          Vinpop += pow(env->radii[iobj],3) ;
        }
        Vinpop *= M_PI*4.0/3.0 ;
    }        
    fic_pops[i] = Vinpop/Vol_pop ;
  }
  //fin_tot = sum(subinfo.cell_pile_sizes.*fin_pops)/(env->pile_period) ;
  *fic_tot = (env->cell_pile_sizes[0])*fic_pops[0] ;
  for (i=1; i< env->numpop; i++){ 
    *fic_tot += (env->cell_pile_sizes[i])*fic_pops[i] ;
  }
  *fic_tot /= env->pile_period ;
}


/*
Returns absolute fraction of total space occupied by intracellular space of
population popID in environment env.
*/
double MP3D_get_pop_fic(unsigned int popID, MP3D_env* env) {
  double Vin, Vtot ;
  unsigned int i ;
  if (env->poptypes[popID] == kCylinder)
  {
    double sumRsq = 0 ;
    for (i = env->obj_start_ind[popID]; i<= env->obj_end_ind[popID]; i++)
    {
        sumRsq += pow(env->radii[i],2) ;
    }
    Vin = M_PI*sumRsq ;
    Vtot = (env->pile_period)*(env->cell_perp_sizes[popID]) ;
  }
  else if (env->poptypes[popID] == kSphere)
  {
    double sumRcub = 0 ;
    for (i = env->obj_start_ind[popID]; i<= env->obj_end_ind[popID] ; i++)
    {
        sumRcub += pow(env->radii[i],3) ;
    }
    Vin = 4.0*M_PI*sumRcub/3.0 ;
    Vtot = (env->pile_period)*(env->cell_perp_sizes[popID])*(env->cell_cdir_sizes[popID]) ;
  }
  else
  {
    Vin = 0 ;
    Vtot = 1 ;
    printf("WARNING MP3D_get_fic_pop_multipop : unknown population type.\n");
 }
  return Vin/Vtot ;
}



/*
Returns true (=1) if position x is in the intracellular space of environment
env.
Returns population ID indpopin of the population containing position x in its
intracellular space, if applicable.

Inputs:
 - x: spin 3D position
 - env: pointer to 3D environment structure
 - cand_obj: must be at least env->totobjects long

Requires:
 - call to MP3D_initialize_spatial_sectors on env once the jump
   length used in the simulation is known.

Modifies:
 - indpopin
 - cand_obj via calls to MP3D_get_sector_objects

Implementation using spatial sectors initialized by
MP3D_initialize_spatial_sectors()
*/
unsigned char MP3D_is_intra(double(*x)[3], MP3D_env* env,
    unsigned int* indpopin, std::vector<unsigned int> & cand_obj) {
  unsigned char isintra = 0;        // return value : 0 or 1

  // NOTE2SELF: double (*x)[3] is a pointer to a pointer holding the first
  // of three contiguous double variables in memory
  unsigned char dim = 3;          // number of spatial dimensions
  double Lpile = env->pile_period;
  *indpopin = env->numpop + 1;      // index of population the spin at x is in
  double xpile = (*x)[0] * (env->piling_dir[0]) 
      + (*x)[1] * (env->piling_dir[1])
      + (*x)[2] * (env->piling_dir[2]); //(*(*x+2)) works too


  unsigned int obj_cnt, ind_num_obj, indpop = 0, Nsectors, Ncand, ind_dir;
  double xperp, xcdir, off_pile_pop, off_perp_pop;
  

  while ((indpop < env->numpop) && !isintra) {
    // From lab frame to population reference frame
    ind_dir = dim*indpop;  // env contains only 1D arrays
    xperp = (*x)[0] * env->perpdir[ind_dir + 0]
        + (*x)[1] * env->perpdir[ind_dir + 1]
        + (*x)[2] * env->perpdir[ind_dir + 2];
    xcdir = (*x)[0] * env->cdir[ind_dir + 0]
        + (*x)[1] * env->cdir[ind_dir + 1]
        + (*x)[2] * env->cdir[ind_dir + 2]; // useful for sphere populations
    off_pile_pop = env->off_pile[indpop];
    off_perp_pop = env->off_perp[indpop];

    // Info from spatial sub-gridding
    // TODO: compute once and for all in MP3D_initialize_spatial_sectors
    Nsectors = env->Nperp[indpop] * env->Npile[indpop] * env->Ncdir[indpop];

    if (Nsectors > 1) {
      // Get sector objects as if in a jump from x to x itself
      // (duplicate operations in MP3D_get_sector_objects)
      MP3D_get_sector_objects(env, Ncand, indpop, xperp, xpile, xcdir,
        xperp, xpile, xcdir, cand_obj);
    }
    else {
      // avoid call to get_sector_objects if no spatial sub-gridding was used
      Ncand = env->popsizes[indpop];
    }

    for (ind_num_obj = 0; ind_num_obj< Ncand; ind_num_obj++) {

      if (Nsectors > 1) {
        // checking only close objects from spatial subgridding
        obj_cnt = cand_obj[ind_num_obj];
      }
      else {
        // when checking all objects of the population
        obj_cnt = env->obj_start_ind[indpop] + ind_num_obj;
      }
      double off_obj_pile = off_pile_pop + env->obj_pile_pos[obj_cnt]; // in "first" unit cell
      double off_obj_perp = off_perp_pop + env->obj_perp_pos[obj_cnt]; // in "first" unit cell
      double cent_pile_min = off_obj_pile + floor((xpile - off_obj_pile) / Lpile)*Lpile;
      double cent_pile_max = off_obj_pile + ceil((xpile - off_obj_pile) / Lpile)*Lpile; // TODO precompute (x-off)/L once and reuse it maybe
      double cent_perp_min = off_obj_perp + floor((xperp - off_obj_perp) / (env->cell_perp_sizes[indpop]))*env->cell_perp_sizes[indpop];
      double cent_perp_max = off_obj_perp + ceil((xperp - off_obj_perp) / (env->cell_perp_sizes[indpop]))*env->cell_perp_sizes[indpop];
      // Identify which of the 4 surrounding cylinders/sphers is the closest in <perpdir,piling_dir> plane
      double cent_pile_near, cent_perp_near;
      if ((xpile - cent_pile_min) < (cent_pile_max - xpile)) {
        cent_pile_near = cent_pile_min;
      }
      else {
        cent_pile_near = cent_pile_max;
      }
      if ((xperp - cent_perp_min) < (cent_perp_max - xperp)) {
        cent_perp_near = cent_perp_min;
      }
      else {
        cent_perp_near = cent_perp_max;
      }
      double sq_dist = 0;
      //printf("DBG MP3D_is_intra : obj %d: cent_pile_near=%5.4e, cent_perp_near=%5.4e\n",obj_cnt,cent_pile_near, cent_perp_near) ;
      // For sphere pops, determine if sphere "above" or "under" is closer
      if (env->poptypes[indpop] == kSphere) {
        double off_obj_cdir = env->off_cdir[indpop] + env->obj_cdir_pos[obj_cnt];
        double cent_cdir_min = off_obj_cdir + floor((xcdir - off_obj_cdir) / (env->cell_cdir_sizes[indpop]))*env->cell_cdir_sizes[indpop];
        double cent_cdir_max = off_obj_cdir + ceil((xcdir - off_obj_cdir) / (env->cell_cdir_sizes[indpop]))*env->cell_cdir_sizes[indpop];
        double cent_cdir_near;
        if ((xcdir - cent_cdir_min) < (cent_cdir_max - xcdir)) {
          cent_cdir_near = cent_cdir_min;
        }
        else {
          cent_cdir_near = cent_cdir_max;
        }
        sq_dist = pow(xcdir - cent_cdir_near, 2);
        //printf("DBG MP3D_is_intra : obj %d: cent_cdir_near=%5.4e\n",obj_cnt,cent_cdir_near) ;
      }

      sq_dist = sq_dist +
                pow(xpile - cent_pile_near, 2) +
                pow(xperp - cent_perp_near, 2);
      double dist = sqrt(sq_dist);
      // Comparing the square distances seemed less stable numerically
      if (dist < env->radii[obj_cnt]) {
        isintra = 1;
        *indpopin = indpop;
        //printf("DBG MP3D_is_intra : spin is inside population %d ! \n",indpopin+1) ;
        break;
      }
    } // for ind_num_obj
    indpop++;
  } // while indpop
  return isintra;

}


/*
Prints description of environment env
*/
void MP3D_print_info(MP3D_env* myenv) {
  if (myenv->numpop == 0) { //empty environment
    printf("\nEmpty environment for free diffusion.\n");
    return;
  }

  printf("\nEnvironment characteristics:\n");
  // Get relative intracellular volume fractions of each population and total intracellular volume fraction
  std::vector<double> fic_pops(myenv->numpop);
  double fic_tot;
  MP3D_get_fic(&fic_tot, &fic_pops[0], myenv);

  unsigned int i;
  // Display environment properties
  printf("\nThe piling direction is [%.4g %.4g %.4g] and the piling period is %g\n", myenv->piling_dir[0], myenv->piling_dir[1], myenv->piling_dir[2], myenv->pile_period);

  std::vector<double> fic_pops_abs(myenv->numpop);
  for (i = 0; i < myenv->numpop; i++) {
    fic_pops_abs[i] = MP3D_get_pop_fic(i, myenv);  // gives fraction of total space occupied by intracellular space of that population
    printf("\nPopulation %d/%d contains %d %s(s) (rel fic=%g, "
           "abs fic=%g)\n\tpile-perp-cdir sizes %.5gx%.5gx%.5g and offsets "
           "%.5g %.5g %.5g.\n",
           i + 1, myenv->numpop, myenv->popsizes[i], kTypeNames[myenv->poptypes[i]], fic_pops[i], fic_pops_abs[i],
           myenv->cell_pile_sizes[i], myenv->cell_perp_sizes[i], myenv->cell_cdir_sizes[i],
           myenv->off_pile[i], myenv->off_perp[i], myenv->off_cdir[i]);
    printf("\tperp direction : [%.4g %.4g %.4g] cyl direction : [%.4g %.4g %.4g]\n",
           myenv->perpdir[3 * i + 0], myenv->perpdir[3 * i + 1], myenv->perpdir[3 * i + 2],
           myenv->cdir[3 * i + 0], myenv->cdir[3 * i + 1], myenv->cdir[3 * i + 2]);
  }
  
  printf("\nList of linearly-independent populations directions constituting the elementary polyhedron used for particle dropping:\n");
  for (i = 0; i < myenv->num_noncolinear_cdirs; i++) {
    printf("\t%d: cdir=[%.4g %.4g %.4g] (period: %.4g) and "
           "pdir=[%.4g %.4g %.4g] (period: %.4g)\n",
           i+1, myenv->noncolinear_cdirs[3*i], myenv->noncolinear_cdirs[3*i+1],
           myenv->noncolinear_cdirs[3*i+2], myenv->cdirdists[i],
           myenv->noncolinear_pdirs[3*i], myenv->noncolinear_pdirs[3*i+1],
           myenv->noncolinear_pdirs[3*i+2], myenv->perpdists[i]);
  }

  printf("\nThe total number of objects is %d, total intracellular fraction %g\n", myenv->totobjects, fic_tot);

  printf("\nList of objects with population-specific piling, perpendicular and cylinder-direction positions: \n");
  const int max_obj = 50;
  // min(myenv->totobjects, max_obj):
  unsigned int obj_to_print = (myenv->totobjects < max_obj) ?
                              (myenv->totobjects) : max_obj;
  // FIXME: print type of each object here
  for (i = 0; i < obj_to_print; i++) {
    printf("\tobj %d: %.4g %.4g %.4g (rad=%.3g)\n",
           i+1, myenv->obj_pile_pos[i], myenv->obj_perp_pos[i],
           myenv->obj_cdir_pos[i], myenv->radii[i]);
  }
  if (myenv->totobjects < max_obj) {
    printf("\n");
  }
  else {
    printf("\t   ...\n\n");
  }
  
}

/*
Computes the elementary polyhedron representing the global environment, i.e. the
"periodicity" of the environment along all the populations' directions. It
makes uniform particle dropping more efficient because parallel populations do
not need to be taken into account multiple times.

Linear independence between the main/cdir direction of two populations is based
on their dot product (less than, or equal to 1). For each group of parallel
populations, the perpendicular direction is also identical since the piling
direction is fixed for the whole environment.

Along the perpendicular and the main/cdir direction of each group of parallel
populations, the length of the side of the polyhedron must be computed. This is
done by calculating the least common multiplier (see real_lcm in mymaths.h) of
the elementary cell sizes of the parallel populations, along their 
perpendicular and main directions.

This function allocates memory via attributes of env:
 - env->noncolinear_cdirs
 - env->perpdists
 - env->cdirdists

*/
void MP3D_compute_generator(MP3D_env* env) {
  unsigned char dim = 3;
  unsigned int max_dir_cmp = dim* env->numpop; // upper bound on number of noncolinear population directions in environment
  env->noncolinear_cdirs = (double*)malloc(max_dir_cmp * sizeof(double));
  env->perpdists = (double*)malloc(env->numpop * sizeof(double));      // NOTE2SELF Check if pointers not NULL.. 
  env->cdirdists = (double*)malloc(env->numpop * sizeof(double));     // to account for sphere populations

  if (!env->noncolinear_cdirs || !env->perpdists || !env->cdirdists) {
    printf("MP3D_compute_generator: could not allocate memory for non-colinear population directions. ABORTING.\n");
    perror("Error detail");
    exit(1);
  }
  env->num_noncolinear_cdirs = 0;  // number of distinct, non-parallel population directions

  /* All the work in the following for loop is intended to be done once and 
  for all. The quantities, num_noncolinear_cdirs, noncolinear_cdirs,
  perpdists and cdirdists are stored as object attributes.
  */
  unsigned int i, j, cmp_pop, already_encountered, cmp_enc;
  for (i = 0; i < env->numpop; i++) {
    cmp_pop = dim*i;
    already_encountered = 0; // 0 or 1
    for (j = 0; j< env->num_noncolinear_cdirs; j++) {
      cmp_enc = dim*j;
      // check if parallel to a direction already considered
      double dotprod = env->noncolinear_cdirs[cmp_enc + 0] * env->cdir[cmp_pop + 0]
              + env->noncolinear_cdirs[cmp_enc + 1] * env->cdir[cmp_pop + 1] 
              + env->noncolinear_cdirs[cmp_enc + 2] * env->cdir[cmp_pop + 2];  
      if (fabs(fabs(dotprod) - 1.0) < 1e-8) {
        already_encountered = 1;
        break;
      }
    }
    if (already_encountered) { // keep last j-value
      // adjust length of dropping region along that direction
      env->perpdists[j] = real_lcm(env->perpdists[j], env->cell_perp_sizes[i], 1e-2);
      env->cdirdists[j] = real_lcm(env->cdirdists[j], env->cell_cdir_sizes[i], 1e-2);
    }
    else { // new independent direction
      unsigned int k;
      for (k = 0; k < dim; k++) {
        // store this new non colinear direction in environment
        env->noncolinear_cdirs[env->num_noncolinear_cdirs*dim + k] = env->cdir[i*dim + k];
      }
      // length of dropping region = size of elementary cell
      env->perpdists[env->num_noncolinear_cdirs] = env->cell_perp_sizes[i];
      env->cdirdists[env->num_noncolinear_cdirs] = env->cell_cdir_sizes[i];
      env->num_noncolinear_cdirs++;
    }
  }
  // Free unused space if num_colinear_cdirs < numpop
  double* tmp_noncolinear_cdirs = (double*)realloc(env->noncolinear_cdirs, dim * env->num_noncolinear_cdirs * sizeof(double)); // NOTE2SELF realloc releases the old block of memory pointed to by first arg
  double* tmp_perpdists = (double*)realloc(env->perpdists, env->num_noncolinear_cdirs * sizeof(double));
  double* tmp_cdirdists = (double*)realloc(env->cdirdists, env->num_noncolinear_cdirs * sizeof(double));
  // Check if reallocation succeeded, 
  if (tmp_noncolinear_cdirs && tmp_perpdists && tmp_cdirdists) {
    env->noncolinear_cdirs = tmp_noncolinear_cdirs;
    env->perpdists = tmp_perpdists;
    env->cdirdists = tmp_cdirdists;
  }
  else {
    printf("MP3D_compute_generator: could not reallocate memory after computing non-colinear population directions. ABORTING.\n");
    perror("Error detail");
    exit(1);
  }    
  // Compute perpendicular directions of non colinear populations' main directions
  env->noncolinear_pdirs = (double*)malloc(dim * env->num_noncolinear_cdirs * sizeof(double));
  if (env->noncolinear_pdirs) {
    for (i = 0; i < env->num_noncolinear_cdirs; i++) {
      cross_3D(&(env->noncolinear_pdirs[dim * i]), env->piling_dir, &(env->noncolinear_cdirs[dim*i]));
    }
  }
  else {
    printf("MP3D_compute_generator: could not allocate memory for perpendicular non-colinear population directions. ABORTING.\n");
    perror("Error detail");
    exit(1);
  }
  
}



/*
Initialize a sub-sampling of the space occupied by the elementary bounding box
of each population for more efficient intersection checking at every random 
jump.

Rule: spatial sector size > 2* max( max(env.radii), Ljump) to limit the number
of sectors which must be checked at every jump at runtime.

Another strategy could be to use fixed spatial sector size and allow
cylinders to span multiple spatial sectors, as in

Hall, Matt G., Gemma Nedjati-Gilani, and Daniel C. Alexander. 
"Realistic voxel sizes and reduced signal variation in Monte-Carlo 
simulation for diffusion MR data synthesis." 
arXiv preprint arXiv:1701.03634 (2017). 
https://arxiv.org/pdf/1701.03634.pdf

Date: September 25, 2018

*/
void MP3D_initialize_spatial_sectors(MP3D_env* env, double Ljump) {

  env->lperp = std::vector<double>(env->numpop);
  env->lpile = std::vector<double>(env->numpop);
  env->lcdir = std::vector<double>(env->numpop);

  env->Nperp = std::vector<unsigned int>(env->numpop, 0);
  env->Npile = std::vector<unsigned int>(env->numpop, 0);
  env->Ncdir = std::vector<unsigned int>(env->numpop, 0);

  unsigned int i, j, st_in, end_in, Nperp, Npile, Ncdir;
  double max_rad, min_sector_length, sector_length, lperp, lpile, lcdir;
  //double cell_vol, max_sector_length, pop_dim;

  for (i = 0; i < env->numpop; i++) {
    st_in = env->obj_start_ind[i];
    end_in = env->obj_end_ind[i];

    // Get radius of largest object in population
    max_rad = 0;
    for (j = st_in; j <= end_in; j++) {
      // max(max_rad, env->radii[j])
      max_rad = (env->radii[j] > max_rad) ? (env->radii[j]) : (max_rad);
    }

    // Sectors MUST have side at least as long as the biggest
    // diameter and twice the jump size:
    // min sector length = 2*max(Ljump, max radius)
    min_sector_length = 2 * ((Ljump > max_rad) ? (Ljump) : (max_rad));


    //// Upper bound on sector size based on expected number of objects per
    //  sector, assuming objects are uniformly distributed in the elementary
    // bounding box of the population
    //pop_dim = 2;
    //cell_vol = env->cell_perp_sizes[i] * env->cell_pile_sizes[i];
    //if (env->poptypes[i] == kSphere) {
    //  pop_dim = 3;
    //  cell_vol = cell_vol * env->cell_cdir_sizes[i];
    //}

    //// Assuming square or cubic sectors first(this will change below)
    //max_sector_length = pow(cell_vol*max_objs_per_sector / env->popsizes[i], 1.0/pop_dim);  // combine popsize/Ncells < max_objs_per_sector and Ncells = (L/sec_length)^dim = cell_vol/ sec_length^dim

    //sector_length = (max_sector_length > min_sector_length) ? (max_sector_length) : (min_sector_length);  // max(min_sector_length, max_sector_length)

    sector_length = min_sector_length;

    // Refine sector dimensions(not necessarily square / cubic anymore)
    Nperp = (unsigned int) floor(env->cell_perp_sizes[i] / sector_length);
    Nperp = (1 > Nperp) ? (1) : (Nperp);  // max(1, Nperp), important because Nperp could be zero for small bounding boxes cell_perp_sizes[i] x ..._pile_... x ..._cdir_...

    Npile = (unsigned int) floor(env->cell_pile_sizes[i] / sector_length);
    Npile = (1 > Npile) ? (1) : (Npile);  // max(1, Npile)

    Ncdir = (unsigned int) floor(env->cell_cdir_sizes[i] / sector_length);
    Ncdir = (1 > Ncdir) ? (1) : (Ncdir);  // max(1, Ncdir), should be 1 for cylinder populations

    lperp = env->cell_perp_sizes[i] / Nperp;
    lpile = env->cell_pile_sizes[i] / Npile;
    lcdir = env->cell_cdir_sizes[i] / Ncdir; // 0 for cylinder populations

    // Check final dimensions for populations in which more than one sector were created
    // There could be one single sector if the bounding box was too small or the jump length too large for instance
    if (Nperp > 1) { // with only one sector, all the objects of the population will be checked at every attempted jump bc objects must be confined in the population's bounding box
      if (lperp < min_sector_length) {
        printf("MP3D_initialize_spatial_sectors ERROR: Sector side length in "
               "perpendicular direction of population %d/%d (%s) too small.\n",
               i+1, env->numpop, kTypeNames[env->poptypes[i]]);
        exit(-1);
      }
    }
    if (Npile > 1) {
      if (lpile < min_sector_length) {
        printf("MP3D_initialize_spatial_sectors ERROR: Sector side length"
               " in piling direction of environment too small.\n");
        exit(-1);
      }
    }        
    if (env->poptypes[i] == kSphere && Ncdir > 1) {
      if (lcdir < min_sector_length) {
        printf("MP3D_initialize_spatial_sectors ERROR: Sector side length"
               " in main/cdir direction of (sphere) population %d/%d too "
               "small.\n", i+1, env->numpop);
      }
    }
    // Store sector dimensions and number of sectors
    env->lperp[i] = lperp;
    env->lpile[i] = lpile;
    env->lcdir[i] = lcdir;

    env->Nperp[i] = Nperp;
    env->Npile[i] = Npile;
    env->Ncdir[i] = Ncdir;

    // The spatial distribution of population i is a Nperp[i] x Npile[i] x Ncdir[i] array in which each entry is a list of object IDs, the length of which is still unknown
    env->obj_spatial_distribution.push_back(std::vector<std::vector<std::vector<std::vector<unsigned int> > > >(Nperp,
                                std::vector<std::vector<std::vector<unsigned int> > >(Npile,
                                        std::vector<std::vector<unsigned int> >(Ncdir, std::vector<unsigned int>()
                                        )
                                )
                        )
                        );

    //printf("DEBUG: pop %d, largest rad %g, Ljump %g => min sector length %g.\n\tMax numb. of objects %d => (desired) max sector length %g\n",
    //  i, max_rad, Ljump, min_sector_length, max_objs_per_sector, max_sector_length);

    printf("MP3D_initialize_spatial_sectors: population %d/%d (%s): "
           "spatial gridding %dx%dx%d (size %gx%gx%g)\n",
           i+1, env->numpop, kTypeNames[env->poptypes[i]],
           Nperp, Npile, Ncdir, lperp, lpile, lcdir);

    // Update list of object indices in each spatial sector
    double x_perp, x_pile, x_cdir;
    unsigned int objID, idx_perp, idx_pile, idx_cdir, tmp;
    for (j = 0; j < env->popsizes[i]; j++) {
      objID = st_in + j;                    // absolute index in list of all objects comprised in the environment
      x_perp = env->obj_perp_pos[objID];            // position in [0, cell_perp_sizes[i]]
      x_pile = env->obj_pile_pos[objID];            // position in [0, cell_pile_sizes[i]]

      tmp = (unsigned int) floor(x_perp / lperp);        // may yield Nperp if x_perp==cell_perp_sizes[i] precisely
      idx_perp = (tmp < Nperp - 1) ? (tmp) : (Nperp - 1);    // min(floor(x_perp/lperp), Nperp-1) to avoid out-of-bounds index
      tmp = (unsigned int) floor(x_pile / lpile);
      idx_pile = (tmp < Npile - 1) ? (tmp) : (Npile - 1);    // min(floor(x_pile / lpile), Npile - 1);

      idx_cdir = 0;                      // default value for cylinder populations
      if (env->poptypes[i] == kSphere) {
        x_cdir = env->obj_cdir_pos[objID];          // position in [0, cell_cdir_sizes[i]]
        tmp = (unsigned int)floor(x_cdir / lcdir);      // in [0, Ncdir], lcdir should be > 0 for sphere populations (avoids NaN)
        idx_cdir = (tmp < Ncdir - 1) ? (tmp) : (Ncdir - 1);  // min(floor(x_cdir / lcdir), Ncdir-1)
      }

      // Append object ID to appropriate spatial sector in current population
      env->obj_spatial_distribution[i][idx_perp][idx_pile][idx_cdir].push_back(objID);

      //printf("DEBUG: pop %d obj %d assigned to sector [%d][%d][%d] (#%d)\n",
      //    i, objID, idx_perp, idx_pile, idx_cdir, (int) env->obj_spatial_distribution[i][idx_perp][idx_pile][idx_cdir].size());
    }
    
  }
}



/*
Get list of objects from a given population popID which are close enough
to the start (x) and the end (xup) points of a tentative jump to be
intersected. This leads to more efficient intersection checking as this
is essentially a look-up of the table initialized at the beginning of the
simulation.

Inputs:
 - cand_obj: must be at least subinfo->totobjects long

Requires:
 - call to MP3D_initialize_spatial_sectors on subinfo once the jump length
  used in the simulation is known.

Modifies:
 - Ncand updated to number of cylinders inserted in cand_obj
 - the first Ncand entries of cand_obj.

Date: September 2018

*/
void MP3D_get_sector_objects(MP3D_env* subinfo, unsigned int & Ncand,
    unsigned int popID, double x_perp, double x_pile, double x_cdir,
    double xup_perp, double xup_pile, double xup_cdir,
    std::vector<unsigned int> & cand_obj) {
  /*
  Possible optimization: if x_sec_perp == xup_sec_perp, if both in upper half then only check 2 sectors, if both in lower half check only two sectors, if either side then check everything
  Mix that with case Nperp < 3 a bit tricky. Important for big voxel with many objects because each sector may contain many objects.
  */

  unsigned int Nperp = subinfo->Nperp[popID];
  unsigned int Npile = subinfo->Npile[popID];
  unsigned int Ncdir = subinfo->Ncdir[popID];

  unsigned int Nsec_perp = 1, Nsec_pile = 0, Nsec_cdir = 1;  // number of sectors to check (! in the pile direction it is 0 by default)

  unsigned int tmp;
  int signed_modulo;                      // used to handle negative integers in modulus operations below
//  unsigned int cnt;                      // used to manually add sector indices when fewer than 3 sectors are present along a dimension

  unsigned int sec_perp[3], sec_pile[3], sec_cdir[3];      // holds indices in each direction of all sectors to be checked

  /* 
   ! C/C++ r=fmod(a,b) in <math.h>  does not alway return a positive modulo. 
   May return in [0, b[ if a>=0, in ]-b, 0] if a < 0. Most compilers ensure 
   |r|<|b| though.
   We want our modulus operation to be in [0, b[ no matter the sign of a!
   Workaround: add b iff r is negative.
  */

  /*
  Note: I tried checking 2 instead of 3 sectors along each direction when 
  both start and end points were located on the same side of the midline of a 
  spatial sector along a given direction but curiously, that did not lead to any
  visible improvement in any of the tests I ran. This is still commented out in
  the code below for future testing.
  */

  /*
  Perpendicular direction
  */
  if (Nperp > 1) {
    // Project in [0, cell_perp_size] 
    double x_perp_cell = fmod(x_perp - subinfo->off_perp[popID], subinfo->cell_perp_sizes[popID]);
    x_perp_cell += (x_perp_cell < 0) * subinfo->cell_perp_sizes[popID];
    double xup_perp_cell = fmod(xup_perp - subinfo->off_perp[popID], subinfo->cell_perp_sizes[popID]);
    xup_perp_cell += (xup_perp_cell < 0) * subinfo->cell_perp_sizes[popID];

    // Find spatial sectors (get sector indices) of start and end points of jump from x to xup
    tmp = (unsigned int)floor(x_perp_cell / subinfo->lperp[popID]);        // reaches Nperp[popID] if x_perp_cell==cell_perp_sizes[popID] precisely, rather rare in practice
    unsigned int x_sec_perp = (tmp < (Nperp - 1)) ? (tmp) : (Nperp - 1);    // min(floor(x_perp/lperp), Nperp-1) to avoid out-of-bounds index
    tmp = (unsigned int)floor(xup_perp_cell / subinfo->lperp[popID]);
    unsigned int xup_sec_perp = (tmp < (Nperp - 1)) ? (tmp) : (Nperp - 1);

    /* The spatial sectors were designed so that a jump can only intersect objects in cells
    adjacent to either the starting or the ending point of the tentative jump.
    */
    if (x_sec_perp != xup_sec_perp) {
      // Since both points are a distance Ljump from one another, neither point can be past the midline of the sector. 
      // Consequently, no other sectors are involved along this direction.
      sec_perp[0] = x_sec_perp;
      sec_perp[1] = xup_sec_perp;
      Nsec_perp = 2;
    }
    else {
      //The jump can potentially affect objects in sectors left and right of
      // the current sector. This could be optimized by comparing the
      //positions of x and xup with respect to the midline of the sectors.

      //// Non-optimized version
      //if (Nperp > 3) {
      //  signed_modulo = ((int)x_sec_perp - 1) % (int)Nperp;            // casting to int ensure proper handling of negative values in the case x_sec_perp-1 = -1 < 0
      //  signed_modulo += (signed_modulo < 0) * Nperp;              // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division

      //  sec_perp[0] = (unsigned int)signed_modulo;
      //  sec_perp[1] = x_sec_perp;
      //  sec_perp[2] = (x_sec_perp + 1) % Nperp;
      //  Nsec_perp = 3;
      //}
      //else {
      //  for (cnt = 0; cnt < Nperp; cnt++) {
      //    sec_perp[cnt] = cnt;                      // Avoid repetitions such as [1, 1, 1] or [1, 1, 2] leading to sectors being checked multiple times needlessly
      //  }
      //  Nsec_perp = Nperp;
      //}

      // Optimization by looking at positions with respect to midline of sector to check 2 rather than 3 sectors
      if (Nperp == 2) {
        Nsec_perp = 2;
        sec_perp[0] = 0;
        sec_perp[1] = 1;
      }
      else {
        if (x_perp_cell > ((x_sec_perp + 0.5)*subinfo->lperp[popID]) && xup_perp_cell > ((xup_sec_perp + 0.5)*subinfo->lperp[popID])) {   // positions in ]lperp/2, lperp]
          Nsec_perp = 2;
          sec_perp[0] = x_sec_perp;
          sec_perp[1] = (x_sec_perp + 1) % Nperp;                  // all positive quantities
        }
        else if (x_perp_cell < (subinfo->lperp[popID] * (x_sec_perp + 0.5)) && xup_perp_cell < (subinfo->lperp[popID] * (xup_sec_perp + 0.5))) {  // positions in [0, lperp/2[
          Nsec_perp = 2;

          signed_modulo = ((int)x_sec_perp - 1) % (int)Nperp;            // casting to int ensure proper handling of negative values in the case x_sec_perp-1 = -1 < 0
          signed_modulo += (signed_modulo < 0) * Nperp;              // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division

          sec_perp[0] = (unsigned int)signed_modulo;
          sec_perp[1] = x_sec_perp;
        }
        else {
          Nsec_perp = 3;
          signed_modulo = ((int)x_sec_perp - 1) % (int)Nperp;            // casting to int ensure proper handling of negative values in the case x_sec_perp-1 = -1 < 0
          signed_modulo += (signed_modulo < 0) * Nperp;              // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division

          sec_perp[0] = (unsigned int)signed_modulo;
          sec_perp[1] = x_sec_perp;
          sec_perp[2] = (x_sec_perp + 1) % Nperp;
        }
      }
      // End of optimization


    }
  }
  else { // Nperp == 1, check one and only sector in perpendicular direction
    Nsec_perp = 1;
    sec_perp[0] = 0;
  }

  /*
  Piling direction
  */

  // Project in [0, pile_period], where pile_period >= cell_pile_size !
  double x_pile_cell = fmod(x_pile - subinfo->off_pile[popID], subinfo->pile_period);
  x_pile_cell += (x_pile_cell < 0) * subinfo->pile_period;
  double xup_pile_cell = fmod(xup_pile - subinfo->off_pile[popID], subinfo->pile_period);
  xup_pile_cell += (xup_pile_cell < 0) * subinfo->pile_period;

  // this can go into the if statement below
  tmp = (unsigned int) floor(x_pile_cell / subinfo->lpile[popID]);        // can easily go over Npile[popID] if x_pile_cell > cell_pile_sizes[popID], since pile_period >= cell_pile_sizes[popID]
  unsigned int x_sec_pile = (tmp < (Npile - 1)) ? (tmp) : (Npile - 1);      // min(floor(x_pile / lpile), Npile - 1) to avoid out-of-bounds index
  tmp = (unsigned int) floor(xup_pile_cell / subinfo->lpile[popID]);
  unsigned int xup_sec_pile = (tmp < (Npile - 1)) ? (tmp) : (Npile - 1);

  // Same reasoning as above for the piling direction, except that since pile_period can be much larger than cell_pile_size, both positions may be far
  // enough from the populations's elementary cell to prevent any intersection with objects from the current population
  
  // if more than half a sector side length away, don't even bother checking (recall that {x|xup}_pile_cell is in [0, subinfo->pile_period] so no need to check negative values
  if ((x_pile_cell <= (subinfo->cell_pile_sizes[popID] + 0.5*subinfo->lpile[popID]))
    || (xup_pile_cell <= (subinfo->cell_pile_sizes[popID] + 0.5*subinfo->lpile[popID]))
    || (x_pile_cell >= subinfo->pile_period - 0.5*subinfo->lpile[popID])
    || (xup_pile_cell >= (subinfo->pile_period - 0.5*subinfo->lpile[popID]))) {
    // x_sec_pile and xup_sec_pile should be computed here
    if (Npile > 1) {

      // Determine which sectors to be checked along piling direction
      if (x_sec_pile != xup_sec_pile) {
        sec_pile[0] = x_sec_pile;
        sec_pile[1] = xup_sec_pile;
        Nsec_pile = 2;
      }
      else {

        // Non-optimized code: just copy all sector indices if 3 sectors or fewer are used
        //if (Npile > 3) {
        //  signed_modulo = ((int)x_sec_pile - 1) % (int)Npile;            // conversion to int to allow for value -1, both args should be converted !
        //  signed_modulo += (signed_modulo < 0) * Npile;              // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division
        //  sec_pile[0] = (unsigned int)signed_modulo; //sec_pile.push_back((unsigned int)signed_modulo);
        //  sec_pile[1] = x_sec_pile; //sec_pile.push_back(x_sec_pile);
        //  sec_pile[2] = (x_sec_pile + 1) % Npile; //sec_pile.push_back((x_sec_pile + 1) % Npile);
        //  Nsec_pile = 3;
        //}
        //else {
        //  //std::vector<unsigned int> sec_pile(Npile, subinfo->totobjects+1);    // initial value chosen to trigger errors later in the code
        //  for (cnt = 0; cnt < Npile; cnt++) {
        //    sec_pile[cnt] = cnt;                      // Avoid repetitions such as [1, 1, 1] or [1, 1, 2] leading to sectors being checked multiple times needlessly
        //  }
        //  Nsec_pile = Npile;
        //}

        // Optimization by looking at positions with respect to midline of sector 
        // so that we check 2 rather than 3 sectors
        if (Npile == 2) {
          Nsec_pile = 2;
          sec_pile[0] = 0;
          sec_pile[1] = 1;
        }
        else {
          if (x_pile_cell > ((x_sec_pile + 0.5)*subinfo->lpile[popID]) && xup_pile_cell > ((xup_sec_pile+0.5)*subinfo->lpile[popID])) {  // positions in ]lpile/2, lpile]
            Nsec_pile = 2;
            sec_pile[0] = x_sec_pile;
            sec_pile[1] = (x_sec_pile + 1) % Npile;                  // all positive quantities
          }
          else if (x_pile_cell < (subinfo->lpile[popID] * (x_sec_pile + 0.5)) && xup_pile_cell < (subinfo->lpile[popID] * (xup_sec_pile + 0.5))) {  // positions in [0, lpile/2[
            Nsec_pile = 2;

            signed_modulo = ((int)x_sec_pile - 1) % (int)Npile;            // casting to int ensure proper handling of negative values in the case x_sec_pile-1 = -1 < 0
            signed_modulo += (signed_modulo < 0) * Npile;              // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division

            sec_pile[0] = (unsigned int)signed_modulo;
            sec_pile[1] = x_sec_pile;
          }
          else {
            Nsec_pile = 3;
            signed_modulo = ((int)x_sec_pile - 1) % (int)Npile;            // casting to int ensure proper handling of negative values in the case x_sec_pile-1 = -1 < 0
            signed_modulo += (signed_modulo < 0) * Npile;              // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division

            sec_pile[0] = (unsigned int)signed_modulo;
            sec_pile[1] = x_sec_pile;
            sec_pile[2] = (x_sec_pile + 1) % Npile;
          }
        }
        // End of optimization

      }
    }
    else { // Npile == 1: check the one and only sector in the piling direction
      Nsec_pile = 1;
      sec_pile[0] = 0;
    }

  }


  // Same reasoning for the cdir direction as for the perpendicular direction
  // Only done for sphere populations to avoid divisions by zero since lcdir==0 for cylinder populations
  unsigned int x_sec_cdir = 0, xup_sec_cdir = 0;    // (could be within if statement but easier outside for debugging below)
  
  if (Ncdir > 1 && subinfo->poptypes[popID] == kSphere) { // the conditions are redundant actually
    // See comment about fmod from <math.h>  above for negative first arguments
    double x_cdir_cell = fmod(x_cdir - subinfo->off_cdir[popID], subinfo->cell_cdir_sizes[popID]);
    x_cdir_cell += (x_cdir_cell < 0) * subinfo->cell_cdir_sizes[popID];
    double xup_cdir_cell = fmod(xup_cdir - subinfo->off_cdir[popID], subinfo->cell_cdir_sizes[popID]);
    xup_cdir_cell += (xup_cdir < 0) * subinfo->cell_cdir_sizes[popID];

    // Get sector index
    tmp = (unsigned int) floor(x_cdir_cell / subinfo->lcdir[popID]);        // would likely return NaN for a cylinder population. Reaches Ncdir[popID] if x_cdir_cell==cell_cdir_sizes[popID] precisely, rather rare in practice
    x_sec_cdir = (tmp < (Ncdir - 1)) ? (tmp) : (Ncdir - 1);              // min(floor(x_cdir/lcdir), Ncdir-1) to avoid out-of-bounds index
    tmp = (unsigned int) floor(xup_cdir_cell / subinfo->lcdir[popID]);
    xup_sec_cdir = (tmp < (Ncdir - 1)) ? (tmp) : (Ncdir - 1);
    
    // Determine which sectors to check in cdir direction
    if (x_sec_cdir != xup_sec_cdir) {
      sec_cdir[0] = (x_sec_cdir);
      sec_cdir[1] = (xup_sec_cdir);
      Nsec_cdir = 2;
    }
    else {
      // Non-optimized code: just copy all sector indices if 3 sectors or fewer are used
      //if (Ncdir > 3) {
      //  signed_modulo = ((int)x_sec_cdir - 1) % (int)Ncdir;              // conversion to int of both arguments to allow for negative value -1
      //  signed_modulo += (signed_modulo < 0) * Ncdir;                // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division
      //  
      //  sec_cdir[0] = (unsigned int)signed_modulo;      
      //  sec_cdir[1] = x_sec_cdir;
      //  sec_cdir[2] = (x_sec_cdir + 1) % Ncdir;
      //  Nsec_cdir = 3;
      //}
      //else {
      //  for (cnt = 0; cnt < Ncdir; cnt++) {
      //    sec_cdir[cnt] = cnt;
      //  }
      //  Nsec_cdir = Ncdir;
      //}

      // Optimized version with respect to midline of sector
      if (Ncdir == 2) {
        Nsec_cdir = 2;
        sec_cdir[0] = 0;
        sec_cdir[1] = 1;
      }
      else {
        if (x_cdir_cell > ((x_sec_cdir + 0.5)*subinfo->lcdir[popID]) && xup_cdir_cell > ((xup_sec_cdir + 0.5)*subinfo->lcdir[popID])) {   // positions in ]lcdir/2, lcdir]
          Nsec_cdir = 2;
          sec_cdir[0] = x_sec_cdir;
          sec_cdir[1] = (x_sec_cdir + 1) % Ncdir;                  // all positive quantities
        }
        else if (x_cdir_cell < (subinfo->lcdir[popID] * (x_sec_cdir + 0.5)) && xup_cdir_cell < (subinfo->lcdir[popID] * (xup_sec_cdir + 0.5))) {  // positions in [0, lcdir/2[
          Nsec_cdir = 2;

          signed_modulo = ((int)x_sec_cdir - 1) % (int)Ncdir;            // casting to int ensure proper handling of negative values in the case x_sec_cdir-1 = -1 < 0
          signed_modulo += (signed_modulo < 0) * Ncdir;              // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division

          sec_cdir[0] = (unsigned int)signed_modulo;
          sec_cdir[1] = x_sec_cdir;
        }
        else {
          Nsec_cdir = 3;
          signed_modulo = ((int)x_sec_cdir - 1) % (int)Ncdir;            // casting to int ensure proper handling of negative values in the case x_sec_cdir-1 = -1 < 0
          signed_modulo += (signed_modulo < 0) * Ncdir;              // most C/C++ compilers do not ensure positivity of the remainder of the Euclidean division

          sec_cdir[0] = (unsigned int)signed_modulo;
          sec_cdir[1] = x_sec_cdir;
          sec_cdir[2] = (x_sec_cdir + 1) % Ncdir;
        }
      }  // end of optimized 2--vs-3-sector code
    }

  }
  else { // only 1 sector in cdir direction (e.g., because non-sphere population)
    sec_cdir[0] = 0;              // [0] by default
    Nsec_cdir = 1;
  }

  // Add indices of all the object comprised in the sectors adjacent to the jump

  //printf("DEBUG MP3D_get_sector_objects: x =(%g, %g, %g) --> xup=(%g, %g, %g) in pop %d.\n",
  //  x_perp, x_pile, x_cdir, xup_perp, xup_pile, xup_cdir, popID);
  //printf("DEBUG MP3D_get_sector_objects: x detected in sector [%d][%d][%d], xup in sector [%d][%d][%d] of pop %d, sec_perp has size %d, sec_pile size %d, sec_cdir size %d\n",
  //    x_sec_perp, x_sec_pile, x_sec_cdir, xup_sec_perp, xup_sec_pile, xup_sec_cdir, popID, (unsigned int)sec_perp.size(), (unsigned int)sec_pile.size(), (unsigned int)sec_cdir.size());  // DEBUG

  std::vector<unsigned int> * sector_list;
  unsigned int i, j, k, l;
  Ncand = 0;                      // modified here for use by the caller of the function
  for (i = 0; i < Nsec_perp; i++) {
    for (j = 0; j < Nsec_pile; j++) {
      for (k = 0; k < Nsec_cdir; k++) {
        sector_list = & (subinfo->obj_spatial_distribution[popID][sec_perp[i]][sec_pile[j]][sec_cdir[k]]); // copied here for clarity
        
        //printf("DEBUG MP3D_get_sector_objects: sector [%d][%d][%d] containing %d objects:\n", sec_perp[i], sec_pile[j], sec_cdir[k], (unsigned int)sector_list.size());
        
        for (l = 0; l < (*sector_list).size(); l++) {
          cand_obj[Ncand] = (* sector_list)[l];
          Ncand++;
        }
      }
    }
  }
}



/*
Uniform particle dropping in extracellular space of env using the
orientational periodicity information obtained by a call to
MP3D_compute_generator.

Thread-safe.

Requires : 
 - call to init_genrand(seed, rng_state) after allocating rng_state
   *in the current thread* in order to initialize the re-entrant MT RNG state
 - call to MP3D_compute_generator on env

Modifies:
 - x: the sampled location in the extracellular space
 - rng_state to keep track of random stream

 Date: October 2016
*/
void MP3D_drop_particle_initialized(double* x, unsigned long** rng_state,
    MP3D_env* env) {
  unsigned int dim = 3;
  double pile_pos = (genrand_res53_r(rng_state) - 0.5)*(env->pile_period);  // drop in [-Lpile/2,Lpile/2]*piling_dir
  x[0] = pile_pos*(env->piling_dir[0]);
  x[1] = pile_pos*(env->piling_dir[1]);
  x[2] = pile_pos*(env->piling_dir[2]);
  unsigned int i;
  double perp_pos, cdir_pos;
  // add along each non-colinear direction, uniformly in the elementary polygon underpinning the environment
  for (i = 0; i<env->num_noncolinear_cdirs; i++) {
    perp_pos = (genrand_res53_r(rng_state) - 0.5)*env->perpdists[i];
    cdir_pos = (genrand_res53_r(rng_state) - 0.5)*env->cdirdists[i];
    x[0] = x[0] + perp_pos*env->noncolinear_pdirs[i * dim] + cdir_pos*env->noncolinear_cdirs[i * dim];
    x[1] = x[1] + perp_pos*env->noncolinear_pdirs[i * dim + 1] + cdir_pos*env->noncolinear_cdirs[i * dim + 1];
    x[2] = x[2] + perp_pos*env->noncolinear_pdirs[i * dim + 2] + cdir_pos*env->noncolinear_cdirs[i * dim + 2];
  }
}



/*
Uniform particle dropping using the orientational periodicity
information obtained by a call to MP3D_compute_generator.

! For single-thread execution only.

Requires:
 - initializing the MT RNG state by using init_genrand(seed)
   or init_by_array(init_key, key_length)
 - calling MP3D_compute_generator on env

Modifies:
 - x: the sampled location in the extracellular space

Date: October 2016
*/
void MP3D_drop_particle_initialized_sgthrd(double* x, MP3D_env* env) {
  unsigned int dim = 3;
  double pile_pos = (genrand_res53() - 0.5)*(env->pile_period);  // drop in [-Lpile/2,Lpile/2]*piling_dir
  x[0] = pile_pos*(env->piling_dir[0]);
  x[1] = pile_pos*(env->piling_dir[1]);
  x[2] = pile_pos*(env->piling_dir[2]);
  unsigned int i;
  double perp_pos, cdir_pos;
  // add along each non-colinear direction, uniformly in the elementary polygon underpinning the environment
  for (i = 0; i<env->num_noncolinear_cdirs; i++) {
    perp_pos = (genrand_res53() - 0.5)*env->perpdists[i];
    cdir_pos = (genrand_res53() - 0.5)*env->cdirdists[i];
    x[0] = x[0] + perp_pos*env->noncolinear_pdirs[i * dim] + cdir_pos*env->noncolinear_cdirs[i * dim];
    x[1] = x[1] + perp_pos*env->noncolinear_pdirs[i * dim + 1] + cdir_pos*env->noncolinear_cdirs[i * dim + 1];
    x[2] = x[2] + perp_pos*env->noncolinear_pdirs[i * dim + 2] + cdir_pos*env->noncolinear_cdirs[i * dim + 2];
  }
}





/*
Detects intersection (if any) with object objID and its periodic repetitions
in population popID between x and xup which is closest to x.

Computes the reflected point x_refl by elastic reflection.

Modifies:
 - crossing
 - x_refl
 - lam_inters

according to nearest crossing that 
would occur between x and xup with object objID in population popID unless
it detects that it is already too far to beat the previously detected
nearest crossing.
*/
void MP3D_reflection(double* x, double* xup,
    double x_perp, double x_pile, double x_cdir,
    double xup_perp, double xup_pile, double xup_cdir,
    unsigned char* crossing, double* x_refl, double* lam_inters,
    unsigned int popID, unsigned int objID, MP3D_env* subinfo) {
    double Lpile = subinfo->pile_period;
    double Lperp = subinfo->cell_perp_sizes[popID];
    double Lcdir = subinfo->cell_cdir_sizes[popID];

    double off_pile_obj = subinfo->off_pile[popID]+ subinfo->obj_pile_pos[objID];
    double off_perp_obj = subinfo->off_perp[popID]+ subinfo->obj_perp_pos[objID];
    double off_cdir_obj = subinfo->off_cdir[popID]+ subinfo->obj_cdir_pos[objID];
    
    double r = subinfo->radii[objID];
    
    // Determine rectangular or parellelepiped "danger" zone containing cylinders potentially
    // intersected during the jump x -> xup in projection plane
    double centers_perp_min = off_perp_obj + ceil((fmin(x_perp,xup_perp)-off_perp_obj-r)/Lperp)*Lperp;
    double centers_perp_max = off_perp_obj + floor((fmax(x_perp,xup_perp)-off_perp_obj+r)/Lperp)*Lperp; 
    unsigned int num_cyl_perp = (unsigned int)((int)round((centers_perp_max-centers_perp_min)/Lperp) + 1); // always positive because first term >= -1
    
    double centers_pile_min = off_pile_obj + ceil((fmin(x_pile,xup_pile)-off_pile_obj-r)/Lpile)*Lpile;
    double centers_pile_max = off_pile_obj + floor((fmax(x_pile,xup_pile)-off_pile_obj+r)/Lpile)*Lpile;
    unsigned int num_cyl_pile = (unsigned int)((int)round((centers_pile_max-centers_pile_min)/Lpile) + 1); // always positive because first term >= -1
    
    double centers_cdir_min = x_cdir; // default for cylinder populations
    double centers_cdir_max = x_cdir;// default for cylinder populations
    unsigned int num_cyl_cdir = 1 ;          // default for cylinder populations
    if (subinfo->poptypes[popID] == kSphere){
        centers_cdir_min = off_cdir_obj + ceil( (fmin(x_cdir,xup_cdir)-off_cdir_obj-r)/Lcdir)*Lcdir ;
        centers_cdir_max = off_cdir_obj + floor( (fmax(x_cdir,xup_cdir)-off_cdir_obj+r)/Lcdir)*Lcdir ;
        num_cyl_cdir = (unsigned int)((int)round((centers_cdir_max-centers_cdir_min)/Lcdir) + 1) ; // always positive because first term >= -1
    }
    
    // Initialize beginning of search. Start with cylinders
    // near x to save computation time : "near-first search" within danger zone
    double cent_perp_start, cent_pile_start, cent_cdir_start ;
    if (x_perp < xup_perp){
        cent_perp_start = centers_perp_min ;
    }
    else{
        cent_perp_start = centers_perp_max ;
    }
    
    if (x_pile < xup_pile){
        cent_pile_start = centers_pile_min ;
    }
    else{
        cent_pile_start = centers_pile_max ;
    }
    
    if (x_cdir < xup_cdir){
        cent_cdir_start = centers_cdir_min ;
    }
    else{
        cent_cdir_start = centers_cdir_max ;
    }
    double xdiff[3] = {x[0]-xup[0], x[1]-xup[1], x[2]-xup[2]} ;
    double L = norm(xdiff,3) ;
    double L_proj = L ;     // effective length between x and xup in plane perpendicular to cylinder axis for cyl pops
    if (subinfo->poptypes[popID] == kCylinder){
        L_proj = sqrt(pow(x_perp-xup_perp,2)+pow(x_pile-xup_pile,2)) ;    // distance in projection plane
    }

    //printf("DEBUG : MP3D_reflection : pop %d, obj %d cent_perp_start=%6.5e, cent_pile_start=%6.5e, cent_cdir_start=%6.5e\n",popID, objID, cent_perp_start, cent_pile_start, cent_cdir_start) ;
    unsigned int indperp, indpile, indcdir ;
    for (indperp=0; indperp< num_cyl_perp ; indperp++){
        double cent_perp = cent_perp_start + indperp*Lperp*signum(xup_perp-x_perp) ; // some quantities could be pre-computed here...
        for (indpile = 0 ; indpile< num_cyl_pile ; indpile++){
            double cent_pile = cent_pile_start + indpile*Lpile*signum(xup_pile-x_pile) ;
            for (indcdir = 0; indcdir< num_cyl_cdir; indcdir++){
                double cent_cdir = cent_cdir_start + indcdir*Lcdir*signum(xup_cdir-x_cdir) ;
                // Formula below works for both cyl. and sph. pops since
                // cent_cdir = cent_cdir_start = x_cdir (indcdir only takes the
                // value 0 for cyl. populations)
                // It is just a useless calculation for cylinder populations
                double lx = sqrt( pow(x_perp-cent_perp,2)+pow(x_pile-cent_pile,2)+pow(x_cdir-cent_cdir,2)) ;
                // With lx, we can again filter out impossible intersections :
                double orientation = (cent_perp-x_perp)*(xup_perp-x_perp) + (cent_pile-x_pile)*(xup_pile-x_pile) + (cent_cdir-x_cdir)*(xup_cdir-x_cdir) ; // <(cent-x),(xup-x)>
                double Del = pow(-orientation,2)-pow(L_proj,2)*(pow(lx,2)-pow(r,2)) ;    // Delta of 2nd degree equation for lambda
                // Equation is L^2 lam^2 + 2*<x-c,xup-x> lam + (lx^2-r1^2)
                if (Del > 0 && orientation > 0){ 
                /*
                     avoid contact cylinder of previous reflexion : should
                     rather be abs(lx-r)> r/5000 ??? Actally, ensuring
                     (x-c)^T(xup-x) <0 is sufficient to avoid "going back".*/
                    double lam = (orientation - sqrt(Del))/pow(L_proj,2) ;
                    /* lambda must be in [0,1], the smaller lambda the closer
                     the intersection. (Orientation>0) already ensures lam>=0.
                     lam>0 is too restrictive for limit cases x|cyl|---> xup
                     when x is right on a boundary and xup is accross that
                     boundary. We double-check anyway but put it behind since
                     nearly always true for boolean short-circuiting.
                    */
                    if ( (lam < *lam_inters ) && (lam >= 0) ){
                        *crossing = 1 ;
                        double xc[3] = {(1-lam)*x[0]+lam*xup[0], (1-lam)*x[1]+lam*xup[1],(1-lam)*x[2]+lam*xup[2] }; // contact point of intersection
                        double normal[3] = {0,0,0} ; // dummy initialization
                        if (subinfo->poptypes[popID]== kCylinder){
                            double xc_proj[2] = {(1-lam)*x_perp+lam*xup_perp, (1-lam)*x_pile+lam*xup_pile } ;    // intersection point in plane perpendicular to cdir
                            double normal_proj[2] = {xc_proj[0]-cent_perp , xc_proj[1]-cent_pile} ;
                            // Normalize projected normal vector :
                            double normal_proj_norm = norm(normal_proj,2) ;
                            normal_proj[0] = normal_proj[0]/normal_proj_norm ;
                            normal_proj[1] = normal_proj[1]/normal_proj_norm ;

                            normal[0] = normal_proj[0]*subinfo->perpdir[3*popID+0] + normal_proj[1]*subinfo->piling_dir[0] ;
                            normal[1] = normal_proj[0]*subinfo->perpdir[3*popID+1] + normal_proj[1]*subinfo->piling_dir[1] ;
                            normal[2] = normal_proj[0]*subinfo->perpdir[3*popID+2] + normal_proj[1]*subinfo->piling_dir[2] ;
                            //printf("\tIntersection with cylinder object %d pop %d at (%4.3e,%4.3e,%4.3e).\n",objID,popID,cent_perp,cent_pile,cent_cdir);
                        }
                        else if (subinfo->poptypes[popID] == kSphere){
                            double xc_proj[3] = {(1-lam)*x_perp+lam*xup_perp, (1-lam)*x_pile+lam*xup_pile, (1-lam)*x_cdir+lam*xup_cdir} ;
                            double normal_proj[3] = {xc_proj[0]-cent_perp , xc_proj[1]-cent_pile, xc_proj[2]-cent_cdir} ;
                            // Normalize projected normal vector :
                            double normal_proj_norm = norm(normal_proj,3) ;
                            normal_proj[0] = normal_proj[0]/normal_proj_norm ;
                            normal_proj[1] = normal_proj[1]/normal_proj_norm ;
                            normal_proj[2] = normal_proj[2]/normal_proj_norm ;

                            normal[0] = normal_proj[0]*subinfo->perpdir[3*popID+0] + normal_proj[1]*subinfo->piling_dir[0] + normal_proj[2]*subinfo->cdir[3*popID+0];
                            normal[1] = normal_proj[0]*subinfo->perpdir[3*popID+1] + normal_proj[1]*subinfo->piling_dir[1] + normal_proj[2]*subinfo->cdir[3*popID+1];
                            normal[2] = normal_proj[0]*subinfo->perpdir[3*popID+2] + normal_proj[1]*subinfo->piling_dir[2] + normal_proj[2]*subinfo->cdir[3*popID+2];
                            //printf("\tIntersection with sphere object %d pop %d at (%4.3e,%4.3e,%4.3e).\n",objID,popID,cent_perp,cent_pile,cent_cdir);
                        }
                        double dotprod = normal[0]*(xup[0]-xc[0]) + normal[1]*(xup[1]-xc[1]) + normal[2]*(xup[2]-xc[2]) ;
                        x_refl[0] = xup[0] - 2*dotprod*normal[0] ;
                        x_refl[1] = xup[1] - 2*dotprod*normal[1] ;
                        x_refl[2] = xup[2] - 2*dotprod*normal[2] ;
                        *lam_inters = lam ;
                    }
                }
            }
        }
    }
}



/*
Performs a jump for x to xup in the extracellular space of subinfo with
perfectly-elastic reflections off object membranes such that the total
length of the trajectory is equal to ||xup-x||_2.

Returns 0 if no crossing was detected, 1 otherwise.

Inputs:
 - cand_obj must be able to hold up to subinfo->totobjects indices.

Requires:
 - call to MP3D_initialize_spatial_sectors on subinfo once the jump length
   used in the simulation is known.

Modifies:
 - x, which becomes the end point.
 - xup, which eventually becomes garbage
 - cand_obj via calls to MP3D_get_sector_objects

Browses through all populations and calls MP3D_get_sector_objects to get a list
of objects potentially intersected during jump; for each candidate object 
computes the intersection (if any) closest to starting point. Reflection is 
performed on the object intersected closest to the starting point out of all 
populations, the reflection point becomes the new starting point and the result
of the reflection becomes the new end point  and the procedure is repeated until
no intersection is detected.
*/
unsigned char MP3D_fixed_jump(double* x, double* xup,
    std::vector<unsigned int> & cand_obj, MP3D_env* subinfo) {
  unsigned char done_flag = 0;
  unsigned char crossing_at_all = 0;
  double xdiff[3] = {x[0]-xup[0], x[1]-xup[1], x[2]-xup[2]}; 
  double L = norm(xdiff, 3);  // residual jump length 
  unsigned int objID;

  // population-wise coordinates of starting point x and end point of jump xup
  double x_perp, x_pile, x_cdir, xup_perp, xup_pile, xup_cdir;
  
  unsigned int Ncand;  //  number of candidate objects to check

  unsigned int Nsectors;  // number of spatial sectors in one population

  while (!done_flag) {
    unsigned char crossing = 0;
    // lam=1 corresponds to full step without intersection
    double lam_inters = 1.0;
    double x_refl[3];  // reflected position
    unsigned int i, j;

    x_pile = x[0] * subinfo->piling_dir[0]
             + x[1] * subinfo->piling_dir[1]
             + x[2] * subinfo->piling_dir[2];  // identical for all populations
    xup_pile = xup[0] * subinfo->piling_dir[0]
               + xup[1] * subinfo->piling_dir[1]
               + xup[2] * subinfo->piling_dir[2];  // identical for all populations
    
    for (i = 0; i < subinfo->numpop; i++) {
      // Compute the population coordinates of x and xup once and for
      // all and pass it to MP3D_reflection
      x_perp = x[0] * subinfo->perpdir[3*i]
               + x[1] * subinfo->perpdir[3*i+1]
               + x[2] * subinfo->perpdir[3*i+2];
      x_cdir = x[0] * subinfo->cdir[3*i]
               + x[1] * subinfo->cdir[3*i+1]
               + x[2] * subinfo->cdir[3*i+2];
      xup_perp = xup[0] * subinfo->perpdir[3*i]
                 + xup[1] * subinfo->perpdir[3*i+1]
                 + xup[2] * subinfo->perpdir[3*i+2];
      xup_cdir = xup[0] * subinfo->cdir[3*i]
                 + xup[1] * subinfo->cdir[3*i+1]
                 + xup[2] * subinfo->cdir[3*i+2];
      
      // Get objects in neighboring sectors (more efficient even would
      // be if Nperp>1 && Npile>1 && Ncdir>1...) 
      // TODO: compute Nsectors once in MP3D_initialize_spatial_sectors
      Nsectors = subinfo->Nperp[i] * subinfo->Npile[i] * subinfo->Ncdir[i];
      if (Nsectors > 1) {
        MP3D_get_sector_objects(subinfo, Ncand, i,
                                x_perp, x_pile, x_cdir,
                                xup_perp, xup_pile, xup_cdir,
                                cand_obj);
      }
      else {
        // avoid call to get_sector_objects if no spatial sub-gridding was used
        Ncand = subinfo->popsizes[i];
      }
      
      //printf("MP3D_fixed_jump DEBUG: Jump x=(%5.4e,%5.4e,%5.4e) -> xup=(%5.4e,%5.4e,%5.4e): %d candidate objects in population %d\n",
      //  x[0], x[1], x[2], xup[0], xup[1], xup[2], Ncand, i);

      for (j = 0; j < Ncand; j++) {
        if (Nsectors > 1) {
          // checking only close objects from spatial subgridding
          objID = cand_obj[j];
        }
        else {
          // when checking all objects of the population
          objID = subinfo->obj_start_ind[i] + j;
        }

        // updates crossing and lam_inters relative to current object
        MP3D_reflection(x, xup,
                        x_perp, x_pile, x_cdir,
                        xup_perp, xup_pile, xup_cdir,
                        &crossing, x_refl, &lam_inters, i, objID, subinfo);

        //printf("MP3D_fixed_jump DEBUG: Checked pop.%d, obj.%d (%d in total list), crossing=%d, lam=%4.3e \n", i, j, objID, crossing, lam_inters); // DEBUG
        //printf("MP3D_fixed_jump DEBUG: x_refl = (%5.4e, %5.4e, %5.4e)\n",x_refl[0],x_refl[1],x_refl[2]) ;

      }
    }
    if (crossing) {
      crossing_at_all = 1;
    }
    else{
      done_flag = 1;
    }

    L = (1 - lam_inters)*L;  // residual steplength
    // if no crossing: lam_inters=1, inters_point = xup and loop ends
    double inters_point[3] = {(1-lam_inters)*x[0]+lam_inters*xup[0],
                                (1-lam_inters)*x[1]+lam_inters*xup[1],
                                (1-lam_inters)*x[2]+lam_inters*xup[2]};
    
    //printf("DEBUG : MP3D_fixed_jump : x=(%5.4e,%5.4e,%5.4e) -> xup=(%5.4e,%5.4e,%5.4e)\n\t intersection point=(%5.4e,%5.4e,%5.4e) -> reflection point (%5.4e,%5.4e,%5.4e), residual L=%5.4e \n",
    //  x[0], x[1], x[2], xup[0], xup[1], xup[2], inters_point[0], inters_point[1], inters_point[2], x_refl[0], x_refl[1], x_refl[2], L);

    // Perform x <- inters_point   and xup <- x_refl
    // explicitely change content of memory cases, not just swap local
    // pointer values
    x[0] = inters_point[0]; x[1] = inters_point[1]; x[2] = inters_point[2]; 
    xup[0] = x_refl[0]; xup[1] = x_refl[1]; xup[2] = x_refl[2];
    // x_refl contains garbage at last iteration ! Doesn't matter since loop
    // breaks when no crossing detected    
  }
  return crossing_at_all ;
}




/*
Return true ( or 1) if position x is in the intracellular space of environment env.
Return population ID indpopin of the population containing position x in its intracellular space, if applicable.
DEPRECATED as of September 25, 2018
*/
//unsigned char MP3D_is_intra_old(double(*x)[3], MP3D_env* env, unsigned int* indpopin) { // add indpopin as output pointer
//                                            // !! : double (*x)[3] is a pointer to a pointer holding the first of three contiguous double variables in memory
//  unsigned char dim = 3;     // number of spatial dimensions
//  double Lpile = env->pile_period;
//  unsigned char isintra = 0; // return value : 0 or 1
//  *indpopin = env->numpop + 1;    // index of population the spin at x is in
//  double xpile = (*x)[0] * (env->piling_dir[0]) + (*x)[1] * (env->piling_dir[1]) + (*x)[2] * (env->piling_dir[2]); //(*(*x+0)) works too
//                                                           //printf("DBG MP3D_is_intra: xpile = %4.3e\n",xpile) ;
//  unsigned int obj_cnt = 0;
//  unsigned int indpop = 0;
//
//  while ((indpop < env->numpop) && !isintra) {
//    unsigned int ind_dir = dim*indpop;
//    double xperp = (*x)[0] * env->perpdir[ind_dir + 0] + (*x)[1] * env->perpdir[ind_dir + 1] + (*x)[2] * env->perpdir[ind_dir + 2]; // x'*subinfo.perpdir(:,indpop) ;
//    double xcdir = (*x)[0] * env->cdir[ind_dir + 0] + (*x)[1] * env->cdir[ind_dir + 1] + (*x)[2] * env->cdir[ind_dir + 2]; //x'*subinfo.cdir(:,indpop) ;     // useful for sphere populations
//    double off_pile_pop = env->off_pile[indpop];
//    double off_perp_pop = env->off_perp[indpop];
//    unsigned int ind_num_obj;
//    for (ind_num_obj = 0; ind_num_obj< env->popsizes[indpop]; ind_num_obj++) {
//      obj_cnt = env->obj_start_ind[indpop] + ind_num_obj;
//      double off_obj_pile = off_pile_pop + env->obj_pile_pos[obj_cnt]; // in "first" unit cell
//      double off_obj_perp = off_perp_pop + env->obj_perp_pos[obj_cnt]; // in "first" unit cell
//      double cent_pile_min = off_obj_pile + floor((xpile - off_obj_pile) / Lpile)*Lpile;
//      double cent_pile_max = off_obj_pile + ceil((xpile - off_obj_pile) / Lpile)*Lpile; // precompute (x-off)/L once and reuse it maybe
//      double cent_perp_min = off_obj_perp + floor((xperp - off_obj_perp) / (env->cell_perp_sizes[indpop]))*env->cell_perp_sizes[indpop];
//      double cent_perp_max = off_obj_perp + ceil((xperp - off_obj_perp) / (env->cell_perp_sizes[indpop]))*env->cell_perp_sizes[indpop];
//      // Identify which of the 4 surrounding cylinders/sphers is the closest in <perpdir,piling_dir> plane
//      double cent_pile_near, cent_perp_near;
//      if ((xpile - cent_pile_min) < (cent_pile_max - xpile)) {
//        cent_pile_near = cent_pile_min;
//      }
//      else {
//        cent_pile_near = cent_pile_max;
//      }
//      if ((xperp - cent_perp_min) < (cent_perp_max - xperp)) {
//        cent_perp_near = cent_perp_min;
//      }
//      else {
//        cent_perp_near = cent_perp_max;
//      }
//      double sq_dist = 0;
//      //printf("DBG MP3D_is_intra : obj %d: cent_pile_near=%5.4e, cent_perp_near=%5.4e\n",obj_cnt,cent_pile_near, cent_perp_near) ;
//      // For sphere pops, determine if sphere "above" or "under" is closer
//      if (env->poptypes[indpop] == kSphere) {
//        double off_obj_cdir = env->off_cdir[indpop] + env->obj_cdir_pos[obj_cnt];
//        double cent_cdir_min = off_obj_cdir + floor((xcdir - off_obj_cdir) / (env->cell_cdir_sizes[indpop]))*env->cell_cdir_sizes[indpop];
//        double cent_cdir_max = off_obj_cdir + ceil((xcdir - off_obj_cdir) / (env->cell_cdir_sizes[indpop]))*env->cell_cdir_sizes[indpop];
//        double cent_cdir_near;
//        if ((xcdir - cent_cdir_min) < (cent_cdir_max - xcdir)) {
//          cent_cdir_near = cent_cdir_min;
//        }
//        else {
//          cent_cdir_near = cent_cdir_max;
//        }
//        sq_dist = pow(xcdir - cent_cdir_near, 2);
//        //printf("DBG MP3D_is_intra : obj %d: cent_cdir_near=%5.4e\n",obj_cnt,cent_cdir_near) ;
//      }
//
//      sq_dist = sq_dist + pow(xpile - cent_pile_near, 2) + pow(xperp - cent_perp_near, 2);
//      double dist = sqrt(sq_dist);
//      // Comparing the square distances seemed less stable numerically
//      if (dist < env->radii[obj_cnt]) {
//        isintra = 1;
//        *indpopin = indpop;
//        //printf("DEBUG MP3D_is_intra : spin is inside population %d !\n", *indpopin) ;
//        break;
//      }
//    } // for ind_num_obj
//    indpop++;
//  } // while indpop
//  return isintra;
//}