#ifndef CL_CL_ARG_CONSTRAINTS_H
#define CL_CL_ARG_CONSTRAINTS_H

#include <string>
#include "CL/tclap/CmdLine.h"  //  entry point to whole header library

// CL string that user must provide to use internal dummy scheme
//  if they don't have one
const std::string kDummySchemeStr =
    "_dummy_";
// Location of our dummy scheme file relative to this source file
const std::string kRelPathDummySchemeFile =
    "../data/schemefiles/dummy.scheme1";

// Replace schemefile by internal dummy scheme file if requested by 
// command-line user
void check_for_dummy_schemefile(std::string & schemefile);


/*********************************************************************
Classes for constraints on parsed arguments (see TCLAP header library)
**********************************************************************/
class PosDoubleCstrnt : public TCLAP::Constraint<double> {
  public:
    bool check(const double& value) const;
    std::string shortID() const;
    std::string description() const;
};

class Pos0DoubleCstrnt : public TCLAP::Constraint<double> {
  public:
    bool check(const double& value) const;
    std::string shortID() const;
    std::string description() const;
};

class PosIntCstrnt : public TCLAP::Constraint<int> {
  public:
    bool check(const int& value) const;
    std::string shortID() const;
    std::string description() const;
};

class Pos0IntCstrnt : public TCLAP::Constraint<int> {
public:
  bool check(const int& value) const;
  std::string shortID() const;
  std::string description() const;
};

class Btw01Cstrnt : public TCLAP::Constraint<double> {
  public:
    bool check(const double& value) const;
    std::string shortID() const;
    std::string description() const;
};

class NonemptyStringCstrnt : public TCLAP::Constraint<std::string> {
  public:
    bool check(const std::string& value) const;
    std::string shortID() const;
    std::string description() const;
};

class SchemefileStrCstrnt : public TCLAP::Constraint<std::string> {
  public:
    bool check(const std::string& value) const;
    std::string shortID() const;
    std::string description() const;
};

/*
Container for Monte Carlo cmd-line argument common to all simulation
frameworks so they can be edited once and for all (in the related .cc
source file). Upon construction, the arguments are added to the cmd-line
parser object cmd passed to the constructor.
*/
class CL_MC_ArgsInitializer {
  private:
    // Constraints on MC command-line args
    Pos0IntCstrnt pos0_int_cons;
    SchemefileStrCstrnt sch_file_cons;
    Pos0DoubleCstrnt pos0_double_cons;
    NonemptyStringCstrnt non0_str_cons;

  public:
    // General command-line options
    TCLAP::ValueArg<int> argNspins;
    TCLAP::ValueArg<int> argNsteps;
    TCLAP::ValueArg<double> argDIFF;
    TCLAP::ValueArg<std::string> argSchemefile;
    TCLAP::ValueArg<std::string> argOutputfile;
    TCLAP::ValueArg<int> argSeed;
    TCLAP::SwitchArg argMultiThread;
    TCLAP::SwitchArg argWriteSpinPhases;

    // Constructor
    CL_MC_ArgsInitializer(TCLAP::CmdLine& cmd);
};

#endif // !CL_CL_ARG_CONSTRAINTS_H
