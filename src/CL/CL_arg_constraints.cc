// for line number in memory reports by _CrtDumpMemoryLeaks();
// using Visual Studio (must be placed in every .c|cxx|cc file)
#define _CRTDBG_MAP_ALLOC
// stop Visual Studio from issuing annoying warnings about fopen being unsafe
#define _CRT_SECURE_NO_WARNINGS

#include "CL/CL_arg_constraints.h"

#include "MP3D/tools.h"  // Fileparts


/*
Use of internal dummy schemefile
*/
void check_for_dummy_schemefile(std::string & schemefile) {
  if (schemefile == kDummySchemeStr) {
    FileParts current_file_fparts = fileparts(std::string(__FILE__));
    std::string dummy_sch_file;
    dummy_sch_file.append(current_file_fparts.path);
    dummy_sch_file.append(kRelPathDummySchemeFile);
    schemefile.assign(dummy_sch_file);
    printf("\nUsing internal dummy schemefile %s.\n", &(schemefile[0]));
  }
}

// Positive double constraint
bool PosDoubleCstrnt::check(const double& value) const {
  return value > 0;
}
std::string PosDoubleCstrnt::shortID() const {
  return "pos. floating-point";
}
std::string PosDoubleCstrnt::description() const {
  return "should be a stricly positive floating-point number";
}


// Non-negative double constraint
bool Pos0DoubleCstrnt::check(const double& value) const {
  return value >= 0;
}
std::string Pos0DoubleCstrnt::shortID() const {
  return "non-neg. floating-point";
}
std::string Pos0DoubleCstrnt::description() const {
  return "should be a non-negative floating-point number";
}

// Positive integer constraint
bool PosIntCstrnt::check(const int& value) const {
  return value > 0;
}
std::string PosIntCstrnt::shortID() const {
  return "pos. integer";
}
std::string PosIntCstrnt::description() const {
  return "should be a stricly positive integer";
}


// Non-negative integer constraint
bool Pos0IntCstrnt::check(const int& value) const {
  return value >= 0;
}
std::string Pos0IntCstrnt::shortID() const {
  return "non-neg. integer";
}
std::string Pos0IntCstrnt::description() const {
  return "should be a non-negative integer";
}


// Floating-point number in ]0, 1[
bool Btw01Cstrnt::check(const double& value) const {
  return value > 0 && value < 1;
}
std::string Btw01Cstrnt::shortID() const {
  return "floating-point in ]0;1[";
}
std::string Btw01Cstrnt::description() const {
  return "should be a floating-point number in ]0;1[";
}

// Non empty string constraint
bool NonemptyStringCstrnt::check(const std::string& value) const {
  return value.size() > 0;
}
std::string NonemptyStringCstrnt::shortID() const {
  return "non-empty string";
}
std::string NonemptyStringCstrnt::description() const {
  return "non-empty unix/like/path";
}

// Schemefile specification constraint
bool SchemefileStrCstrnt::check(const std::string& value) const {
  return value.size() > 0;
}
std::string SchemefileStrCstrnt::shortID() const {
  return "non empty path or \"" + kDummySchemeStr + "\"";
}
std::string SchemefileStrCstrnt::description() const {
  return "non-empty unix/like/path or \"" + kDummySchemeStr + "\"";
}

/*
class CL_MC_ArgsContainer
*/
CL_MC_ArgsInitializer::CL_MC_ArgsInitializer(TCLAP::CmdLine& cmd)
    : argNspins("", "Nspins",
                "number of random walkers",
                true, 10, &pos0_int_cons),
      argNsteps("", "Nsteps",
                "number of timesteps used with the longest sequence",
                true, 10, &pos0_int_cons),
      argDIFF("", "diffusivity",
              "extracellular diffusivity [m^2/s]",
              true, 2.0e-9, &pos0_double_cons),
      argSchemefile("", "schemefile",
                    "unix/like/path to a valid schemefile or \""
                    + kDummySchemeStr + "\" to use a default dummy protocol",
                    true, "", &sch_file_cons),
      argOutputfile("", "outputfile",
                    "unix/like/path/and/basename of the output files"
                    " that will be written to",
                    true, "", &non0_str_cons),
      argSeed("", "seed",
              "non-negative integer random seed [default:141414]",
              false, 141414, &pos0_int_cons),
      argMultiThread("", "multithread",
                     "parallelization of spins' random walks on as many"
                     " threads as are available [default:off]",
                     false),
      argWriteSpinPhases("", "writeSpinPhases",
                         "produce binary files containing Nseqs x Nspins"
                         " final spin phases in each spatial direction "
                         "[default:false]",
                         false) {
  cmd.add(argWriteSpinPhases);
  cmd.add(argMultiThread);
  cmd.add(argSeed);
  cmd.add(argOutputfile);
  cmd.add(argSchemefile);
  cmd.add(argDIFF);
  cmd.add(argNsteps);
  cmd.add(argNspins);
}

