#define _CRTDBG_MAP_ALLOC   // for line number in memory reports by _CrtDumpMemoryLeaks(); must be placed in every .c[xx] file
#define _USE_MATH_DEFINES

#include "pore_packing/pack_pores.h"

#include <algorithm>
#include <math.h>
#include <random>


// max number of attempts at placing a cylinder before giving up

Pore::Pore(double P[3], double radius): radius_(radius){
  this->P_[0] = P[0];
  this->P_[1] = P[1];
  this->P_[2] = P[2];
}


/************ SUBSTRATE Base Class ************/
Substrate::Substrate(std::string distribution, double minrad, double maxrad,
                     double mean, double std, double i, double gap, int n_pores,
                     int n_max_attempts)
  : dist_law(distribution),
    minRad(minrad),
    maxRad(maxrad),
    dist_mean(mean),
    dist_std(std),
    icvf(i),
    sep_gap(gap),
    num_pores(n_pores),
    max_attempts(n_max_attempts) {
  this->pores.clear();
  this->unique_pores.clear();
  // The vertices defining the bounding box containing the pores is
  // always 3-D in our implementation. The last entry will be ignored
  // for 2D substrates containing cylinders for instance.
  this->min_limits.resize(3, 0.0);  // {0.0,0.0,0.0}
  this->max_limits.resize(3, -1.0);
}
Substrate::Substrate(std::string distribution, double minrad, double maxrad,
                     double mean, double std, double i,
                     double gap, int n_pores)
    : Substrate(distribution, minrad, maxrad, mean, std,
                i, gap, n_pores, Substrate::kDefaultMaxAttempts) {}

void Substrate::printPores(std::ostream &out) {
    out.precision(17);

    double scaling = 1;  // from units of dist_mean and dist_std (typically microns) to desired units (meters in Camino for instance). E.g., 1e-6 to go from microns to meter.
    long int i;
    for(i = 0; i < (long int)this->pores.size(); i++){
        out << std::fixed << this->pores[i].P_[0] * scaling << " " << std::fixed << this->pores[i].P_[1] * scaling << " " << std::fixed << this->pores[i].P_[2] * scaling << " "
        << std::fixed << this->pores[i].radius_ * scaling << std::endl;
    }
}

void Substrate::printUniquePores(std::ostream &out) {
    out.precision(17);

    double scaling = 1;  // from units of dist_mean and dist_std (typically microns) to desired units (meters in Camino for instance). E.g., 1e-6 to go from microns to meter.
    long int i;
    for(i = 0; i < (long int)this->unique_pores.size(); i++){
        out << std::fixed << this->unique_pores[i].P_[0] * scaling << " " << std::fixed << this->unique_pores[i].P_[1] * scaling << " "
        << std::fixed << this->unique_pores[i].P_[2] * scaling << " " << std::fixed << this->unique_pores[i].radius_ * scaling << std::endl;
    }
}

void Substrate::printSubstrateProperties(std::ostream & out) {
    out.precision(17);
    
    // Print substrate size, number of pores placed, number of pores including mirrors, intra-axonal vol. frac. reached in the end
    out << std::fixed << this->max_limits[0] - this->min_limits[0] << std::endl;
    out << std::fixed << this->unique_pores.size() << std::endl;
    out << std::fixed << this->pores.size() << std::endl;
    out << std::fixed << this->computeICVF() << std::endl;
}

std::vector<double> Substrate::draw_radii(unsigned long seedrad) {
  // Initialize random number generator
  std::mt19937 gen_rad(seedrad);

  // Draw the sphere's radii according to selected distribution
  // if std dev is zero, all radii are set to the mean
  std::vector<double> radii(this->num_pores, this->dist_mean);

  if (this->dist_std != 0.0) {
    // if std dev is non-zero, perform an actual random draw
    int i;
    if (this->dist_law == "gamma") {
      // Compute shape and scale parameters of a gamma distribution
      double dist_var = (this->dist_std)*(this->dist_std);
      // shape parameter (number of exponentially-distributed random variables
      // to add, can be extended to non-integer values):
      double dist_a = this->dist_mean* this->dist_mean/dist_var;
      // scale parameter (mean of each exponentially-distributed random variable)
      double dist_b = dist_var/this->dist_mean;
        
      std::gamma_distribution<double> distribution(dist_a, dist_b);
      for (i=0; i < this->num_pores; ++i) {
        do{
          // in units of dist_mean and dist_std, typically microns
          radii[i] = distribution(gen_rad);
        }
        while (radii[i] < this->minRad || radii[i] > this->maxRad);
      }
    }
    else {
      if (this->dist_law == "normal") {
        std::normal_distribution<double> distribution(this->dist_mean,
                                                      this->dist_std);
        for (i=0; i < this->num_pores; ++i) {
          do {
            // in units of dist_mean and dist_std, typically microns
            radii[i] = distribution(gen_rad);
          }
          while (radii[i] < this->minRad || radii[i] > this->maxRad);
        }
      }
    }
  }

  return radii;
}


/****************** SPHERES ***********************/
SphereSubstrate::SphereSubstrate(std::string distribution, double minrad,
  double maxrad, double mean, double std,
  double icvf, double gap, int num_pores, int n_attempts_max)
  : Substrate(distribution, minrad, maxrad, mean, std,
    icvf, gap, num_pores, n_attempts_max) {}

/*
Constructor with default value from Base class used for data member
max_attempts.
*/
SphereSubstrate::SphereSubstrate(std::string distribution, double minrad,
                                 double maxrad, double mean, double std,
                                 double icvf, double gap, int num_pores)
    : Substrate(distribution, minrad, maxrad, mean, std,
                icvf, gap, num_pores) {}

/*
Radii drawn from distribution with random seed seedrad, verifying r[i]>0.
Spatial sectors should have side length strictly greater than
2*max(radii) + gap so that only cylinders in adjacent sectors should be
checked when placing a new cylinder.
Positions sampled in [0,L[ x [0,L[ (L excluded).
*/
void SphereSubstrate::position_pores(unsigned long seedrad, unsigned long seedpos) {
  // Clear spheres lists
  this->pores.clear();
  this->unique_pores.clear();

  // Sample radii
  std::vector<double> radii = this->draw_radii(seedrad);

  double volume_intra = 0.0;
  double volume_intra_gap = 0.0;                        // intra-axonal space considering the minimum gap between cylinder edges
  long int i;
  for (i = 0; i < this->num_pores; ++i) {
    volume_intra += radii[i] * radii[i] * radii[i];
    volume_intra_gap += (radii[i] + this->sep_gap / 2.0)*(radii[i] + this->sep_gap / 2.0)*(radii[i] + this->sep_gap / 2.0);
  }
  volume_intra *= (4.0 / 3.0)*M_PI;
  volume_intra_gap *= (4.0 / 3.0)*M_PI;

  // Adjust substrate dimension to match icvf precisely after drawing the radii
  double substrate_volume = volume_intra / this->icvf;
  double substrate_side_length = std::cbrt(substrate_volume);  // Change here for non square environments
  this->min_limits[0] = 0.0;
  this->min_limits[1] = 0.0;
  this->min_limits[2] = 0.0;
  this->max_limits[0] = substrate_side_length;
  this->max_limits[1] = substrate_side_length;
  this->max_limits[2] = substrate_side_length;
  // Sort spheres by radius in descending order (using a lambda function for sorting)
  std::sort(radii.begin(), radii.end(), [](const double a, double  b) -> bool
  {
    return a> b;
  });

  // Compute number of spatial sectors or subcells so that each sphere can
  // only cover directly adjacent sectors (including the separation gap)
  unsigned int n_sectors_x = (unsigned int)floor((this->max_limits[0]
                              - this->min_limits[0])
                              / (2.0*radii[0] + this->sep_gap));
  unsigned int n_sectors_y = (unsigned int)floor((this->max_limits[1]
                              - this->min_limits[1])
                              / (2.0*radii[0] + this->sep_gap));
  unsigned int n_sectors_z = (unsigned int)floor((this->max_limits[2]
                              - this->min_limits[2])
                              / (2.0*radii[0] + this->sep_gap));
  unsigned int n_sectors = ( ( (n_sectors_x < n_sectors_y) ?
                                n_sectors_y
                                : n_sectors_x
                             ) < n_sectors_z) ?
                             n_sectors_z
                             : ((n_sectors_x < n_sectors_y) ?
                                 n_sectors_y : n_sectors_x);

  std::cout << std::setprecision(15) << "\nUsing " << n_sectors << " x "
    << n_sectors << " x " << n_sectors
    << " spatial sectors for efficient intersection checking"
       " in substrate of dimensions "
    << this->max_limits[0] - this->min_limits[0] << " by "
    << this->max_limits[1] - this->min_limits[1] << " by "
    << this->max_limits[2] - this->min_limits[2]
    << " in the units used to specify the mean and standard deviation"
       " of the distribution of radii.\n"
    << std::endl;
  if (this->sep_gap > 0) {
    std::cout << "The effective intra-axonal volume fraction considering "
                 "the inter-sphere gap is "
              << volume_intra_gap / substrate_volume
              << " and could prevent sphere packing from succeeding if"
                 " too high. Compare to target icvf "
              << this->icvf << ".\n" << std::endl;
  }

  // Start positioning (starting with largest sphere)
  std::mt19937 gen_pos(seedpos);
  std::uniform_real_distribution<double> udist(0, 1);

  double perc_display = 0.10;  // interval in icvf percentage between consecutive outputs to console (just for monitoring progress)

  std::vector<Pore> spheres_to_add;

  this->sector_sphere_idx = /* 4-dimensional array*/
    std::vector<std::vector<std::vector<std::vector<long int> > > >
    (n_sectors, std::vector<std::vector<std::vector<long int> > >
                (n_sectors, std::vector<std::vector<long int> >
                            (n_sectors, std::vector<long int>()
                            )
                )
    );

  double icvf_current = 0.0;

  for (i = 0; i < (long)radii.size(); i++) {
    int attempts = 0;

    while (++attempts <= this->max_attempts) {  // N_ATTEMPTS_MAX
      double x, y, z, t;
      // Sample center position uniformly in
      // [x_min, x_max[ x [y_min, y_max[ x [z_min, z_max[
      // avoiding upper-bound edges for periodicity. The >= conditions
      // could technically be == but we never know with rounding errors.
      do {
        t = udist(gen_pos);
        x = (t*this->max_limits[0]) + ((1 - t)*this->min_limits[0]);
        t = udist(gen_pos);
        y = (t*this->max_limits[1]) + ((1 - t)*this->min_limits[1]);
        t = udist(gen_pos);
        z = (t*this->max_limits[2]) + ((1 - t)*this->min_limits[2]);
      } while (x >= max_limits[0] || y >= max_limits[1] || z >= max_limits[2]);

      double D[3] = { x, y, z };
      Pore sph(D, radii[i]);

      // Set collision to false and mirror sph in spheres_to_add if no overlap. 
      // spheres_to_add contains 1 (no mirror), 2 (reflection across a side)
      // or 4 spheres (reflection in a corner)
      bool collision = checkForCollision(sph, spheres_to_add, n_sectors);

      // If no collision detected, add current sphere to list of unique spheres
      // add current sphere and potential mirrors to list of all spheres
      int j, k;
      if (!collision) {
        for (j = 0; j < (int) spheres_to_add.size(); j++) {
          this->pores.push_back(spheres_to_add[j]);
          // Get vector of triplets (ix, iy, iz) which are the positions of
          // the sectors containing spheres_to_add[j]
          std::vector<std::vector<int>> idx = get_sector_indexes(spheres_to_add[j], n_sectors);
          for (k = 0; k < (int) idx.size(); k++) {
            this->sector_sphere_idx[idx[k][0]][idx[k][1]][idx[k][2]].push_back((long int)this->pores.size() - 1);
          }
        }
        this->unique_pores.push_back(sph);
        break;
      }
    }

    double perc_ = float(i) / float(radii.size());

    icvf_current = computeICVF();

    if (perc_ > perc_display) {
      perc_display += 0.10;

      std::cout << "Completed so far: " + std::to_string(perc_*100.0)
        + "%,\nICVF achieved: " + std::to_string(icvf_current * 100)
        + "  (" + std::to_string(int((icvf_current / this->icvf) * 100))
        + "% of the desired icvf)\n"
        << std::endl;
    }
  }

  // Print final result to console
  std::string message;
  std::cout << "Effective ICVF reached " << icvf_current * 100
    << "% (versus target " << this->icvf * 100 << "%).\n" << std::endl;

  if (this->unique_pores.size()<radii.size()) {
    message = "Managed to place only " + std::to_string(this->unique_pores.size())
      + " spheres instead of " + std::to_string(radii.size())
      + " (" + std::to_string(this->pores.size()) + " counting mirrored copies).\n";
   std::cout << message << std::endl;
  }
  else {
    message = "Managed to place all " + std::to_string(this->unique_pores.size())
      + " spheres ("
      + std::to_string(this->pores.size()) + " counting mirrored copies).\n";
    std::cout << message << std::endl;
  }
}

/*
Check collision (including gap) between cyl and every cylinder in the list
cylinders_to_add (which is still empty at this point)
*/
bool SphereSubstrate::checkForCollision(const Pore& sph,
                                        std::vector<Pore>& spheres_to_add,
                                        unsigned int n_sectors) const {
  spheres_to_add.clear();

  // Add sph and its mirrors --if any--
  checkBoundaryConditions(sph, spheres_to_add);

  bool collision = false;

  int i, j, k;
  for (j = 0; j < (long)spheres_to_add.size(); j++) {
    // idx contains all the indices of the sectors covered (even partially) by spheres_to_add[j], the current mirror copy of sph
    std::vector<std::vector<int> > idx = get_sector_indexes(spheres_to_add[j], n_sectors);
    // For each sector, check all cylinders contained in the sector
    for (i = 0; i < (long)idx.size(); i++) {
      // for code readability while avoiding copy, extract pore list:
      const std::vector<long int>* pore_list =
          &(this->sector_sphere_idx[idx[i][0]][idx[i][1]][idx[i][2]]);
      for (k = 0; k < (long)(*pore_list).size(); ++k) {
        double distance =
          pow(this->pores[(*pore_list)[k]].P_[0]-spheres_to_add[j].P_[0], 2) +
          pow(this->pores[(*pore_list)[k]].P_[1]-spheres_to_add[j].P_[1], 2) +
          pow(this->pores[(*pore_list)[k]].P_[2]-spheres_to_add[j].P_[2], 2);
        distance = sqrt(distance);
        if (distance -
            (this->pores[this->sector_sphere_idx[idx[i][0]][idx[i][1]][idx[i][2]][k]].radius_
             + spheres_to_add[j].radius_) < this->sep_gap) {
          collision = true;
        }
        if (collision == true) { break; }
      }  // for k
      if (collision == true) { break; }
    }  // for i
    if (collision == true) { break; }
  }  // for j
  return collision;
}

/*
 Returns the positions (in index form) of all the sectors (partially)
 covered by sph. A position is a triplet of the form (ix, iy, iz).

 FIXME: simplification possible.
 In the special case where sph covers a sector of index 0 (along any axis)
 and the sector before it (index -1 or equivalently, index n_sectors-1),
 the returned vector of indexes only contains 0 and NOT {0, n_sectors-1}.
 SphereSubstrate::checkBoundaryConditions then has to take care of it.
*/
std::vector<std::vector<int>> SphereSubstrate::get_sector_indexes(
    const Pore& sph, unsigned int n_sectors) const {

  // Get dimensions of a sector, formerly Eigen::Vector3d
  double sector_side[3] = {(this->max_limits[0]-this->min_limits[0])/n_sectors,
                           (this->max_limits[1]-this->min_limits[1])/n_sectors,
                           (this->max_limits[2]-this->min_limits[2])/n_sectors };
  // FIXME: make max sector index coordinate-dependent
  int max_sector_idx = (int)n_sectors - 1;  // shorthand.

  // ! Do not use unsigned integers here as some calculations rely on being
  // able to manipulate the value -1, which is then typically mapped to
  // max_sector_idx.

  // idx will be a std::vector containing 1, 2, 3, 4, or 7
  // three-dimensional vectors, representing the sectors covered by sph.
  std::vector<std::vector<int> > idx;
  // Store position of sector containing the center of sph    
  std::vector<int> tmp(3, -1);
  tmp[0] = int(floor(sph.P_[0] / sector_side[0]));
  tmp[1] = int(floor(sph.P_[1] / sector_side[1]));
  tmp[2] = int(floor(sph.P_[2] / sector_side[2]));

  // At this point tmp[i] should be >=0 but could be == n_sectors
  // if sph.P_[i] == sector_side[i] precisely, which is not good.
  int i;

  for(i=0; i<3; ++i) {
    tmp[i] = std::max(tmp[i], 0);  // technically unnecessary
    tmp[i] = std::min(tmp[i], max_sector_idx);
  }

  // idx[0] contains the index of the sector in which the sphere sph lies.
  idx.push_back(tmp);

  // Get sectors spanned by sphere sph on 6 main sides, i.e. sectors
  // with position p such that (p-idx[0]) is a 3-dim vector containing
  // two zeros and one +1 or -1, e.g., [0, -1, 0], [1, 0, 0], ...
  for (i = 0; i<3; ++i) {
    // initialize tmp to sector containing sph
    tmp[0] = idx[0][0];
    tmp[1] = idx[0][1];
    tmp[2] = idx[0][2];
    if(int(floor((sph.P_[i]-sph.radius_-this->sep_gap)/sector_side[i]))
        < idx[0][i]){
      tmp[i] = std::max(tmp[i]-1, 0);
      idx.push_back(tmp);
    } else if(int(floor((sph.P_[i]+sph.radius_ + this->sep_gap)/sector_side[i]))
              >idx[0][i]) {
      tmp[i] = std::min(tmp[i]+1, max_sector_idx);
      idx.push_back(tmp);
    }
  }
    
  int j;
  if(idx.size()>=3) {
    // Add sectors spanned by sphere sph in each 2D slice, along diagonal
    // directions x-y, x-z, and y-z, i.e. sectors with position p such that
    // (p-idx[0]) contains one zero and two +1 or -1, modulo max_sector_idx.
    for (i = 0; i<3; ++i) {
      for (j = i+1; j<3; ++j) {
        // Initialize tmp to sector containing sph
        tmp[0] = idx[0][0];
        tmp[1] = idx[0][1];
        tmp[2] = idx[0][2];
        if(int(floor((sph.P_[i]-sph.radius_ - this->sep_gap)
            /sector_side[i]))<idx[0][i] &&
            int(floor((sph.P_[j]-sph.radius_ - this->sep_gap)
            /sector_side[j]))<idx[0][j]) {
          // FIXME: if idx[0][i]=0 we should have max_sector_idx[i] instead of
          // 0 ideally. The program is still correct because the function
          // SphereSubstrate::checkBoundaryConditions checks it but it adds
          // unnecessary complication
          tmp[i] = std::max(tmp[i]-1, 0);
          tmp[j] = std::max(tmp[j]-1, 0);
          idx.push_back(tmp);
        } else if(int(floor((sph.P_[i]-sph.radius_ - this->sep_gap)
              /sector_side[i]))<idx[0][i] &&
              int(floor((sph.P_[j]+sph.radius_ + this->sep_gap)
              /sector_side[j]))>idx[0][j]) {
          tmp[i] = std::max(tmp[i]-1,0);
          tmp[j] = std::min(tmp[j]+1,max_sector_idx);
          idx.push_back(tmp);
        } else if(int(floor((sph.P_[i]+sph.radius_ + this->sep_gap)
              /sector_side[i]))>idx[0][i] &&
              int(floor((sph.P_[j]-sph.radius_ - this->sep_gap)
              /sector_side[j]))<idx[0][j]) {
          tmp[i] = std::min(tmp[i]+1,max_sector_idx);
          tmp[j] = std::max(tmp[j]-1,0);
          idx.push_back(tmp);
        } else if(int(floor((sph.P_[i]+sph.radius_ + this->sep_gap)
              /sector_side[i]))>idx[0][i] &&
              int(floor((sph.P_[j]+sph.radius_ + this->sep_gap)
              /sector_side[j]))>idx[0][j]) {
          tmp[i] = std::min(tmp[i]+1,max_sector_idx);
          tmp[j] = std::min(tmp[j]+1,max_sector_idx);
          idx.push_back(tmp);
        }
      }
    }

    // initialize tmp to sector containing sph
    tmp[0] = idx[0][0];
    tmp[1] = idx[0][1];
    tmp[2] = idx[0][2];
    // Add sectors spanned by sphere in 3D, i.e. with position p such that
    // (p-idx[0]) is a 3-D vector with no zeros, containing only +1 and -1
    // (modulo max_sector_idx, see above)
    if(idx.size()>=7){      
      if(int(floor((sph.P_[0]-sph.radius_ - this->sep_gap)/sector_side[0]))<idx[0][0] &&
          int(floor((sph.P_[1]-sph.radius_ - this->sep_gap)/sector_side[1]))<idx[0][1] &&
          int(floor((sph.P_[2]-sph.radius_ - this->sep_gap)/sector_side[2]))<idx[0][2]) {
        tmp[0] = std::max(tmp[0]-1,0);
        tmp[1] = std::max(tmp[1]-1,0);
        tmp[2] = std::max(tmp[2]-1,0);
        idx.push_back(tmp);
      } else if(int(floor((sph.P_[0]+sph.radius_ + this->sep_gap)/sector_side[0]))>idx[0][0] &&
          int(floor((sph.P_[1]-sph.radius_ - this->sep_gap)/sector_side[1]))<idx[0][1] &&
          int(floor((sph.P_[2]-sph.radius_ - this->sep_gap)/sector_side[2]))<idx[0][2]) {
        tmp[0] = std::min(tmp[0]+1,max_sector_idx);
        tmp[1] = std::max(tmp[1]-1,0);
        tmp[2] = std::max(tmp[2]-1,0);
        idx.push_back(tmp);
      } else if(int(floor((sph.P_[0]-sph.radius_ - this->sep_gap)/sector_side[0]))<idx[0][0] &&
          int(floor((sph.P_[1]+sph.radius_ + this->sep_gap)/sector_side[1]))>idx[0][1] &&
          int(floor((sph.P_[2]-sph.radius_ - this->sep_gap)/sector_side[2]))<idx[0][2]) {
        tmp[0] = std::max(tmp[0]-1, 0);
        tmp[1] = std::min(tmp[1]+1, max_sector_idx);
        tmp[2] = std::max(tmp[2]-1, 0);
        idx.push_back(tmp);
      } else if(int(floor((sph.P_[0]-sph.radius_ - this->sep_gap)/sector_side[0]))<idx[0][0] &&
          int(floor((sph.P_[1]-sph.radius_ - this->sep_gap)/sector_side[1]))<idx[0][1] &&
          int(floor((sph.P_[2]+sph.radius_ + this->sep_gap)/sector_side[2]))>idx[0][2]) {
        tmp[0] = std::max(tmp[0]-1,0);
        tmp[1] = std::max(tmp[1]-1,0);
        tmp[2] = std::min(tmp[2]+1,max_sector_idx);
        idx.push_back(tmp);
      } else if(int(floor((sph.P_[0]-sph.radius_ - this->sep_gap)/sector_side[0]))<idx[0][0] &&
          int(floor((sph.P_[1]+sph.radius_ + this->sep_gap)/sector_side[1]))>idx[0][1] &&
          int(floor((sph.P_[2]+sph.radius_ + this->sep_gap)/sector_side[2]))>idx[0][2]) {
        tmp[0] = std::max(tmp[0]-1,0);
        tmp[1] = std::min(tmp[1]+1,max_sector_idx);
        tmp[2] = std::min(tmp[2]+1,max_sector_idx);
        idx.push_back(tmp);
      } else if(int(floor((sph.P_[0]+sph.radius_ + this->sep_gap)/sector_side[0]))>idx[0][0] &&
          int(floor((sph.P_[1]-sph.radius_ - this->sep_gap)/sector_side[1]))<idx[0][1] &&
          int(floor((sph.P_[2]+sph.radius_ + this->sep_gap)/sector_side[2]))>idx[0][2]) {
        tmp[0] = std::min(tmp[0]+1,max_sector_idx);
        tmp[1] = std::max(tmp[1]-1,0);
        tmp[2] = std::min(tmp[2]+1,max_sector_idx);
        idx.push_back(tmp);
      } else if(int(floor((sph.P_[0]+sph.radius_ + this->sep_gap)/sector_side[0]))>idx[0][0] &&
          int(floor((sph.P_[1]+sph.radius_ + this->sep_gap)/sector_side[1]))>idx[0][1] &&
          int(floor((sph.P_[2]-sph.radius_ - this->sep_gap)/sector_side[2]))<idx[0][2]) {
        tmp[0] = std::min(tmp[0]+1,max_sector_idx);
        tmp[1] = std::min(tmp[1]+1,max_sector_idx);
        tmp[2] = std::max(tmp[2]-1,0);
        idx.push_back(tmp);
      } else if(int(floor((sph.P_[0]+sph.radius_ + this->sep_gap)/sector_side[0]))>idx[0][0] &&
          int(floor((sph.P_[1]+sph.radius_ + this->sep_gap)/sector_side[1]))>idx[0][1] &&
          int(floor((sph.P_[2]+sph.radius_ + this->sep_gap)/sector_side[2]))>idx[0][2]) {
        tmp[0] = std::min(tmp[0]+1,max_sector_idx);
        tmp[1] = std::min(tmp[1]+1,max_sector_idx);
        tmp[2] = std::min(tmp[2]+1,max_sector_idx);
        idx.push_back(tmp);
      }
    }  // if idx.size() >= 7
  } //if (idx.size() >= 3)

  return idx;
}

/*
 Adds the sphere itself and its reflected versions to the list spheres_to_add, if substrate boundaries are crossed.
*/
void SphereSubstrate::checkBoundaryConditions(
    const Pore& sph, std::vector<Pore>& spheres_to_add) const {
  spheres_to_add.push_back(sph);
  double rad = sph.radius_;

  // Reflect a sphere if part of it crosses the substrate boundary along the x or y direction
  int i;
  for(i = 0 ; i < 3; i++) {   
    if(sph.P_[i]+rad >= max_limits[i]){
      Pore tmp = sph;
      tmp.P_[i]+= this->min_limits[i] - this->max_limits[i];
      spheres_to_add.push_back(tmp);
    }
    if(sph.P_[i] - rad < this->min_limits[i]){
      Pore tmp = sph;
      tmp.P_[i]+= this->max_limits[i] - this->min_limits[i];
      spheres_to_add.push_back(tmp);
    }
  }

  // If a sphere trespassed the substrate boundaries along the x AND the
  // y direction then it also needs to be reflected in the opposite corner
  // of the substrate:
  int j;
  if(spheres_to_add.size() >= 3) {
    for(i = 0 ; i < 3; i++) {
      for(j = i+1 ; j < 3; j++) {
        if(sph.P_[i]-rad<min_limits[i] && sph.P_[j]-rad<min_limits[j]) {
          Pore tmp = sph;
          tmp.P_[i]+= max_limits[i] - min_limits[i];
          tmp.P_[j]+= max_limits[j] - min_limits[j];
          spheres_to_add.push_back(tmp);
        }
        if(sph.P_[i]-rad<min_limits[i] && sph.P_[j]+rad>=max_limits[j]) {
          Pore tmp = sph;
          tmp.P_[i]+= max_limits[i] - min_limits[i];
          tmp.P_[j]+= min_limits[j] - max_limits[j];
          spheres_to_add.push_back(tmp);
        }
        if(sph.P_[i]+rad>=max_limits[i] && sph.P_[j]-rad<min_limits[j]) {
          Pore tmp = sph;
          tmp.P_[i]+= min_limits[i] - max_limits[i];
          tmp.P_[j]+= max_limits[j] - min_limits[j];
          spheres_to_add.push_back(tmp);
        }
        if(sph.P_[i]+rad>=max_limits[i] && sph.P_[j]+rad>=max_limits[j]) {
          Pore tmp = sph;
          tmp.P_[i]+= min_limits[i] - max_limits[i];
          tmp.P_[j]+= min_limits[j] - max_limits[j];
          spheres_to_add.push_back(tmp);
        }
      }
    }
    if(spheres_to_add.size() == 7) {
      // Adding mirror along 3D
      if(sph.P_[0]-rad<min_limits[0] && sph.P_[1]-rad<min_limits[1] && sph.P_[2]-rad<min_limits[2]) {
        Pore tmp = sph;
        tmp.P_[0]+= max_limits[0] - min_limits[0];
        tmp.P_[1]+= max_limits[1] - min_limits[1];
        tmp.P_[2]+= max_limits[2] - min_limits[2];
        spheres_to_add.push_back(tmp);
      } else if(sph.P_[0]-rad<min_limits[0] && sph.P_[1]-rad<min_limits[1] && sph.P_[2]+rad>=max_limits[2]) {
        Pore tmp = sph;
        tmp.P_[0]+= max_limits[0] - min_limits[0];
        tmp.P_[1]+= max_limits[1] - min_limits[1];
        tmp.P_[2]+= min_limits[2] - max_limits[2];
        spheres_to_add.push_back(tmp);
      } else if(sph.P_[0]-rad<min_limits[0] && sph.P_[1]+rad>=max_limits[1] && sph.P_[2]-rad<min_limits[2]) {
        Pore tmp = sph;
        tmp.P_[0]+= max_limits[0] - min_limits[0];
        tmp.P_[1]+= min_limits[1] - max_limits[1];
        tmp.P_[2]+= max_limits[2] - min_limits[2];
        spheres_to_add.push_back(tmp);
      } else if(sph.P_[0]+rad>=max_limits[0] && sph.P_[1]-rad<min_limits[1] && sph.P_[2]-rad<min_limits[2]) {
        Pore tmp = sph;
        tmp.P_[0]+= min_limits[0] - max_limits[0];
        tmp.P_[1]+= max_limits[1] - min_limits[1];
        tmp.P_[2]+= max_limits[2] - min_limits[2];
        spheres_to_add.push_back(tmp);
      } else if(sph.P_[0]-rad<min_limits[0] && sph.P_[1]+rad>=max_limits[1] && sph.P_[2]+rad>=max_limits[2]) {
        Pore tmp = sph;
        tmp.P_[0]+= max_limits[0] - min_limits[0];
        tmp.P_[1]+= min_limits[1] - max_limits[1];
        tmp.P_[2]+= min_limits[2] - max_limits[2];
        spheres_to_add.push_back(tmp);
      } else if(sph.P_[0]+rad>=max_limits[0] && sph.P_[1]-rad<min_limits[1] && sph.P_[2]+rad>=max_limits[2]) {
        Pore tmp = sph;
        tmp.P_[0]+= min_limits[0] - max_limits[0];
        tmp.P_[1]+= max_limits[1] - min_limits[1];
        tmp.P_[2]+= min_limits[2] - max_limits[2];
        spheres_to_add.push_back(tmp);
      } else if(sph.P_[0]+rad>=max_limits[0] && sph.P_[1]+rad>=max_limits[1] && sph.P_[2]-rad<min_limits[2]) {
        Pore tmp = sph;
        tmp.P_[0]+= min_limits[0] - max_limits[0];
        tmp.P_[1]+= min_limits[1] - max_limits[1];
        tmp.P_[2]+= max_limits[2] - min_limits[2];
        spheres_to_add.push_back(tmp);
      } else if(sph.P_[0]+rad>=max_limits[0] && sph.P_[1]+rad>=max_limits[1] && sph.P_[2]+rad>=max_limits[2]) {
        Pore tmp = sph;
        tmp.P_[0]+= min_limits[0] - max_limits[0];
        tmp.P_[1]+= min_limits[1] - max_limits[1];
        tmp.P_[2]+= min_limits[2] - max_limits[2];
        spheres_to_add.push_back(tmp);
      }
    }
  }
}


double SphereSubstrate::computeICVF() const {
  if (this->unique_pores.size() == 0) {
    return 0;
  }

  double VolumeV = (this->max_limits[0] - this->min_limits[0])*(this->max_limits[1] - this->min_limits[1])*(this->max_limits[2] - this->min_limits[2]);

  double VolumeC = 0;
  long int i;
  for (i = 0; i < (long int)this->unique_pores.size(); i++) {
    double rad = this->unique_pores[i].radius_;
    VolumeC += (4.0 / 3.0)*M_PI*rad*rad*rad;
  }
  return VolumeC / VolumeV;
}


/*********************** CYLINDERS *************************/

CylinderSubstrate::CylinderSubstrate(std::string distribution, double minrad,
                                     double maxrad, double mean, double std,
                                     double icvf, double gap, int num_cyl,
                                     int n_max_attempts)
    : Substrate(distribution, minrad, maxrad, mean, std, icvf, gap, num_cyl,
                n_max_attempts) {}

/* Constructor using default value from base class for data member 
max_attempts */
CylinderSubstrate::CylinderSubstrate(std::string distribution, double minrad,
                                     double maxrad, double mean, double std,
                                     double icvf, double gap, int num_cyl)
    : Substrate(distribution, minrad, maxrad, mean, std, icvf, gap, num_cyl) {}

/*
 Radii drawn from distribution with random seed seedrad, verifying r[i]>0.
 Spatial sectors should have side length strictly greater than
 2*max(radii) + gap so that only cylinders in adjacent sectors should be
 checked when placing a new cylinder.
 Positions sampled in [0,L[ x [0,L[ (L excluded).
 */
void CylinderSubstrate::position_pores(unsigned long seedrad, unsigned long seedpos) {
  // Clear cylinder lists
  this->pores.clear();
  this->unique_pores.clear();
    
  // Draw the cylinder's radii according to selected distribution
  std::vector<double> radii = draw_radii(seedrad);
    
  double area_intra = 0.0;
  // intra-axonal space considering the minimum gap between cylinder edges
  double area_intra_gap = 0.0;
  long int i;
  for (i=0; i<(long int)this->num_pores; ++i) {
    area_intra += radii[i]*radii[i];
    area_intra_gap += (radii[i] + this->sep_gap/2.0)
                      *(radii[i] + this->sep_gap/2.0);
  }
  area_intra *= M_PI;
  area_intra_gap *= M_PI;
    
  // Adjust substrate dimension to match icvf precisely after drawing the radii
  double substrate_area = area_intra / this->icvf;
  // Change here for non square environments:
  double substrate_side_length = sqrt(substrate_area);
  this->min_limits[0] = 0.0;
  this->min_limits[1] = 0.0;
  this->min_limits[2] = 0.0;
  this->max_limits[0] = substrate_side_length;
  this->max_limits[1] = substrate_side_length;
  this->max_limits[2] = substrate_side_length;
    
  // Sort cylinders by radius in descending order
  // (using a lambda function for sorting)
  std::sort(radii.begin(),radii.end(),[](const double a, double  b) -> bool
                                      {return a> b;}
            );

  // Compute number of spatial sectors or subcells so that each cylinder
  // can only cover directly adjacent sectors (including the separation gap)
  unsigned int n_sectors_x = (unsigned int)floor((this->max_limits[0]
                              - this->min_limits[0])
                             / (2.0*radii[0] + this->sep_gap));
  unsigned int n_sectors_y = (unsigned int)floor((this->max_limits[1]
                              - this->min_limits[1])
                             / (2.0*radii[0] + this->sep_gap));
  // min(n_sectors_x, n_sectors_y)
  // CHANGE HERE FOR RECTANGULAR, NON SQUARE SUBSTRATES:
  unsigned int n_sectors = (n_sectors_x < n_sectors_y) ?
                            n_sectors_x : n_sectors_y;
    
  std::cout << std::setprecision(15) << "\nUsing " << n_sectors
            << " x " << n_sectors
            << " spatial sectors for efficient intersection"
               " checking in substrate of dimensions "
            << this->max_limits[0] - this->min_limits[0] << " by "
            << this->max_limits[1] - this->min_limits[1]
            << " in the units used to specify the mean and standard "
               "deviation of the distribution of radii.\n"
            << std::endl;
  if (this->sep_gap > 0) {
    std::cout << "The effective intra-axonal volume fraction considering"
                 " the inter-cylinder gap is "
              << area_intra_gap / substrate_area
              << " and could prevent cylinder packing from succeeding if"
                 " too high. Compare to target icvf "
              << this->icvf << ".\n"
              << std::endl;
  }
    
  // Start positioning (starting with largest cylinder)
  std::mt19937 gen_pos(seedpos);
  std::uniform_real_distribution<double> udist(0,1);
    
  // interval in icvf percentage between consecutive outputs to console
  // (just for monitoring progress)
  double perc_display = 0.10;
    
  std::vector<Pore> cylinders_to_add;
    
  this->sector_cylinder_idx = /*3-dimensional array*/
    std::vector<std::vector<std::vector<long int> > >
    (n_sectors, std::vector<std::vector<long int> >
                (n_sectors, std::vector<long int>()
                )
     );

  double icvf_current = 0.0;
    
  for(unsigned i = 0; i < radii.size(); i++) {
    int attempts = 0;
        
    while(++attempts <= this->max_attempts) {  // N_ATTEMPTS_MAX
      // Sample center position uniformly in [x_min, x_max]x[y_min, y_max]
      double x, y, t;
      double z = 0.0;
            
      do {
        t = udist(gen_pos);
        x = (t*max_limits[0]) + ((1 - t)*min_limits[0]);
        t = udist(gen_pos);
        y = (t*max_limits[1]) + ((1 - t)*min_limits[1]);
      }
      // centers can be on left and bottom edges but not top and
      // right edges for periodicity:
      while (x >= max_limits[0] || y >= max_limits[1]);
            
      double D[3] = { x, y, z };
      Pore cyl(D, radii[i]);
            
      // Set collision to false and mirrors cyl in cylinders_to_add if no overlap. cylinders_to_add contains 1 (no mirror), 2 (reflection across a side) or 4 cylinders (reflection in a corner)
      bool collision = checkForCollision(cyl, cylinders_to_add, n_sectors);
            
      // If no collision detected, add current cylinder to list of unique cylinders; add current cylinder and potential mirrors to list of all cylinders
      if(!collision) {
        for (unsigned j = 0; j < cylinders_to_add.size(); j++) {
          this->pores.push_back(cylinders_to_add[j]);
          // vector of pairs of indices (ix, iy) representing the spatial
          // sectors containing cylinders_to_add[j]:
          std::vector<std::vector<int> > idx = get_sector_indexes(
              cylinders_to_add[j], n_sectors);
          for (unsigned k = 0; k < idx.size(); k++) {
            this->sector_cylinder_idx[idx[k][0]][idx[k][1]].push_back(
                (long int) this->pores.size()-1);
          }
        }
        this->unique_pores.push_back(cyl);
        break;
      }
    }
   
    double perc_ = float(i)/float(radii.size());

    icvf_current = computeICVF();

    if(perc_ > perc_display) {
        perc_display+=0.10;
        std::cout << "Completed so far: "+ std::to_string(perc_*100.0)
        + "%,\nICVF achieved: " + std::to_string(icvf_current*100)
        + "  ("+ std::to_string( int((icvf_current/icvf)*100))
        + "% of the desired icvf)\n" << std::endl;
    }
  }
    
  // Print final result to console
  std::cout << "Effective ICVF reached "
              + std::to_string(icvf_current * 100) + "% (versus target "
              + std::to_string(this->icvf * 100) + "%).\n"
            << std::endl;
    
  if(this->unique_pores.size()<radii.size()){
    std::cout << "Managed to place only "
                + std::to_string(this->unique_pores.size())
                + " cylinders instead of "
                + std::to_string(radii.size())
                + " (" + std::to_string(this->pores.size())
                + " counting mirrored copies).\n"
              << std::endl;
  }
  else {
    std::cout << "Managed to place all "
      + std::to_string(this->unique_pores.size())
      + " cylinders ("
      + std::to_string(this->pores.size())
      + " counting mirrored copies).\n"
      << std::endl;
  }
}

/*
 Check collision (including gap) between cyl and every cylinder in the list cylinders_to_add (which is still empty at this point)
 */
bool CylinderSubstrate::checkForCollision(const Pore& cyl,
                                          std::vector<Pore>& cylinders_to_add,
                                          unsigned int n_sectors) const {
  cylinders_to_add.clear();
  // Add cyl and its mirror cylinders --if any--
  this->checkBoundaryConditions(cyl, cylinders_to_add);
  bool collision = false;
  int j, i, k;
  for (j = 0; j < (int)cylinders_to_add.size(); j++) {
    // idx contains all the indices of the sectors covered (even partially)
    // by cylinders_to_add[j], the current mirror copy of cyl
    std::vector<std::vector<int> > idx = get_sector_indexes(
      cylinders_to_add[j], n_sectors);
    for (i = 0; i < (int)idx.size(); i++) {
      // For each sector, check all cylinders contained in the sector
      // Extract pore list for code readability
      const std::vector<long int>* pore_list =
          &(this->sector_cylinder_idx[idx[i][0]][idx[i][1]]);
      for (k = 0; k < (int) (*pore_list).size(); k++) {   
        double distance = pow(this->pores[(*pore_list)[k]].P_[0]
                              - cylinders_to_add[j].P_[0], 2) +
                          pow(this->pores[(*pore_list)[k]].P_[1]
                              - cylinders_to_add[j].P_[1], 2) +
                          pow(this->pores[(*pore_list)[k]].P_[2]
                              - cylinders_to_add[j].P_[2], 2);
        distance = sqrt(distance);
        if (distance -
            (this->pores[this->sector_cylinder_idx[idx[i][0]][idx[i][1]][k]].radius_
             + cylinders_to_add[j].radius_
             ) < this->sep_gap){
          collision = true;
        }
        if (collision == true) { break; }
      }  // for k
      if (collision == true) { break; }
    }  // for i
    if (collision == true) { break; }
  }  // for j 
  return collision;
}

/*
For Cylinder cyl, returns the indices of all the spatial sectors (partially)
covered by cyl.
FIXME
In the special case where cyl covers a sector of index 0 (along either axis)
and the sector before it (index -1 or equivalently, index n_sectors-1),
the returned vector of indexes only contains 0 and NOT {0, n_sectors-1}.
Code correctness is ensured by CylinderSubstrate::checkBoundaryConditions
but could be simplified nevertheless (see SphereSubstrate above).
*/
std::vector<std::vector<int> > CylinderSubstrate::get_sector_indexes(
    const Pore& cyl, unsigned int n_sectors) const {
  double sector_side[2] = {(this->max_limits[0]-this->min_limits[0])/n_sectors,
                           (this->max_limits[1]-this->min_limits[1])/n_sectors };
  // FIXME: make max sector index coordinate-dependent
  int max_sector_idx = (int)n_sectors - 1;  // shorthand

  // ! Do not use unsigned integers here as some calculations rely on being
  // able to manipulate the value -1, which is then typically mapped to
  // max_sector_idx.

  // idx will be a std::vector containing 1, 2 or 4 two-dimensional vectors,
  // reprensenting the sectors covered by cyl
  std::vector<std::vector<int> > idx;

  // Store position of sector containing center of cyl
  std::vector<int> tmp(2, -1);
  tmp[0] = int(floor(cyl.P_[0] / sector_side[0]));
  tmp[1] = int(floor(cyl.P_[1] / sector_side[1]));
  int i;
  for (i = 0; i<2; ++i) {
    tmp[i] = std::max(tmp[i], 0);
    tmp[i] = std::min(tmp[i], max_sector_idx);
  }
  // idx[0] contains the position index of the sector in which cyl lies
  idx.push_back(tmp);

  // Get sectors spanned by cylinder cyl on 4 main sides, i.e. sectors
  // with positions p such that (p-idx[0]) is a 2-dim vector containing
  // one zero and one +1 or -1.
  for (i = 0; i<2; ++i) {
    // initialize tmp to sector containing cyl
    tmp[0] = idx[0][0];
    tmp[1] = idx[0][1];
    if (int(floor((cyl.P_[i]-cyl.radius_-this->sep_gap)
            /sector_side[i]))<idx[0][i]) {
      tmp[i] = std::max(tmp[i] - 1, 0);
      idx.push_back(tmp);
    } else if (int(floor((cyl.P_[i]+cyl.radius_+this->sep_gap)
               /sector_side[i]))>idx[0][i]) {
      tmp[i] = std::min(tmp[i] + 1, max_sector_idx);
      idx.push_back(tmp);
    }
  }

  if (idx.size() == 3) {
    // Add sectors spanned by cyl in diagonal directions, i.e. sectors with
    // position p such that (p-idx[0]) is a 2-dim vector containing only
    // +1 and -1 entries.

    // FIXME: if idx[0][i]=0 and we're doing -1 then we should have
    // tmp[i]=max_sector_idx[i] instead of 0 ideally. The program is still
    // correct because the function CylinderSubstrate::checkBoundaryConditions
    // checks it but it adds unnecessary complication
    tmp[0] = idx[0][0];
    tmp[1] = idx[0][1];
    if (int(floor((cyl.P_[0] - cyl.radius_ - this->sep_gap) / sector_side[0]))
        <idx[0][0] &&
        int(floor((cyl.P_[1] - cyl.radius_ - this->sep_gap) / sector_side[1]))
        <idx[0][1]) {
      tmp[0] = std::max(tmp[0] - 1, 0);
      tmp[1] = std::max(tmp[1] - 1, 0);
      idx.push_back(tmp);
    } else if (int(floor((cyl.P_[0] - cyl.radius_ - this->sep_gap) / sector_side[0]))
               <idx[0][0] &&
               int(floor((cyl.P_[1] + cyl.radius_ + this->sep_gap) / sector_side[1]))
               >idx[0][1]) {
      tmp[0] = std::max(tmp[0] - 1, 0);
      tmp[1] = std::min(tmp[1] + 1, max_sector_idx);
      idx.push_back(tmp);
    } else if (int(floor((cyl.P_[0] + cyl.radius_ + this->sep_gap) / sector_side[0]))
               >idx[0][0] &&
               int(floor((cyl.P_[1] - cyl.radius_ - this->sep_gap) / sector_side[1]))
               <idx[0][1]) {
      tmp[0] = std::min(tmp[0] + 1, max_sector_idx);
      tmp[1] = std::max(tmp[1] - 1, 0);
      idx.push_back(tmp);
    } else if (int(floor((cyl.P_[0] + cyl.radius_ + this->sep_gap) / sector_side[0]))
               >idx[0][0] &&
               int(floor((cyl.P_[1] + cyl.radius_ + this->sep_gap) / sector_side[1]))
               >idx[0][1]) {
      tmp[0] = std::min(tmp[0] + 1, max_sector_idx);
      tmp[1] = std::min(tmp[1] + 1, max_sector_idx);
      idx.push_back(tmp);
    }
  }
  return idx;
}

/*
 Adds the cylinder itself and its reflected versions to the list cylinders_to_add, if substrate boundaries are crossed.
 */
void CylinderSubstrate::checkBoundaryConditions(const Pore& cyl,
    std::vector<Pore>& cylinders_to_add) const {
  cylinders_to_add.push_back(cyl);
  double rad = cyl.radius_;
  // Reflect a cylinder if part of it crosses the substrate boundary
  // along the x or y direction
  int i;
  for (i = 0; i<2; i++) {
    if (cyl.P_[i]+rad >= max_limits[i]) {       
      Pore tmp = cyl;
      tmp.P_[i]+= min_limits[i] - max_limits[i];
      cylinders_to_add.push_back(tmp);
    }
    if (cyl.P_[i] - rad < min_limits[i]) {
      Pore tmp = cyl;
      tmp.P_[i]+= max_limits[i] - min_limits[i];
      cylinders_to_add.push_back(tmp);
    }
  }
  // If a cylinder trespassed the substrate boundaries along the x AND
  // the y direction then it also needs to be reflected in the opposite
  // corner of the substrate:
  if (cylinders_to_add.size() == 3) {
    if (cyl.P_[0]-rad<min_limits[0] && cyl.P_[1]-rad<min_limits[1]) {
      Pore tmp = cyl;
      tmp.P_[0]+= max_limits[0] - min_limits[0];
      tmp.P_[1]+= max_limits[1] - min_limits[1];
      cylinders_to_add.push_back(tmp);
    }
    if (cyl.P_[0]-rad<min_limits[0] && cyl.P_[1]+rad>=max_limits[1]) {
      Pore tmp = cyl;
      tmp.P_[0]+= max_limits[0] - min_limits[0];
      tmp.P_[1]+= min_limits[1] - max_limits[1];
      cylinders_to_add.push_back(tmp);
    }
    if (cyl.P_[0]+rad>=max_limits[0] && cyl.P_[1]-rad<min_limits[1]) {
      Pore tmp = cyl;
      tmp.P_[0]+= min_limits[0] - max_limits[0];
      tmp.P_[1]+= max_limits[1] - min_limits[1];
      cylinders_to_add.push_back(tmp);
    }
    if (cyl.P_[0]+rad>=max_limits[0] && cyl.P_[1]+rad>=max_limits[1]) {
      Pore tmp = cyl;
      tmp.P_[0]+= min_limits[0] - max_limits[0];
      tmp.P_[1]+= min_limits[1] - max_limits[1];
      cylinders_to_add.push_back(tmp);
    }
  }
}

double CylinderSubstrate::computeICVF() const {
  if (this->unique_pores.size() == 0)
    return 0;

  double AreaV = (this->max_limits[0] - this->min_limits[0])
                  *(this->max_limits[1] - this->min_limits[1]);

  double AreaC = 0;
  long int i;
  for (i = 0; i< (long int)this->unique_pores.size(); i++) {
    double rad = this->unique_pores[i].radius_;
    AreaC += M_PI*rad*rad;
  }
  return AreaC / AreaV;
}
