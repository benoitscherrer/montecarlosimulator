#ifndef PORE_PACKING_PACK_PORES_H
#define PORE_PACKING_PACK_PORES_H
#define _CRTDBG_MAP_ALLOC   // for line number in memory reports by _CrtDumpMemoryLeaks(); must be placed in every .c[xx] file

#include <iostream>
#include <string>
#include <vector>

#include "tclap/Constraint.h"

class Pore{
 public:
  Pore(double P[3], double radius);

  double P_[3];  // Pore's center
  double radius_;    // Radius of the pore
};

/*************** SUBSTRATE base class ******************/
class Substrate{
 public: 
  // Default value for number of attempts at placing a pore in the
  // the bounding box of the substrate
  static const int kDefaultMaxAttempts = 1000000;
  // Full constructor
  Substrate(std::string distribution, double minrad, double maxrad,
            double mean, double std, double icvf, double gap, int num_pores,
            int max_attemps);
  // Constructor using default value for max_attempts
  Substrate(std::string distribution, double minrad, double maxrad,
            double mean, double std, double icvf, double gap, int num_pores);
  // Virtual destructor for deleting a derived-class object declared with a
  // pointer to a base class
  virtual ~Substrate() {};
  // Draw num_pores radius values from specified distribution. Common to
  // all substrate types
  std::vector<double> draw_radii(unsigned long seedrad);
  // Place pores from draw radius list using brute force within the
  // bounding box, avoiding intersections
  virtual void position_pores(unsigned long seedrad,
                              unsigned long seedpos) = 0;
  // Get intra-pore volume fraction
  virtual double computeICVF() const = 0;
  void printPores(std::ostream &out);
  void printUniquePores(std::ostream &out);
  void printSubstrateProperties(std::ostream & out);

  // List of pores including mirrored copies for pores crossing the
  // boundaries of the substrate. FIXME: could have been avoided...
  std::vector<Pore> pores;
  // List of pores excluding mirrored copies, which actually entirely
  // chraracterize the substrate
  std::vector<Pore> unique_pores;
  std::string dist_law;
  double minRad, maxRad, dist_mean, dist_std, icvf, sep_gap;
  // Target number of pores to place in substrate, not always attained
  long int num_pores;
  // Opposite vertices of a rectangle (for a substrate containing
  // cylinders) or a cube (for a substrate containing spheres) representing
  // the bounding box containing the pores
  std::vector<double> min_limits, max_limits;
  // Max number of attempts at placing a pore in a substrate before
  // giving up and discarding that pore althogether
  int max_attempts;
};

/****************** SPHERES ***********************/

class SphereSubstrate: public Substrate{
 public:
  SphereSubstrate(std::string distribution, double minrad, double maxrad,
                  double m, double s, double i, double gap, int num_sph,
                  int max_attempts);
  SphereSubstrate(std::string distribution, double minrad, double maxrad,
                  double m, double s, double i, double gap, int num_sph);
  void position_pores(unsigned long seedrad, unsigned long seedpos);  // final
  bool checkForCollision(const Pore& sph, std::vector<Pore>& spheres_to_add,
                         unsigned int n_sectors) const;
  std::vector<std::vector<int> > get_sector_indexes(const Pore& sph,
    unsigned int n_sectors) const;
  void checkBoundaryConditions(const Pore& sph,
                               std::vector<Pore>& spheres_to_add) const;
  double computeICVF() const; //final(-std=c++11) 

  // entry [i,j,k] contains a list of all the indics of the pores
  // contained in spatial sector i,j,k
  std::vector<std::vector<std::vector<std::vector<
  long int> > > > sector_sphere_idx;
};
                            

/*********************** CYLINDERS **************************/

class CylinderSubstrate: public Substrate{
 public:
  CylinderSubstrate(std::string distribution, double minrad,
                    double maxrad, double mean, double std, double icvf,
                    double gap, int num_cyl, int max_attemtps);
  CylinderSubstrate(std::string distribution, double minrad,
                    double maxrad, double mean, double std, double icvf,
                    double gap, int num_cyl);
  void position_pores(unsigned long seedrad, unsigned long seedpos); // final
  bool checkForCollision(const Pore& cyl,
                         std::vector<Pore>& cylinders_to_add,
                         unsigned int n_sectors) const;
  std::vector<std::vector<int> > get_sector_indexes(
      const Pore& cyl, unsigned int n_sectors) const;
  void checkBoundaryConditions(const Pore& cyl,
                               std::vector<Pore>& cylinders_to_add) const;

  double computeICVF() const;  // final requires std=c++11

  // sector_cylinder_idx [i,j] contains  a list of all the indexes of
  // the pores contained in spatial sector i,j
  std::vector<std::vector<std::vector<
  long int> > > sector_cylinder_idx;
};


///*********************************************************************
//Classes for constraints on parsed arguments (see TCLAP header library)
//**********************************************************************/
//class PosDoubleCstrnt : public TCLAP::Constraint<double> {
// public:
//  bool check(const double& value) const {
//    return value > 0;
//  }
//  std::string shortID() const{
//    return "pos. floating-point";
//  }
//  std::string description() const{
//    return "should be a stricly positive floating-point number";
//  }
//};
//
//class Pos0DoubleCstrnt : public TCLAP::Constraint<double> {
// public:
//  bool check(const double& value) const {
//    return value >=0;
//  }
//  std::string shortID() const {
//    return "non-neg. floating-point";
//  }
//  std::string description() const {
//    return "should a non-negative floating-point number";
//  }
//};
//
//class PosIntCstrnt : public TCLAP::Constraint<int> {
// public:
//  bool check(const int& value) const {
//    return value > 0;
//  }
//  std::string shortID() const {
//    return "pos. integer";
//  }
//  std::string description() const {
//    return "should be a stricly positive integer";
//  }
//};
//
//class Btw01Cstrnt : public TCLAP::Constraint<double> {
// public:
//  bool check(const double& value) const {
//    return value > 0 && value < 1;
//  }
//  std::string shortID() const {
//    return "floating-point in ]0;1[";
//  }
//  std::string description() const {
//    return "should be a floating-point number in ]0;1[";
//  }
//};
//
//class NonemptyStringCstrnt : public TCLAP::Constraint<std::string> {
// public:
//  bool check(const std::string& value) const {
//    return value.size() > 0;
//  }
//  std::string shortID() const {
//    return "unix/like/path";
//  }
//  std::string description() const {
//    return "basename cannot be empty";
//  }
//};
//
//class DistStringCstrnt : public TCLAP::Constraint<std::string> {
// public:
//  bool check(const std::string& value) const {
//    return (value == "normal" || value == "gamma");
//  }
//  std::string shortID() const{
//    return "normal/gamma";
//  }
//  std::string description() const{
//    return "should be normal or gamma";
//  }
//};
//
//class PoreStringCstrnt : public TCLAP::Constraint<std::string> {
// public:
//  bool check(const std::string& value) const {
//    return (value == "cylinder" || value == "sphere");
//  }
//  std::string shortID() const {
//    return "cylinder/sphere";
//  }
//  std::string description() const {
//    return "should be cylinder or sphere";
//  }
//};

#endif //!PORE_PACKING_PACK_PORES_H
